﻿using Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Monitoring.Data;
using Monitoring.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Class
{
    public class ManageUsers
    {
        //private static ApplicationDbContext userContext = new ApplicationDbContext();
        private readonly ApplicationDbContext _context;

        public ManageUsers(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task CreateUser(Users ent,string Role)
        {
            var user = new ApplicationUser
            {
                UserName = ent.UsrUsername,
                NormalizedUserName = ent.UsrUsername,
                Email = ent.UsrUsername,
                NormalizedEmail = ent.UsrUsername,
                EmailConfirmed = true,
                LockoutEnabled = false,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var roleStore = new RoleStore<IdentityRole>(_context);

            if (!_context.Roles.Any(r => r.Name == Role))
            {
                await roleStore.CreateAsync(new IdentityRole { Name = Role, NormalizedName = Role });

            }

            if (!_context.Users.Any(r => r.UserName == user.UserName))
            {
                var password = new PasswordHasher<ApplicationUser>();
                var hashed = password.HashPassword(user, ent.Password);
                user.PasswordHash = hashed;
                var userStore = new UserStore<ApplicationUser>(_context);
                await userStore.CreateAsync(user);
                await userStore.AddToRoleAsync(user, "SuperUser");

            }
            await _context.SaveChangesAsync();
        }


        public async Task CreateAdminUser()
        {
            var user = new ApplicationUser
            {
                UserName = "Cesar_gonzales_aguije@hotmail.com",
                NormalizedUserName = "Cesar_gonzales_aguije@hotmail.com",
                Email = "Cesar_gonzales_aguije@hotmail.com",
                NormalizedEmail = "Cesar_gonzales_aguije@hotmail.com",
                EmailConfirmed = true,
                LockoutEnabled = false,
                SecurityStamp = Guid.NewGuid().ToString()
            };

            var roleStore = new RoleStore<IdentityRole>(_context);

            if (!_context.Roles.Any(r => r.Name == "SuperUser"))
            {
                await roleStore.CreateAsync(new IdentityRole { Name = "SuperUser", NormalizedName = "SuperUser" });

            }

            if (!_context.Users.Any(r => r.UserName == user.UserName))
            {
                var password = new PasswordHasher<ApplicationUser>();
                var hashed = password.HashPassword(user, "q1w2e3r4t5y");
                user.PasswordHash = hashed;
                var userStore = new UserStore<ApplicationUser>(_context);
                await userStore.CreateAsync(user);
                await userStore.AddToRoleAsync(user, "SuperUser");

            }
            await _context.SaveChangesAsync();

            //string[] roles = new string[] { "SuperUser","Administrator","User"};
            //foreach (var role in roles)
            //{
            //    var roleStore = new RoleStore<IdentityRole>(context);
            //    if (!context.)
            //    {

            //    }
            //}
            //var settings = serviceProvider.GetService<IOptions<SiteSettings>>().Value;

            //const string adminRole = "Administrator";



            //var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();

            //var roleManager = serviceProvider.GetService<RoleManager<IdentityRole>>();



            //if (!await roleManager.RoleExistsAsync(adminRole))

            //{

            //    await roleManager.CreateAsync(new IdentityRole(adminRole));

            //}



            //var user = await userManager.FindByNameAsync(settings.DefaultAdminUsername);

            //if (user == null)

            //{

            //    user = new ApplicationUser { UserName = settings.DefaultAdminUsername };

            //    await userManager.CreateAsync(user, settings.DefaultAdminPassword);

            //    await userManager.AddToRoleAsync(user, adminRole);

            //    await userManager.AddClaimAsync(user, new Claim("app-ManageStore", "Allowed"));

            //}

        }

        public static void CheckRole(string roleName)
        {
            //var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(userContext));

            //// Check to see if Role Exists, if not create it
            //if (!roleManager.RoleExistsAsync(roleName).Result)
            //{
            //    roleManager.CreateAsync(new IdentityRole(roleName));
            //}
        }
    }
}
