﻿using Entities;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Hubs;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Hubs
{
    [HubName("TrainingHub")]
    public class TrainingHub : Hub
    {
        public async Task Training(string TrainingId)
        {
            await Groups.Add(Context.ConnectionId, GetGroup(TrainingId));

            // When a user training a classifier that was already 
            // highlighted, we need to send it immediately, because
            // otherwise she will listen for it infinitely.

            using (var db = new MonitoringContext())
            {

                var training = await db.Training

                    .Where(x => x.TraId == TrainingId && x.TraStatus != null)
                    .SingleOrDefaultAsync();

                if (training != null)

                {

                    Clients.Client(Context.ConnectionId)
                    .highlight(training.TraId, training.TraStatus, training.HighlightedIn?.TotalMilliseconds.ToString("N0"));

                }

            }

        }

        public static string GetGroup(string TrainingtId)
        {
            return "Training:" + TrainingtId;
        }

    }
}
