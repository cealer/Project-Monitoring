﻿// Write your Javascript code.
$('#myModalContent').on('shown.bs.modal', function () {
    //$('#myInput').focus()    
    ValidarModal();
})

$("#myModalContent").on("hidden.bs.modal", function () {
    $('#myModalContent').removeData();
})
function ValidarModal() {
    $("#myModalContent").removeData("validator");
    $("#myModalContent").removeData("unobtrusiveValidation");
    $.validator.unobtrusive.parse("#myModalContent");
}
//function frmValid() {
//    $.validator.unobtrusive.parse('#myModalContent');
//}

function getDataAjax(id, action) {
    $.ajax({
        type: "GET",
        url: action,
        data: { id },
        success: function (response) {
            $('#myModalContent').html(response);
        }
    });
}