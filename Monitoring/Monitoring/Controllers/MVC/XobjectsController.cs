using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Entities;

namespace Monitoring.Controllers.MVC
{
    public class XobjectsController : Controller
    {
        private readonly MonitoringContext _context;

        public XobjectsController(MonitoringContext context)
        {
            _context = context;
        }

        // GET: Xobjects
        public async Task<IActionResult> Index()
        {
            var monitoringContext = _context.Xobjects.Where(x=>x.ObjId!= "OBJ000000000000001").Include(x => x.Sub).Include(x=>x.Sub.Com)/*.Include(x => x.Usr)*/;
            return View(await monitoringContext.ToListAsync());
        }

        // GET: Xobjects/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var xobjects = await _context.Xobjects
                .Include(x => x.Sub)
                //.Include(x => x.Usr)
                .SingleOrDefaultAsync(m => m.ObjId == id);
            if (xobjects == null)
            {
                return NotFound();
            }

            return View(xobjects);
        }

        // GET: Xobjects/Create
        public IActionResult Create()
        {
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId");
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId");
            return View();
        }

        // POST: Xobjects/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ObjId,ObjDescription,ObjPath,ObjDate,SubId,UsrId,ObjEnabled")] Xobjects xobjects)
        {
            if (ModelState.IsValid)
            {
                _context.Add(xobjects);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", xobjects.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", xobjects.UsrId);
            return View(xobjects);
        }

        // GET: Xobjects/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var xobjects = await _context.Xobjects.SingleOrDefaultAsync(m => m.ObjId == id);
            if (xobjects == null)
            {
                return NotFound();
            }
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", xobjects.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", xobjects.UsrId);
            return View(xobjects);
        }

        // POST: Xobjects/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("ObjId,ObjDescription,ObjPath,ObjDate,SubId,UsrId,ObjEnabled")] Xobjects xobjects)
        {
            if (id != xobjects.ObjId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(xobjects);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!XobjectsExists(xobjects.ObjId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", xobjects.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", xobjects.UsrId);
            return View(xobjects);
        }

        // GET: Xobjects/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var xobjects = await _context.Xobjects
                .Include(x => x.Sub)
                //.Include(x => x.Usr)
                .SingleOrDefaultAsync(m => m.ObjId == id);
            if (xobjects == null)
            {
                return NotFound();
            }

            return View(xobjects);
        }

        // POST: Xobjects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var xobjects = await _context.Xobjects.SingleOrDefaultAsync(m => m.ObjId == id);
            _context.Xobjects.Remove(xobjects);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool XobjectsExists(string id)
        {
            return _context.Xobjects.Any(e => e.ObjId == id);
        }
    }
}
