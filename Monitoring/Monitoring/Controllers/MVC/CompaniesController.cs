using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Entities;
using Entities.Utilities;
using Microsoft.ProjectOxford.Face;
using Microsoft.AspNetCore.Authorization;

namespace Monitoring.Controllers
{
    [Authorize]
    public class CompaniesController : Controller
    {
        private readonly MonitoringContext _context;
        private readonly Generate _Utilities;

        public CompaniesController(MonitoringContext context)
        {
            _context = context;
            _Utilities = new Generate(context);
        }

        // GET: Companies
        public async Task<IActionResult> Index()
        {
            var monitoringContext = _context.Companies/*.Include(c => c.Usr)*/;
            return View(await monitoringContext.Where(x => x.ComEnabled == true).ToListAsync());
        }

        // GET: Companies/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companies = await _context.Companies
                //.Include(c => c.Usr)
                .SingleOrDefaultAsync(m => m.ComId == id);
            if (companies == null)
            {
                return NotFound();
            }

            return View(companies);
        }

        // GET: Companies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Companies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Companies companies)
        {
            if (ModelState.IsValid)
            {
                //var currentUser = _context.Users.Where(x => x.UsrUsername == User.Identity.Name).FirstOrDefault();
                //companies.UsrId = currentUser.UsrId;
                companies.ComDate = DateTime.Now;
                companies.ComEnabled = true;
                companies.ComId = await _Utilities.GenerateIdAsync("COM", "Register.Companies");
                _context.Add(companies);
                await _context.SaveChangesAsync();

                const string subscriptionKey = "667ff4c7d73442698763ccec18a7f13b";
                var faceServiceClient = new FaceServiceClient(subscriptionKey);
                await faceServiceClient.CreatePersonGroupAsync(companies.ComId.ToLower(), $"{companies.ComDescription}");


                return RedirectToAction("Index");
            }
            return View(companies);
        }

        // GET: Companies/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companies = await _context.Companies.SingleOrDefaultAsync(m => m.ComId == id);
            if (companies == null)
            {
                return NotFound();
            }
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", companies.UsrId);
            return View(companies);
        }

        // POST: Companies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("ComId,ComDate,ComDescription,ComLocation,UsrId,ComEnabled")] Companies companies)
        {
            if (id != companies.ComId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(companies);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CompaniesExists(companies.ComId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", companies.UsrId);
            return View(companies);
        }

        // GET: Companies/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var companies = await _context.Companies
                //.Include(c => c.Usr)
                .SingleOrDefaultAsync(m => m.ComId == id);
            if (companies == null)
            {
                return NotFound();
            }

            return View(companies);
        }

        // POST: Companies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var companies = await _context.Companies.SingleOrDefaultAsync(m => m.ComId == id);
            companies.ComEnabled = false;
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool CompaniesExists(string id)
        {
            return _context.Companies.Any(e => e.ComId == id);
        }
    }
}
