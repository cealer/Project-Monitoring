﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Monitoring.Azure.Storage;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Text;
using Monitoring.Azure.B2C;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authorization;
using System.Web;
using System.Collections.Specialized;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Monitoring.PowerBI;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using Monitoring.Models;
using System.Collections.Generic;
using RestSharp;
using Microsoft.Rest;

namespace Monitoring.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        private readonly Blob_Operations blob = new Blob_Operations();
        AzureAdB2COptions AzureAdB2COptions;

        IConfiguration _iconfiguration;

        public HomeController(IConfiguration iconfiguration, IOptions<AzureAdB2COptions> azureAdB2COptions)
        {
            _iconfiguration = iconfiguration;
            AzureAdB2COptions = azureAdB2COptions.Value;
        }

        [HttpPost]
        public async Task<IActionResult> Index(Report report)
        {
            string token = "";

            string clientId = _iconfiguration.GetSection("PowerBi").GetSection("ClientID").Value;
            string PowerBiAPI = _iconfiguration.GetSection("PowerBi").GetSection("PowerBiAPI").Value;
            string AADAuthorityUri = _iconfiguration.GetSection("PowerBi").GetSection("AADAuthorityUri").Value;
            string username = _iconfiguration.GetSection("PowerBi").GetSection("Username").Value;
            string password = _iconfiguration.GetSection("PowerBi").GetSection("Password").Value;
            string ClientSecret = _iconfiguration.GetSection("PowerBi").GetSection("ClientSecret").Value;
            string grant_type = _iconfiguration.GetSection("PowerBi").GetSection("grant_type").Value;

            AAD AADs = new AAD();


            //var client = new RestClient("https://login.windows.net/common/oauth2/token");
            //var request = new RestRequest(Method.POST);
            //request.AddHeader("cache-control", "no-cache");
            //request.AddHeader("content-type", "application/x-www-form-urlencoded");
            //request.AddParameter("application/x-www-form-urlencoded", $"grant_type={grant_type}&username={username}&password={password}&resource={PowerBiAPI}&client_id={clientId}&client_secret={ClientSecret}", ParameterType.RequestBody);
            //IRestResponse response = client.Execute(request);

            //var client = new RestClient("https://login.microsoftonline.com/common/oauth2/token");
            //var request = new RestRequest(Method.POST);

            //request.AddHeader("content-type", "application/x-www-form-urlencoded");
            //request.AddParameter("application/x-www-form-urlencoded", "grant_type=password&username=powerbiembeded%40cesargonzalesaguijehotmail.onmicrosoft.com&password=%40%40AskS81928SADJ&resource=https%3A%2F%2Fanalysis.windows.net%2Fpowerbi%2Fapi&client_id=538f2848-90b4-4e23-ac9e-764d0e9c13e7&client_secret=VYNtxmC%2FL8azWqCseX5o4gS01Pk%2BulWP%2FvzvphOHTac%3D", ParameterType.RequestBody);
            //IRestResponse response = client.Execute(request);

            var client = new RestClient("https://login.microsoftonline.com/common/oauth2/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("postman-token", "aba0f862-5a04-596f-8160-e0f70797aabb");
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("application/x-www-form-urlencoded", "grant_type=password&username=powerbi%40cesargonzalescordovahotmail.onmicrosoft.com&password=%40%40Daho5006%40%40&resource=https%3A%2F%2Fanalysis.windows.net%2Fpowerbi%2Fapi&client_id=bc362514-fdcf-4fdd-8a39-6a6d8d5a66ea&client_secret=M0gIsaHQFhBJSa529Eo%2F2A%2FqpOjjrg4gcT%2BNu6l6Kg4%3D", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            var result = response.Content;
            AADs = JsonConvert.DeserializeObject<AAD>(result);

            var tokenCredentials = new TokenCredentials(AADs.access_token, "Bearer");

            var client2 = new RestClient("https://api.powerbi.com/v1.0/myorg/reports");
            var request2 = new RestRequest(Method.GET);

            request2.AddHeader("authorization", $"Bearer {AADs.access_token}");
            request2.AddHeader("content-type", "application/json");
            IRestResponse response2 = client2.Execute(request2);
            string responseData = response2.Content;
            report = JsonConvert.DeserializeObject<Report>(responseData);

            var client3 = new RestClient("https://api.powerbi.com/v1.0/myorg/groups/0d7d610e-88e7-4dff-baf3-4117997b0753/reports/d08a85ef-77e3-436e-9aeb-613bae127866/GenerateToken");
            var request3 = new RestRequest(Method.POST);
            request3.AddHeader("content-type", "application/json");
            request3.AddHeader("authorization", $"Bearer {AADs.access_token}");
            request3.AddParameter("application/json", "{\r\n    \"accessLevel\": \"View\",\r\n    \"identities\": [\r\n        {\r\n            \"username\": \"powerbiembeded@cesargonzalesaguijehotmail.onmicrosoft.com\",\r\n            \"roles\": [ \"User\" ],\r\n            \"datasets\": [ \"ec29e94f-1af0-4903-a5ee-d813ddd25385\" ]\r\n        }\r\n    ]\r\n}", ParameterType.RequestBody);
            IRestResponse response3 = client3.Execute(request3);
            string responseData2 = response3.Content;
            var tokenEmbed = JsonConvert.DeserializeObject<TokenEmbed>(responseData2);
            report.token = tokenEmbed.token;
            //ViewData["Token"] = tokenEmbed.token;
            //ViewData["EmbedUrl"] = report.value[0].embedUrl;
            //ViewData["Id"] = report.value[0].id;

            return View(report);
        }
        
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var client = new RestClient("http://notificationhub20170827083407.azurewebsites.net/api/values/reports");
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            var result = response.Content;
            var embedded = JsonConvert.DeserializeObject<Models.PowerBi.Report>(result);
            return View(embedded);
        }

        [Route("AcercaDe")]
        [Route("About")]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            string account = _iconfiguration.GetSection("ConnectionStrings").GetSection("StorageConnectionString").Value;
            //var stringX = Blob_Operations.GetContainerUri(account, "peoplecontainer");

            var r = blob.GetBlobs("peoplecontainer", account);

            ViewData["Lista"] = r.Result;
            return View();
        }

        [HttpGet]
        [Route("TestRec")]
        public async Task<IActionResult> TestRec()
        {
            const string subscriptionKey = "4271e94dd8e74862a4bb07e7ed0bbf7a";
            const string uriBase = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect";

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(uriBase);
                //client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                // Request headers.
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

                // Request parameters. A third optional parameter is "details".
                string requestParameters = "returnFaceId=true&returnFaceLandmarks=false&returnFaceAttributes=age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise";

                // Assemble the URI for the REST API Call.
                string uri = uriBase + "?" + requestParameters;

                Uri photo = new Uri("http://127.0.0.1:10000/devstoreaccount1/peoplecontainer/personaDetectada_3dbe9dbe-6ffc-11e7-9504-24fd52b749d6");

                StringBuilder aux = new StringBuilder();
                aux.Append("{\"url\":");
                aux.Append("\"");
                aux.Append($"{photo.AbsoluteUri}");
                aux.Append("\"}");

                HttpContent content = new StringContent(aux.ToString());

                content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

                HttpResponseMessage response = await client.PostAsync(uri, content);

                var res = response.Content.ReadAsStringAsync();

            }

            return Ok();
        }

        [Authorize]
        [HttpGet]
        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        //public IActionResult GetProgress(string jobId)
        //{
        //    if (string.IsNullOrEmpty(jobId))
        //        throw new ArgumentNullException(nameof(jobId));

        //    var job = JobStorage.Current.GetMonitoringApi().JobDetails(jobId);
        //    if (job == null)
        //        return NotFound();

        //    double? progress = null;

        //    var state = job.History.FirstOrDefault(s => s.StateName == ProcessingState.StateName);
        //    if (state != null)
        //    {


        //        //using (var consoleApi = JobStorage.Current. ())
        //        //{
        //        //    var lines = consoleApi. GetLines(jobId,
        //        //            JobHelper.DeserializeDateTime(state.Data["StartedAt"]),
        //        //            LineType.ProgressBar);
        //        //    if (lines.Count > 0)
        //        //        progress = lines[0].Progress;
        //        //}
        //    }

        //    return Json(new
        //    {
        //        State = job.History[0].StateName,
        //        Reason = job.History[0].Reason,
        //        Progress = progress
        //    });
        //}

        public IActionResult loadsidemenu()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}