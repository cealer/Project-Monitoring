using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Entities;

namespace Monitoring.Controllers.MVC
{
    public class TrainingsController : Controller
    {
        private readonly MonitoringContext _context;

        public TrainingsController(MonitoringContext context)
        {
            _context = context;
        }

        // GET: Trainings
        public async Task<IActionResult> Index()
        {
            var monitoringContext = _context.Training.Include(t => t.Neu)/*.Include(t => t.Usr)*/;
            return View(await monitoringContext.ToListAsync());
        }

        // GET: Trainings/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var training = await _context.Training
                .Include(t => t.Neu)
                //.Include(t => t.Usr)
                .SingleOrDefaultAsync(m => m.TraId == id);
            if (training == null)
            {
                return NotFound();
            }

            return View(training);
        }

        // GET: Trainings/Create
        public IActionResult Create()
        {
            ViewData["NeuId"] = new SelectList(_context.NeuralNetworks, "NeuId", "NeuDescription");
            ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId");
            return View();
        }

        // POST: Trainings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TraId,TraDescription,TraDate,NeuId,UsrId,TraEnabled")] Training training)
        {
            if (ModelState.IsValid)
            {
                _context.Add(training);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["NeuId"] = new SelectList(_context.NeuralNetworks, "NeuId", "NeuId", training.NeuId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", training.UsrId);
            return View(training);
        }

        // GET: Trainings/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var training = await _context.Training.SingleOrDefaultAsync(m => m.TraId == id);
            if (training == null)
            {
                return NotFound();
            }
            ViewData["NeuId"] = new SelectList(_context.NeuralNetworks, "NeuId", "NeuId", training.NeuId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", training.UsrId);
            return View(training);
        }

        // POST: Trainings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("TraId,TraDescription,TraDate,NeuId,UsrId,TraEnabled")] Training training)
        {
            if (id != training.TraId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(training);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TrainingExists(training.TraId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["NeuId"] = new SelectList(_context.NeuralNetworks, "NeuId", "NeuId", training.NeuId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", training.UsrId);
            return View(training);
        }

        // GET: Trainings/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var training = await _context.Training
                .Include(t => t.Neu)
                //.Include(t => t.Usr)
                .SingleOrDefaultAsync(m => m.TraId == id);
            if (training == null)
            {
                return NotFound();
            }

            return View(training);
        }

        // POST: Trainings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var training = await _context.Training.SingleOrDefaultAsync(m => m.TraId == id);
            _context.Training.Remove(training);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool TrainingExists(string id)
        {
            return _context.Training.Any(e => e.TraId == id);
        }
    }
}
