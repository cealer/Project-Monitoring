using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Entities;
using Entities.Utilities;

namespace Monitoring.Controllers.MVC
{
    public class AlarmsController : Controller
    {
        private readonly MonitoringContext _context;
        private readonly Generate _Utilities;

        public AlarmsController(MonitoringContext context)
        {
            _context = context;
            _Utilities = new Generate(context);
        }

        // GET: Alarms
        public async Task<IActionResult> Index()
        {
            var monitoringContext = _context.Alarms.Include(a => a.Obj).Include(a => a.Per).Include(a => a.Sub)/*.Include(a => a.Usr)*/;
            return View(await monitoringContext.ToListAsync());
        }

        // GET: Alarms/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alarms = await _context.Alarms
                .Include(a => a.Obj)
                .Include(a => a.Per)
                .Include(a => a.Sub)
                //.Include(a => a.Usr)
                .SingleOrDefaultAsync(m => m.AlmId == id);
            if (alarms == null)
            {
                return NotFound();
            }

            return View(alarms);
        }

        // GET: Alarms/Create
        public IActionResult Create()
        {
            ViewData["ObjId"] = new SelectList(_context.Xobjects, "ObjId", "ObjDescription");
            ViewData["PerId"] = new SelectList(_context.Persons, "PerId", "FullName");
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription");
            ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrUsername");
            return View();
        }

        // POST: Alarms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("AlmId,AlmDescription,AlmDate,PerId,ObjId,SubId,UsrId,AlmEnabled")] Alarms alarms)
        {
            if (ModelState.IsValid)
            {
                alarms.AlmDate = DateTime.Now;
                alarms.AlmId = await _Utilities.GenerateIdAsync("ALM", "Register.Alarms");
                _context.Add(alarms);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewData["ObjId"] = new SelectList(_context.Xobjects, "ObjId", "ObjDescription", alarms.ObjId);
            ViewData["PerId"] = new SelectList(_context.Persons, "PerId", "FullName", alarms.PerId);
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription", alarms.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrUsername", alarms.UsrId);
            return View(alarms);
        }

        // GET: Alarms/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alarms = await _context.Alarms.SingleOrDefaultAsync(m => m.AlmId == id);
            if (alarms == null)
            {
                return NotFound();
            }
            ViewData["ObjId"] = new SelectList(_context.Xobjects, "ObjId", "ObjId", alarms.ObjId);
            ViewData["PerId"] = new SelectList(_context.Persons, "PerId", "PerId", alarms.PerId);
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", alarms.SubId);
            ////ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", alarms.UsrId);
            return View(alarms);
        }

        // POST: Alarms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("AlmId,AlmDescription,AlmDate,PerId,ObjId,SubId,UsrId,AlmEnabled")] Alarms alarms)
        {
            if (id != alarms.AlmId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(alarms);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AlarmsExists(alarms.AlmId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["ObjId"] = new SelectList(_context.Xobjects, "ObjId", "ObjId", alarms.ObjId);
            ViewData["PerId"] = new SelectList(_context.Persons, "PerId", "PerId", alarms.PerId);
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", alarms.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", alarms.UsrId);
            return View(alarms);
        }

        // GET: Alarms/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var alarms = await _context.Alarms
                .Include(a => a.Obj)
                .Include(a => a.Per)
                .Include(a => a.Sub)
                //.Include(a => a.Usr)
                .SingleOrDefaultAsync(m => m.AlmId == id);
            if (alarms == null)
            {
                return NotFound();
            }

            return View(alarms);
        }

        // POST: Alarms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var alarms = await _context.Alarms.SingleOrDefaultAsync(m => m.AlmId == id);
            _context.Alarms.Remove(alarms);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool AlarmsExists(string id)
        {
            return _context.Alarms.Any(e => e.AlmId == id);
        }
    }
}
