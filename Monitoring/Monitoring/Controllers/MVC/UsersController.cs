using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Entities;
using Entities.Utilities;
using Monitoring.Class;
using Monitoring.Data;

namespace Monitoring.Controllers
{
    public class UsersController : Controller
    {
        private readonly MonitoringContext _context;
        private readonly ApplicationDbContext _UserContext;
        private readonly Generate _Utilities;
        private readonly ManageUsers _Manage;


        public UsersController(MonitoringContext context, ApplicationDbContext userContext)
        {
            _context = context;
            _UserContext = userContext;
            _Utilities = new Generate(context);
            _Manage = new ManageUsers(userContext);
        }

        // GET: Users
        public async Task<IActionResult> Index()
        {
            var monitoringContext = _context.Users/*Include(u => u.Sub)*/;
            return View(await monitoringContext.Where(x => x.UsrEnabled == true).ToListAsync());
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var users = await _context.Users
                //.Include(u => u.Sub)
                .SingleOrDefaultAsync(m => m.UsrId == id);
            if (users == null)
            {
                return NotFound();
            }

            return View(users);
        }

        // GET: Users/Create
        public IActionResult Create()
        {
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription");
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Users users)
        {
            if (ModelState.IsValid)
            {
                //users.UsrDate = DateTime.Now;
                users.UsrId = await _Utilities.GenerateIdAsync("USR", "Register.Users");
                users.UsrEnabled = true;
                _context.Add(users);
                await _context.SaveChangesAsync();

                await _Manage.CreateUser(users, "User");

                return RedirectToAction("Index");
            }
            //ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", users.SubId);
            return View(users);
        }

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var users = await _context.Users.SingleOrDefaultAsync(m => m.UsrId == id);
            if (users == null)
            {
                return NotFound();
            }
            //ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription", users.SubId);
            return View(users);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, Users users)
        {
            if (id != users.UsrId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(users);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UsersExists(users.UsrId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            //ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription", users.SubId);
            return View(users);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var users = await _context.Users
                //.Include(u => u.Sub)
                .SingleOrDefaultAsync(m => m.UsrId == id);
            if (users == null)
            {
                return NotFound();
            }

            return View(users);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var users = await _context.Users.SingleOrDefaultAsync(m => m.UsrId == id);
            users.UsrEnabled = false;
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool UsersExists(string id)
        {
            return _context.Users.Any(e => e.UsrId == id);
        }
    }
}
