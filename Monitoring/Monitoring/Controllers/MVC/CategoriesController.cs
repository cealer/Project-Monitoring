using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Entities;
using Microsoft.AspNetCore.Authorization;
using Entities.Utilities;
using Monitoring.Models;
using Microsoft.ProjectOxford.Face;

namespace Monitoring.Controllers
{
    [Authorize]
    public class CategoriesController : Controller
    {
        private readonly MonitoringContext _context;
        private readonly Generate _generate;

        public Generate Generate => _generate;

        public CategoriesController(MonitoringContext context)
        {
            _context = context;
            _generate = new Generate(_context);
        }

        // GET: Categories
        public async Task<IActionResult> Index()
        {
            var monitoringContext = _context.Categories.Where(x => x.CatEnabled == true).Include(c => c.Sub).Include(c => c.Sub.Com).Take(10)/*.Include(c => c.Usr)*/;
            return View(await monitoringContext.ToListAsync());
        }

        // GET: Categories/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categories = await _context.Categories
                .Include(c => c.Sub)
                //.Include(c => c.Usr)
                .SingleOrDefaultAsync(m => m.CatId == id);
            if (categories == null)
            {
                return NotFound();
            }

            return View(categories);
        }

        // GET: Categories/Create
        public IActionResult Create()
        {
            //var currentUser = _context.Users.Where(x => x.UsrUsername == User.Identity.Name).Include(x => x.Per).FirstOrDefault();
            //ViewData["SubId"] = currentUser.Per.SubId;
            //ViewData["UsrId"] = currentUser.UsrId;
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription");
            return View();
        }

        // POST: Categories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ViewCategory view)
        {

            if (ModelState.IsValid)
            {
                view.Category.CatId = _generate.GenerateId("CAT", "Register.Categories");
                _context.Add(view.Category);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            //var currentUser = _context.Users.Where(x => x.UsrUsername == User.Identity.Name).Include(x => x.Per).FirstOrDefault();
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription", view.Category.SubId);
            //ViewData["SubId"] = currentUser.Per.SubId;
            //ViewData["UsrId"] = currentUser.UsrId;
            return View(view);
        }

        // GET: Categories/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categories = await _context.Categories.SingleOrDefaultAsync(m => m.CatId == id);
            if (categories == null)
            {
                return NotFound();
            }
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription", categories.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", categories.UsrId);
            return View(categories);
        }

        // POST: Categories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("CatId,CatDescription,CatDate,SubId,UsrId,CatEnabled")] Categories categories)
        {
            if (id != categories.CatId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(categories);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CategoriesExists(categories.CatId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription", categories.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", categories.UsrId);
            return View(categories);
        }

        // GET: Categories/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var categories = await _context.Categories
                .Include(c => c.Sub)
                //.Include(c => c.Usr)
                .SingleOrDefaultAsync(m => m.CatId == id);
            if (categories == null)
            {
                return NotFound();
            }

            return View(categories);
        }

        // POST: Categories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var categories = await _context.Categories.SingleOrDefaultAsync(m => m.CatId == id);
            categories.CatEnabled = false;
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult ValidateDescription(ViewCategory view)
        {
            if (view.CatId != null)
            {
                return Json(_context.Categories.Where(x => x.CatId != view.CatId).Any(x => x.CatDescription == view.Cat) ?
                    string.Format("El nombre del cargo ya est� en uso.", view.Cat) : "true");
            }
            return Json(_context.Categories.Any(x => x.CatDescription == view.Cat) ?
                string.Format("El nombre del cargo ya est� en uso.", view.Cat) : "true");
        }

        private bool CategoriesExists(string id)
        {
            return _context.Categories.Any(e => e.CatId == id);
        }
    }
}
