using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Entities;
using Entities.Utilities;
using Monitoring.Class;

namespace Monitoring.Controllers
{
    public class NeuralNetworksController : Controller
    {
        private readonly MonitoringContext _context;
        private readonly Generate _Utilities;
        private readonly Files _File;

        public NeuralNetworksController(MonitoringContext context)
        {
            _context = context;
            _Utilities = new Generate(context);
            _File = new Files();
        }

        // GET: NeuralNetworks
        public async Task<IActionResult> Index()
        {
            var monitoringContext = _context.NeuralNetworks.Where(x => x.SubEnabled == true).Include(n => n.Sub).Include(x=>x.Sub.Com)/*.Include(n => n.Usr)*/;
            return View(await monitoringContext.ToListAsync());
        }

        // GET: NeuralNetworks/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var neuralNetworks = await _context.NeuralNetworks
                .Include(n => n.Sub)
                //.Include(n => n.Usr)
                .SingleOrDefaultAsync(m => m.NeuId == id);
            if (neuralNetworks == null)
            {
                return NotFound();
            }

            return View(neuralNetworks);
        }

        // GET: NeuralNetworks/Create
        public IActionResult Create()
        {
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription");
            return View();
        }

        // POST: NeuralNetworks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(NeuralNetworks neuralNetworks)
        {
            var currentUser = _context.Users.Where(x => x.UsrUsername == User.Identity.Name)/*.Include(p => p.Companies)*/.FirstOrDefault();

            if (ModelState.IsValid)
            {
                neuralNetworks.NeuDate = DateTime.Now;
                neuralNetworks.NeuId = await _Utilities.GenerateIdAsync("NEU", "Register.NeuralNetworks");
                neuralNetworks.SubEnabled = true;
                //neuralNetworks.UsrId = currentUser.UsrId;
                neuralNetworks.NeuPath = _File.CreateNeu(neuralNetworks.NeuId);
                _context.Add(neuralNetworks);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription", neuralNetworks.SubId);

            return View(neuralNetworks);
        }

        // GET: NeuralNetworks/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var neuralNetworks = await _context.NeuralNetworks.SingleOrDefaultAsync(m => m.NeuId == id);
            if (neuralNetworks == null)
            {
                return NotFound();
            }
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", neuralNetworks.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", neuralNetworks.UsrId);
            return View(neuralNetworks);
        }

        // POST: NeuralNetworks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("NeuId,NeuDescription,NeuPath,NeuDate,SubId,UsrId,SubEnabled")] NeuralNetworks neuralNetworks)
        {
            if (id != neuralNetworks.NeuId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(neuralNetworks);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NeuralNetworksExists(neuralNetworks.NeuId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", neuralNetworks.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", neuralNetworks.UsrId);
            return View(neuralNetworks);
        }

        // GET: NeuralNetworks/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var neuralNetworks = await _context.NeuralNetworks
                .Include(n => n.Sub)
                //.Include(n => n.Usr)
                .SingleOrDefaultAsync(m => m.NeuId == id);
            if (neuralNetworks == null)
            {
                return NotFound();
            }

            return View(neuralNetworks);
        }

        // POST: NeuralNetworks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var neuralNetworks = await _context.NeuralNetworks.SingleOrDefaultAsync(m => m.NeuId == id);
            _context.NeuralNetworks.Remove(neuralNetworks);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool NeuralNetworksExists(string id)
        {
            return _context.NeuralNetworks.Any(e => e.NeuId == id);
        }
    }
}
