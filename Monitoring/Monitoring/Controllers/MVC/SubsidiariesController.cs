using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Entities;
using Entities.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.ProjectOxford.Face;

namespace Monitoring.Controllers
{
    [Authorize]
    public class SubsidiariesController : Controller
    {
        private readonly MonitoringContext _context;
        private readonly Generate _Utilities;

        public SubsidiariesController(MonitoringContext context)
        {
            _context = context;
            _Utilities = new Generate(context);
        }

        // GET: Subsidiaries
        public async Task<IActionResult> Index()
        {
            var monitoringContext = _context.Subsidiaries.Where(x => x.SubEnabled == true).Include(s => s.Com)/*.Include(x => x.Usr)*/;
            return View(await monitoringContext.ToListAsync());
        }

        // GET: Subsidiaries/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subsidiaries = await _context.Subsidiaries
                .Include(s => s.Com)
                .SingleOrDefaultAsync(m => m.SubId == id);
            if (subsidiaries == null)
            {
                return NotFound();
            }

            return View(subsidiaries);
        }

        // GET: Subsidiaries/Create
        public IActionResult Create()
        {
            ViewData["ComId"] = new SelectList(_context.Companies, "ComId", "ComDescription");
            return View();
        }

        // POST: Subsidiaries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Subsidiaries subsidiaries)
        {
            if (ModelState.IsValid)
            {
                //var currentUser = _context.Users.Where(x => x.UsrUsername == User.Identity.Name)/*.Include(p => p.Companies)*/.FirstOrDefault();
                subsidiaries.SubId = await _Utilities.GenerateIdAsync("SUB", "Register.Subsidiaries");
                subsidiaries.SubEnabled = true;
                subsidiaries.SubDate = DateTime.Now;
                //subsidiaries.UsrId = currentUser.UsrId;
                _context.Add(subsidiaries);
                await _context.SaveChangesAsync();
                
                return RedirectToAction("Index");
            }
            ViewData["ComId"] = new SelectList(_context.Companies, "ComId", "ComDescription", subsidiaries.ComId);
            return View(subsidiaries);
        }

        // GET: Subsidiaries/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subsidiaries = await _context.Subsidiaries.SingleOrDefaultAsync(m => m.SubId == id);
            if (subsidiaries == null)
            {
                return NotFound();
            }
            ViewData["ComId"] = new SelectList(_context.Companies, "ComId", "ComDescription", subsidiaries.ComId);
            return View(subsidiaries);
        }

        // POST: Subsidiaries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("SubId,ComId,SubDate,SubDescription,SubLocation,SubEnabled")] Subsidiaries subsidiaries)
        {
            if (id != subsidiaries.SubId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(subsidiaries);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SubsidiariesExists(subsidiaries.SubId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["ComId"] = new SelectList(_context.Companies, "ComId", "ComDescription", subsidiaries.ComId);
            return View(subsidiaries);
        }

        // GET: Subsidiaries/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subsidiaries = await _context.Subsidiaries
                .Include(s => s.Com)
                .SingleOrDefaultAsync(m => m.SubId == id);
            if (subsidiaries == null)
            {
                return NotFound();
            }

            return View(subsidiaries);
        }

        // POST: Subsidiaries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var subsidiaries = await _context.Subsidiaries.SingleOrDefaultAsync(m => m.SubId == id);
            subsidiaries.SubEnabled = false;
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool SubsidiariesExists(string id)
        {
            return _context.Subsidiaries.Any(e => e.SubId == id);
        }
    }
}
