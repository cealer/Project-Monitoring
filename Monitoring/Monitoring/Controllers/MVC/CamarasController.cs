using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Entities;
using Entities.Utilities;

namespace Monitoring.Controllers
{
    public class CamarasController : Controller
    {
        private readonly MonitoringContext _context;
        private readonly Generate _generate;

        public CamarasController(MonitoringContext context)
        {
            _context = context;
            _generate = new Generate(_context);
        }

        // GET: Camaras
        public async Task<IActionResult> Index()
        {
            var monitoringContext = _context.Camaras.Include(c => c.Sub)/*.Include(c => c.Usr)*/;
            return View(await monitoringContext.ToListAsync());
        }

        // GET: Camaras/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var camaras = await _context.Camaras
                .Include(c => c.Sub)
                //.Include(c => c.Usr)
                .SingleOrDefaultAsync(m => m.CamId == id);
            if (camaras == null)
            {
                return NotFound();
            }

            return View(camaras);
        }

        // GET: Camaras/Create
        public IActionResult Create()
        {
            ViewData["CamEnabled"] = true;
            ViewData["CamDate"] = DateTime.Now;
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId");
            return View();
        }

        public async Task<IActionResult> CamaraConection()
        {
            
            return Ok();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Camaras camaras)
        {
            if (ModelState.IsValid)
            {
                camaras.CamId = await _generate.GenerateIdAsync("CAM", "Register.Camaras");
                _context.Add(camaras);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewData["CamEnabled"] = true;
            ViewData["CamDate"] = DateTime.Now;
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", camaras.SubId);
            return View(camaras);
        }

        // GET: Camaras/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var camaras = await _context.Camaras.SingleOrDefaultAsync(m => m.CamId == id);
            if (camaras == null)
            {
                return NotFound();
            }
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", camaras.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", camaras.UsrId);
            return View(camaras);
        }

        // POST: Camaras/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("CamId,CamDescription,CamLocation,CamDate,SubId,CamSource,UsrId,CamEnabled")] Camaras camaras)
        {
            if (id != camaras.CamId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(camaras);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CamarasExists(camaras.CamId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", camaras.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", camaras.UsrId);
            return View(camaras);
        }

        // GET: Camaras/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var camaras = await _context.Camaras
                .Include(c => c.Sub)
                //.Include(c => c.Usr)
                .SingleOrDefaultAsync(m => m.CamId == id);
            if (camaras == null)
            {
                return NotFound();
            }

            return View(camaras);
        }

        // POST: Camaras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var camaras = await _context.Camaras.SingleOrDefaultAsync(m => m.CamId == id);
            _context.Camaras.Remove(camaras);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private bool CamarasExists(string id)
        {
            return _context.Camaras.Any(e => e.CamId == id);
        }
    }
}
