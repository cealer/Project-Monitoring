using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Entities;
using Entities.Utilities;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Hangfire;
using Monitoring.Models;
using Microsoft.AspNetCore.Authorization;
using Monitoring.Azure.Storage;
using Monitoring.Azure.B2C;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Monitoring.Controllers
{

    [Authorize]
    public class PeopleController : Controller
    {
        private readonly MonitoringContext _context;
        private readonly Generate _Utilities;
        private readonly Tools _tools;
        private IHostingEnvironment _env;
        private readonly Blob_Operations _blob;
        private readonly BlobPersonOperations _personOperation;

        private readonly Blob_Operations blob = new Blob_Operations();
        AzureAdB2COptions AzureAdB2COptions;

        IConfiguration _iconfiguration;

        public PeopleController(MonitoringContext context, IHostingEnvironment env, IConfiguration iconfiguration, IOptions<AzureAdB2COptions> azureAdB2COptions)
        {
            _env = env;
            _context = context;
            _tools = new Tools();
            _Utilities = new Generate(context);
            _blob = new Blob_Operations();
            _personOperation = new BlobPersonOperations();
            _iconfiguration = iconfiguration;
            AzureAdB2COptions = azureAdB2COptions.Value;
        }

        // GET: Persons
        public async Task<IActionResult> Index()
        {
            var monitoringContext = _context.Persons.Include(p => p.Cat).Include(p => p.Sub)/*.Include(p => p.Usr)*/.Where(x => x.PerEnabled == true && x.PerId != "PER000000000000001");

            return View(await monitoringContext.Take(10).ToListAsync());
        }

        // GET: Persons/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var persons = await _context.Persons
                .Include(p => p.Cat)
                .Include(p => p.Sub)
                //.Include(p => p.Usr)
                .Include(p => p.Sub.Com)
                .SingleOrDefaultAsync(m => m.PerId == id);

            if (persons == null)
            {
                return NotFound();
            }

            var details = await _context.DetailsRecognitions.Include(x => x.Rec).Where(x => x.Rec.PerId == id && x.DreEnabled == true)
                .Include(x => x.Alm).
                Include(x => x.Alm.Sub).
                Include(x => x.Alm.Sub.Com)
                .Take(3)
                .ToListAsync();

            var count = await _context.DetailsRecognitions.Include(x => x.Rec).Where(x => x.Rec.PerId == id).CountAsync();

            var recognition = await _context.Recognitions.Include(x => x.Cam).Where(x => x.PerId == id && x.RecEnabled == true).ToListAsync();

            return View(new viewDetailsPerson { Person = persons, Recognition = recognition, DetailsRecognitions = details, CountDetailRecognitions = count });
        }

        private List<ViewGender> LoadGender()
        {
            List<ViewGender> gender = new List<ViewGender> { new ViewGender {Description= "Masculino",Value="M" }
            ,new ViewGender{Description="Femenino",Value="F" } };
            return gender;
        }


        // GET: Persons/Create
        public IActionResult Create()
        {
            ViewData["Gender"] = new SelectList(LoadGender(), "Value", "Description");
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription");
            ViewData["CatId"] = new SelectList(_context.Categories, "CatId", "CatDescription");
            //ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", currentUser.Sub.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", currentUser.UsrId);

            return View();
        }



        // POST: Persons/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ViewPerson view)
        {
            string Photo = null;
            view.Person.PerDate = DateTime.Now;
            view.Person.PerEnabled = true;
            view.Person.PerId = await _Utilities.GenerateIdAsync("PER", "Register.Persons");

            if (ModelState.IsValid)
            {
                var photos = HttpContext.Request.Form.Files;
                string account = _iconfiguration.GetSection("ConnectionStrings").GetSection("StorageConnectionString").Value;
                foreach (var item in photos)
                {
                    Photo = await _blob.UploadPhotoPerson(item, account);
                }

                try
                {
                    var company = _context.Subsidiaries.Where(x=>x.SubId==view.Person.SubId).FirstOrDefault();
                    if (photos != null)
                    {
                        await _personOperation.CreatePersonAsync(Photo, view.Person.FullName, view.Person.PerDni,company.ComId.ToLower() );
                        view.Person.PerPath = view.Person.PerId;
                        view.Person.PerPhoto = Photo;
                        _context.Add(view.Person);

                        await _context.SaveChangesAsync();
                        return RedirectToAction("Index");
                    }

                }
                catch (Exception)
                {

                }
               

            }
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubDescription", view.Person.SubId);
            ViewData["Gender"] = new SelectList(LoadGender(), "Value", "Description", view.Person.PerGender);
            ViewData["CatId"] = new SelectList(_context.Categories, "CatId", "CatDescription", view.Person.CatId);
            ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", view.Person.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", view.Person.UsrId);
            return View(view);
        }

        //private async Task CreateDatasetPerson(ViewPerson view)
        //{
        //    var files = HttpContext.Request.Form.Files;
        //    foreach (var Image in files)
        //    {
        //        if (Image != null && Image.Length > 0)
        //        {
        //            var file = Image;
        //            var uploads = Path.Combine(_env.WebRootPath, $"Dataset_Person/{view.Person.PerId}");

        //            if (file.Length > 0)
        //            {
        //                var fileName = $"{Guid.NewGuid().ToString()}.jpg";
        //                //System.Console.WriteLine(fileName);
        //                using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
        //                {
        //                    await file.CopyToAsync(fileStream);
        //                    view.Person.PerPhoto = file.FileName;

        //                    RunBackGroundProcess(view, fileName);
        //                }
        //            }
        //        }
        //    }
        //}

        //private static void RunBackGroundProcess(ViewPerson view, string fileName)
        //{
        //    BackgroundJob.Enqueue<Tools>(x => x.RunImageProcessing(view.Person.PerId, $"{view.Person.PerId}/{fileName}"));
        //    var jobId = BackgroundJob.Enqueue<Tools>(x => x.RunClassifier());

        //}

        //private void CreateDirectoryPerson(string PersonId)
        //{
        //    var webRoot = _env.WebRootPath;
        //    if (!System.IO.Directory.Exists($"{webRoot}/Dataset_Person/{PersonId}"))
        //    {
        //        System.IO.Directory.CreateDirectory($"{webRoot}/Dataset_Person/{PersonId}");
        //    }
        //}

        // GET: Persons/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var persons = await _context.Persons.SingleOrDefaultAsync(m => m.PerId == id);

            if (persons == null)
            {
                return NotFound();
            }

            ViewPerson view = new ViewPerson { Person = persons, DateBirth = persons.PerDateBirth, DNI = persons.PerDni, Email = persons.PerEmail };

            ViewData["CatDescription"] = _context.Categories.Where(x => x.CatId == view.Person.CatId).FirstOrDefault();

            ViewData["CatId"] = new SelectList(_context.Categories, "CatId", "CatDescription", persons.CatId);
            ViewData["Gender"] = new SelectList(LoadGender(), "Value", "Description", view.Person.PerGender);

            //ViewData["SubId"] = new SelectList(_context.Subsidiaries, "SubId", "SubId", persons.SubId);
            //ViewData["UsrId"] = new SelectList(_context.Users, "UsrId", "UsrId", persons.UsrId);
            return View(view);
        }

        // POST: Persons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, ViewPerson view)
        {
            if (id != view.Person.PerId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(view.Person);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PersonsExists(view.Person.PerId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Index");
            }
            ViewData["CatDescription"] = _context.Categories.Where(x => x.CatId == view.Person.CatId).FirstOrDefault();
            ViewData["CatId"] = new SelectList(_context.Categories, "CatId", "CatDescription", view.Person.CatId);
            ViewData["Gender"] = new SelectList(LoadGender(), "Value", "Description", view.Person.PerGender);

            return View(view);
        }

        // GET: Persons/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var persons = await _context.Persons
                .Include(p => p.Cat)
                .Include(p => p.Sub)
                //.Include(p => p.Usr)
                .SingleOrDefaultAsync(m => m.PerId == id);
            if (persons == null)
            {
                return NotFound();
            }

            return View(persons);
        }

        // POST: Persons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var persons = await _context.Persons.SingleOrDefaultAsync(m => m.PerId == id);
            _context.Persons.Remove(persons);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult ValidateEmailAddress(ViewPerson view)
        {
            if (view.PerId != null)
            {
                return Json(_context.Persons.Where(x => x.PerId != view.PerId).Any(x => x.PerEmail == view.Email) ?
                    string.Format("El correo ya est� en uso.", view.Email) : "true");
            }
            return Json(_context.Persons.Any(x => x.PerEmail == view.Email) ?
                    string.Format("El correo ya est� en uso.", view.Email) : "true");
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult ValidateDNI(ViewPerson view)
        {
            if (view.PerId != null)
            {
                return Json(_context.Persons.Where(x => x.PerId != view.PerId).Any(x => x.PerDni == view.DNI) ?
                                    string.Format("El DNI ya est� en uso.", view.DNI) : "true");
            }

            return Json(_context.Persons.Any(x => x.PerDni == view.DNI) ?
                    string.Format("El DNI ya est� en uso.", view.DNI) : "true");
        }


        public async Task<IActionResult> DeletePerson(string personid)
        {
            var persons = await _context.Persons.SingleOrDefaultAsync(m => m.PerId == personid && m.PerEnabled == true);
            persons.PerEnabled = false;
            await _context.SaveChangesAsync();
            return Json(string.Format(persons.FullName));
        }

        public PartialViewResult TrainingImages(string id)
        {
            var webRoot = _env.WebRootPath;
            string filePath = string.Empty;

            var list = new List<string>();

            var userDirectory = _context.Persons.FindAsync(id).Result.PerPath;

            foreach (string file in Directory.EnumerateFiles(
            $"{webRoot}/{userDirectory}",
            "*",
            SearchOption.AllDirectories)
            )
            {
                FileInfo info = new FileInfo(file);

                list.Add($"{userDirectory}/{info.Name}");

            }

            return PartialView("TrainingImages", list);
        }

        public async Task<PartialViewResult> Alarms(string id)
        {
            var alarm = await _context.Alarms.Where(x => x.PerId == id).Include(x => x.Sub).Include(x => x.Sub.Com).ToListAsync();

            return PartialView("Alarms", alarm);
        }

        private bool PersonsExists(string id)
        {
            return _context.Persons.Any(e => e.PerId == id);
        }
        
    }
}