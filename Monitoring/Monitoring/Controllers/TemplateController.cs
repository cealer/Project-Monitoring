using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Monitoring.Controllers
{
    public class TemplateController : Controller
    {
        public IActionResult loadsidemenu()
        {
            return PartialView("loadsidemenu");
        }

        public IActionResult loadfooter()
        {
            return PartialView("loadfooter");
        }

        public IActionResult loadmailmenu()
        {
            return PartialView("loadmailmenu");
        }

        public IActionResult loadmobilnavbar()
        {
            return PartialView("loadmobilnavbar");
        }

        public IActionResult loadmobilside()
        {
            return PartialView("loadmobilside");
        }

        public IActionResult loadnavbar()
        {
            return PartialView("loadnavbar");
        }

        public IActionResult loadthinmailmenu() { return PartialView("loadthinmailmenu"); }

        public IActionResult loadthinmenu() { return PartialView("loadthinmenu"); }

        public IActionResult photoeditor() { return PartialView("photoeditor"); }

    }
}
