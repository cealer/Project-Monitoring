using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities;
using Entities.Utilities;
using Newtonsoft.Json;
using Microsoft.ProjectOxford.Face;
using Entities.Models.API;
using Microsoft.ProjectOxford.Vision.Contract;
using Microsoft.ProjectOxford.Vision;
using Microsoft.ProjectOxford.Face.Contract;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;

namespace Monitoring.Controllers.Api
{
    //[Authorize]
    [Produces("application/json")]
    [Route("api/Recognitions")]
    public class RecognitionsController : Controller
    {
        private readonly MonitoringContext _context;
        private readonly Generate _Utilities;
        private readonly RecOperations _rec;
        IConfiguration _iconfiguration;

        public RecognitionsController(MonitoringContext context, IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _context = context;
            _Utilities = new Generate(context);
            _rec = new RecOperations(context, _iconfiguration);
        }

        [Route("GetA")]
        [HttpGet]
        public IActionResult Get()

        {

            var scopes = HttpContext.User.FindFirst("http://schemas.microsoft.com/identity/claims/scope")?.Value;

            if (!string.IsNullOrEmpty(Startup.ScopeRead) && scopes != null

                    && scopes.Split(' ').Any(s => s.Equals(Startup.ScopeRead)))

                return Ok(new string[] { "value1", "value2" });

            else

                return Unauthorized();

        }

        // GET: api/Recognitions
        [HttpGet]
        public IEnumerable<Recognitions> GetRecognitions()
        {
            return _context.Recognitions;
        }

        // GET: api/Recognitions/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRecognitions([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var recognitions = await _context.Recognitions.SingleOrDefaultAsync(m => m.RecId == id);

            if (recognitions == null)
            {
                return NotFound();
            }

            return Ok(recognitions);
        }

        [RequireHttps]
        [Route("GetRecognitionsByName/{person}")]
        public async Task<IActionResult> GetRecognitionsByName(string person)
        {

            var Per = await _context.Persons.Where(x => x.FullName.ToLower().Contains(person.ToLower())).FirstOrDefaultAsync();

            var recognitions = _context.Recognitions.Where(m => m.PerId == Per.PerId).Include(x => x.DescriptionRecs)
                .Include(x => x.Cam).Include(x => x.Sub)
                .Include(x => x.Sub.Com)
                .Include(x => x.Per)
                .OrderByDescending(x => x.RecId).FirstOrDefaultAsync().Result;

            if (recognitions == null)
            {
                return NotFound();
            }

            var response = new RecFaceResponse()
            {
                camDescription = recognitions.Cam.CamDescription,
                personFullName = recognitions.Per.FullName,
                personDNI = recognitions.Per.PerDni,
                recFullPath = recognitions.RecFullPath,
                recPath = recognitions.RecPath,
                recId = recognitions.RecId,
                subDescription = recognitions.Sub.SubDescription,
                descriptionRecs = recognitions.DescriptionRecs.ToList(),
                recDate = recognitions.RecDate,
                comDescription = recognitions.Sub.Com.ComDescription
            };

            return Ok(response);
        }

        private static async Task DescribeImage(string apiKey, string imageUrl)
        {
            var VisionServiceClient = new VisionServiceClient(apiKey);
            var analysisResult = await VisionServiceClient.DescribeAsync(imageUrl, 3);
            LogAnalysisResult(analysisResult);

        }

        private static void LogAnalysisResult(AnalysisResult result)

        {

            if (result == null)
            {
                return;

            }

            if (result.Categories != null && result.Categories.Length > 0)
            {
                string cat = "";
                foreach (var category in result.Categories)

                {
                    cat = "   Name : " + category.Name + "; Score : " + category.Score;

                }

            }

            if (result.Description != null)
            {
                string r = "";

                foreach (var caption in result.Description.Captions)
                {
                    r = "  Caption : " + caption.Text + "; Confidence : " + caption.Confidence;
                }

                string tags = "  Tags : ";

                foreach (var tag in result.Description.Tags)
                {
                    tags += tag + ", ";
                }

            }



            //if (result.Tags != null)

            //{

            //    Console.ForegroundColor = ConsoleColor.Cyan;

            //    Log("Tags : ");

            //    foreach (var tag in result.Tags)

            //    {

            //        Log("  Name : " + tag.Name + "; Confidence : " + tag.Confidence + "; Hint : " + tag.Hint);

            //    }

            //}



        }



        [HttpGet]
        [Route("TestGroup")]
        public async Task<IActionResult> CreateGroup()
        {
            try
            {
                const string subscriptionKey = "0147e10f8d264413a5e4b4c03a4b0aa4";
                var imageUrl = "https://appblobperson.blob.core.windows.net/peoplecontainer/personaDetectada_c9b3c0f4-72b7-11e7-9d47-24fd52b749d6";
                var faceServiceClient = new FaceServiceClient(subscriptionKey);
                string personGroupId = $"familia";



                await faceServiceClient.CreatePersonGroupAsync(personGroupId, "Familia Gonzales");

                // Define Cesar
                CreatePersonResult person = await faceServiceClient.CreatePersonAsync(
                    // Id of the person group that the person belonged to
                    personGroupId,
                    // Name of the person
                    "Cesar Gonzales-71993670"
                );

                // Detect faces in the image and add to Anna
                await faceServiceClient.AddPersonFaceAsync(
                    personGroupId, person.PersonId, imageUrl);

                await faceServiceClient.TrainPersonGroupAsync(personGroupId);

                TrainingStatus trainingStatus = null;
                while (true)
                {
                    trainingStatus = await faceServiceClient.GetPersonGroupTrainingStatusAsync(personGroupId);

                    if (trainingStatus.Status.ToString() != "running")
                    {
                        break;
                    }

                    await Task.Delay(1000);
                }
            }
            catch (Exception ex)
            {

                return Ok(ex.Message);
            }


            return Ok();
        }

        //[HttpGet]
        //[Route("TestIdentify")]
        //public async Task<IActionResult> Identify()
        //{
        //    var rec = await _rec.IdentifyPerson("https://appblobperson.blob.core.windows.net/peoplecontainer/9246f2f0-30f1-11e7-9157-24fd52b749d6.jpg.jpg");

        //    //const string subscriptionKey = "0147e10f8d264413a5e4b4c03a4b0aa4";
        //    //var imageUrl = "https://appblobperson.blob.core.windows.net/peoplecontainer/dc04c688-18a1-11e7-ac6c-e67613e0d9be.jpg";
        //    //var faceServiceClient = new FaceServiceClient(subscriptionKey);

        //    //var faces = await faceServiceClient.DetectAsync(imageUrl);
        //    //var faceIds = faces.Select(face => face.FaceId).ToArray();

        //    //string personGroupId = $"familia";

        //    //var results = await faceServiceClient.IdentifyAsync(personGroupId, faceIds);
        //    //foreach (var identifyResult in results)
        //    //{
        //    //    Console.WriteLine("Result of face: {0}", identifyResult.FaceId);
        //    //    if (identifyResult.Candidates.Length == 0)
        //    //    {
        //    //        return Ok("No one identified");
        //    //    }
        //    //    else
        //    //    {
        //    //        // Get top 1 among all candidates returned
        //    //        var candidateId = identifyResult.Candidates[0].PersonId;
        //    //        var person = await faceServiceClient.GetPersonAsync(personGroupId, candidateId);
        //    //        return Ok($"Identified as {person.Name}");
        //    //    }
        //    //}

        //    return Ok(rec);
        //}

        [HttpGet]
        [Route("TestPerson")]
        public async Task<IActionResult> Person()
        {

            //var imageUrl = "https://appblobperson.blob.core.windows.net/peoplecontainer/personaDetectada_c9b3c0f4-72b7-11e7-9d47-24fd52b749d6";

            //const string subscriptionKey = "0147e10f8d264413a5e4b4c03a4b0aa4";
            //const string uriBase = "https://westus.api.cognitive.microsoft.com/face/v1.0/detect";

            //var faceServiceClient = new FaceServiceClient(subscriptionKey);

            await _rec.DescribeImage("https://t1.uc.ltmcdn.com/images/9/6/8/img_como_bailar_vals_en_pareja_paso_a_paso_32869_300_square.jpg", "REC000000000000001");

            return Ok();
        }

        [HttpGet]
        [Route("TestRec")]
        public async Task<IActionResult> TestRec()
        {
            await _rec.DetectAlarm("PER000000000000002");

            return Ok();

            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri(uriBase);
            //    //client.DefaultRequestHeaders.Accept.Clear();
            //    //client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            //    // Request headers.
            //    client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

            //    // Request parameters. A third optional parameter is "details".
            //    string requestParameters = "returnFaceId=true&returnFaceLandmarks=false&returnFaceAttributes=age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise";

            //    // Assemble the URI for the REST API Call.
            //    string uri = uriBase + "?" + requestParameters;

            //    Uri photo = new Uri("http://127.0.0.1:10000/devstoreaccount1/peoplecontainer/personaDetectada_3dbe9dbe-6ffc-11e7-9504-24fd52b749d6");

            //    string json = "{\"url\":\"\"}";

            //    StringBuilder aux = new StringBuilder();
            //    aux.Append("{\"url\":");
            //    aux.Append("\"");
            //    aux.Append($"{photo.AbsoluteUri}");
            //    aux.Append("\"}");

            //    HttpContent content = new StringContent(aux.ToString());

            //    content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            //    HttpResponseMessage response = await client.PostAsync(uri, content);

            //    var res = response.Content.ReadAsStringAsync();


            //}

            //var json = JsonConvert.SerializeObject(faces);

            //var data = JsonConvert.DeserializeObject<List<FaceApiResponse>>(json);

            //await DescribeImage("4cd67ff2a8eb459a81eb945cf0877e7e", imageUrl);



            ////Save

            //return Ok(faces.ToList());
        }

        // POST: api/Recognitions
        [Route("SaveRecognition")]
        [HttpPost]
        public async Task<IActionResult> PostRecognitions([FromBody] Recognitions recognitions)
        {
            try
            {
                //Recognize Person
                ModelState.Clear();
                recognitions.RecId = await _Utilities.GenerateIdAsync("REC", "Detection.Recognitions");
                recognitions.RecDate = DateTime.Now;
                recognitions.SubId = _context.Camaras.FindAsync(recognitions.CamId).Result.SubId;
                recognitions.RecEnabled = true;
                var company = _context.Subsidiaries.Where(x => x.SubId == recognitions.SubId).FirstOrDefault();

                var rec = await _rec.IdentifyPerson(recognitions.RecPath, company.ComId.ToLower());

                recognitions.PerId = rec;

                TryValidateModel(recognitions);

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                _context.Recognitions.Add(recognitions);

                await _context.SaveChangesAsync();

                await _rec.DetectAlarm(recognitions.PerId);

                await _rec.DescribeImage(recognitions.RecFullPath, recognitions.RecId);

                return CreatedAtAction("GetRecognitions", new { id = recognitions.RecId }, recognitions);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        // DELETE: api/Recognitions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRecognitions([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var recognitions = await _context.Recognitions.SingleOrDefaultAsync(m => m.RecId == id);
            if (recognitions == null)
            {
                return NotFound();
            }

            _context.Recognitions.Remove(recognitions);
            await _context.SaveChangesAsync();

            return Ok(recognitions);
        }

        private bool RecognitionsExists(string id)
        {
            return _context.Recognitions.Any(e => e.RecId == id);
        }
    }
}