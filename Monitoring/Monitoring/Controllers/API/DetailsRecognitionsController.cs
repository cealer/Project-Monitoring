using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entities;
using Entities.Models.API;

namespace Monitoring.Controllers.Api
{
    [Produces("application/json")]
    [Route("api/DetailsRecognitions")]
    public class DetailsRecognitionsController : Controller
    {
        private readonly MonitoringContext _context;

        public DetailsRecognitionsController(MonitoringContext context)
        {
            _context = context;
        }

        // GET: api/DetailsRecognitions
        [HttpGet]
        public IEnumerable<DetailsRecognitions> GetDetailsRecognitions()
        {
            return _context.DetailsRecognitions.Include(x => x.Alm).Include(x => x.Obj).Include(x => x.Rec);
        }

        //[RequireHttps]
        //[Route("GetAssistance/{person}")]
        //public async Task<IActionResult> GetAssistance(string person)
        //{
        //    person = person.ToUpper();
        //    var per = await _context.DetailsRecognitions
        //        .Include(x => x.Alm).Include(x => x.Obj).Include(x => x.Rec).Include(x => x.Rec.Per).
        //        Include(x => x.Rec.Cam).Include(x => x.Rec.Sub).Include(x => x.Rec.Sub.Com)
        //        .Where(x => x.Rec.Per.FullName.Contains(person)).OrderByDescending(x => x.RecId).FirstOrDefaultAsync();

        //    var response = new RecResponse
        //    {
        //        Alm = per.Alm,
        //        AlmId = per.AlmId,
        //        DreDate = per.DreDate,
        //        DreEnabled = per.DreEnabled,
        //        DreId = per.DreId,
        //        DrePercentage = per.DrePercentage,
        //        Obj = per.Obj,
        //        ObjId = per.ObjId,
        //        Rec = per.Rec,
        //        RecId = per.RecId,
        //        Per = per.Rec.Per,
        //        Cam = per.Rec.Cam,
        //        Sub = per.Rec.Sub,
        //        Com = per.Rec.Sub.Com
        //    };

        //    return Ok(response);
        //}


        [RequireHttps]
        [Route("GetAssistance/{person}")]
        public async Task<IActionResult> GetAssistance(string person)
        {
            person = person.ToUpper();
            var per = await _context.DetailsRecognitions
                .Include(x => x.Alm).Include(x => x.Obj).Include(x => x.Rec).Include(x => x.Rec.Per).
                Include(x => x.Rec.Cam).Include(x => x.Rec.Sub).Include(x => x.Rec.Sub.Com)
                .Where(x => x.Rec.Per.FullName.Contains(person)).OrderByDescending(x => x.RecId).FirstOrDefaultAsync();

            var response = new RecResponse
            {
                Alm = per.Alm,
                AlmId = per.AlmId,
                DreDate = per.DreDate,
                DreEnabled = per.DreEnabled,
                DreId = per.DreId,
                DrePercentage = per.DrePercentage,
                Obj = per.Obj,
                ObjId = per.ObjId,
                Rec = per.Rec,
                RecId = per.RecId,
                Per = per.Rec.Per,
                Cam = per.Rec.Cam,
                Sub = per.Rec.Sub,
                Com = per.Rec.Sub.Com
            };

            return Ok(response);
        }

        // GET: api/DetailsRecognitions/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDetailsRecognitions([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var detailsRecognitions = await _context.DetailsRecognitions.SingleOrDefaultAsync(m => m.DreId == id);

            if (detailsRecognitions == null)
            {
                return NotFound();
            }

            return Ok(detailsRecognitions);
        }

        // PUT: api/DetailsRecognitions/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDetailsRecognitions([FromRoute] string id, [FromBody] DetailsRecognitions detailsRecognitions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != detailsRecognitions.DreId)
            {
                return BadRequest();
            }

            _context.Entry(detailsRecognitions).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DetailsRecognitionsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/DetailsRecognitions
        [HttpPost]
        public async Task<IActionResult> PostDetailsRecognitions([FromBody] DetailsRecognitions detailsRecognitions)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.DetailsRecognitions.Add(detailsRecognitions);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDetailsRecognitions", new { id = detailsRecognitions.DreId }, detailsRecognitions);
        }

        // DELETE: api/DetailsRecognitions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDetailsRecognitions([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var detailsRecognitions = await _context.DetailsRecognitions.SingleOrDefaultAsync(m => m.DreId == id);
            if (detailsRecognitions == null)
            {
                return NotFound();
            }

            _context.DetailsRecognitions.Remove(detailsRecognitions);
            await _context.SaveChangesAsync();

            return Ok(detailsRecognitions);
        }

        private bool DetailsRecognitionsExists(string id)
        {
            return _context.DetailsRecognitions.Any(e => e.DreId == id);
        }
    }
}