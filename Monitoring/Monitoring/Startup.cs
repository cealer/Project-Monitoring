﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Monitoring.Data;
using Monitoring.Models;
using Monitoring.Services;
using Entities;
using Monitoring.Class;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Serialization;
using Monitoring.Azure.B2C;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Threading.Tasks;
using System.Text;

namespace Monitoring
{
    public class Startup
    {
        public static string ScopeRead;

        public static string ScopeWrite;

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                //builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.Configure<AzureAdB2COptions>(Configuration.GetSection("Authentication:AzureAdB2C"));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //services.AddHangfire(x =>
            //{
            //    x.UseSqlServerStorage("Data Source=.;Initial Catalog=Monitoring;User ID=sa;Password=123;MultipleActiveResultSets=true");
            //    x.UseConsole();
            //});

            services.AddTransient<Entities.Utilities.Tools>();
            // Add framework services.
            #region Enforcing ssl
            services.Configure<MvcOptions>(options =>
            {

                options.Filters.Add(new RequireHttpsAttribute());
            });
            #endregion

            //services.AddDbContext<ApplicationDbContext>(options =>
            //    options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddDbContext<MonitoringContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            // Adds a default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = System.TimeSpan.FromHours(1);
                options.CookieHttpOnly = true;

            });

            //services.AddIdentity<ApplicationUser, IdentityRole>()
            //.AddEntityFrameworkStores<ApplicationDbContext>()
            //.AddDefaultTokenProviders();

            services.AddMvc();

            services.AddMvc()
    .AddJsonOptions(options =>
    {
        options.SerializerSettings.ReferenceLoopHandling =
            Newtonsoft.Json.ReferenceLoopHandling.Ignore;

        // use standard name conversion of properties
        options.SerializerSettings.ContractResolver =
            new CamelCasePropertyNamesContractResolver();

        //// include $id property in the output
        //options.SerializerSettings.PreserveReferencesHandling =
        //    PreserveReferencesHandling.Objects;

    });

            //  services.AddMvc().AddWebApiConventions();

            services.AddSignalR();

            //Get appsetings.json values
            services.AddSingleton<IConfiguration>(Configuration);

            // Add application services.
            //services.AddTransient<ManageUsers>();
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();

            services.AddAuthentication(
                sharedOptions => sharedOptions.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme);

            services.AddSingleton<IConfigureOptions<OpenIdConnectOptions>, OpenIdConnectOptionsSetup>();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory/*, ManageUsers seeder*/)
        {
            //app.UseHangfireServer();
            //app.UseHangfireDashboard();
            //GlobalConfiguration.Configuration.UseSqlServerStorage("Data Source=DESKTOP-UF9MHHN;Initial Catalog=Monitoring;User ID=sa;Password=123;MultipleActiveResultSets=true").UseConsole();
            //seeder.CreateAdminUser();
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseSignalR();

            app.UseSession();

            //app.UseIdentity();

            app.UseCookieAuthentication();

            app.UseOpenIdConnectAuthentication();

            app.UseJwtBearerAuthentication(new JwtBearerOptions
            {
                Authority = string.Format("https://login.microsoftonline.com/tfp/{0}/{1}/v2.0/",
          Configuration["Authentication:AzureAdB2C:Tenant"], Configuration["Authentication:AzureAdB2C:SignUpSignInPolicyId"]),
                Audience = Configuration["Authentication:AzureAdB2C:ClientId"],
                Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = AuthenticationFailed
                }
            });

            // Add external authentication middleware below. To configure them please see https://go.microsoft.com/fwlink/?LinkID=532715

            ScopeRead = Configuration["Authentication:AzureAdB2C:ScopeRead"];

            ScopeWrite = Configuration["Authentication:AzureAdB2C:ScopeWrite"];

            app.UseMvc(routes =>
            {

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }


        private Task AuthenticationFailed(AuthenticationFailedContext arg)

        {

            // For debugging purposes only!

            var s = $"AuthenticationFailed: {arg.Exception.Message}";

            arg.Response.ContentLength = s.Length;

            arg.Response.Body.Write(Encoding.UTF8.GetBytes(s), 0, s.Length);

            return Task.FromResult(0);

        }
    }
}