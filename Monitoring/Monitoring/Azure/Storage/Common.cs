﻿using System;
using Microsoft.WindowsAzure.Storage;

namespace Monitoring.Azure.Storage
{



    /// <summary>

    /// Contains public method for validating the storage connection string.

    /// </summary>

    public static class Common

    {

        /// <summary>

        /// Validates the connection string information in app.config and throws an exception if it looks like 

        /// the user hasn't updated this to valid values. 

        /// </summary>

        /// <returns>CloudStorageAccount object</returns>
        
        public static CloudStorageAccount CreateStorageAccountFromConnectionString(string stringConnection)
        {

            CloudStorageAccount storageAccount;

            const string Message = "Invalid storage account information provided. Please confirm the AccountName and AccountKey are valid in the app.config file - then restart the sample.";



            try

            {

                storageAccount = CloudStorageAccount.Parse(stringConnection);

            }

            catch (FormatException)

            {

                Console.WriteLine(Message);

                Console.ReadLine();

                throw;

            }

            catch (ArgumentException)

            {

                Console.WriteLine(Message);

                Console.ReadLine();

                throw;

            }



            return storageAccount;

        }

    }
}
