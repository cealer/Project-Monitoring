﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Monitoring.Azure.Storage
{
    public class Blob_Operations
    {
        /// <summary>

        /// Returns the container URI.  

        /// </summary>

        /// <param name="containerName">The container name</param>

        /// <returns>A URI for the container.</returns>

        public static Uri GetContainerUri(string account, string containerName)
        {
            // Retrieve storage account information from connection string
            CloudStorageAccount storageAccount = Common.CreateStorageAccountFromConnectionString(account);
            return storageAccount.CreateCloudBlobClient().GetContainerReference(containerName).Uri;
        }

        public async Task<List<string>> GetBlobs(string containerName, string account)
        {
            List<string> blobs = new List<string>();
            // Retrieve storage account information from connection string

            CloudStorageAccount storageAccount = Common.CreateStorageAccountFromConnectionString(account);

            // Create a blob client for interacting with the blob service.

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

            CloudBlobContainer container = blobClient.GetContainerReference(containerName);

            BlobContinuationToken token = null;

            do
            {
                BlobResultSegment resultSegment = await container.ListBlobsSegmentedAsync(token);
                token = resultSegment.ContinuationToken;

                foreach (IListBlobItem item in resultSegment.Results)
                {
                    blobs.Add(item.Uri.AbsoluteUri);
                    //    if (item.GetType() == typeof(CloudBlockBlob))
                    //    {
                    //        CloudBlockBlob blob = (CloudBlockBlob)item;
                    //        Console.WriteLine("Block blob of length {0}: {1}", blob.Properties.Length, blob.Uri);
                    //    }

                    //    else if (item.GetType() == typeof(CloudPageBlob))
                    //    {
                    //        CloudPageBlob pageBlob = (CloudPageBlob)item;

                    //        Console.WriteLine("Page blob of length {0}: {1}", pageBlob.Properties.Length, pageBlob.Uri);
                    //    }

                    //    else if (item.GetType() == typeof(CloudBlobDirectory))
                    //    {
                    //        CloudBlobDirectory directory = (CloudBlobDirectory)item;

                    //        Console.WriteLine("Directory: {0}", directory.Uri);
                    //    }
                }
            } while (token != null);

            //foreach (IListBlobItem blob in container.)

            //{

            //    // Blob type will be CloudBlockBlob, CloudPageBlob or CloudBlobDirectory

            //    // Use blob.GetType() and cast to appropriate type to gain access to properties specific to each type
            //    blobs.Add(blob.Uri.AbsoluteUri);
            //    //Console.WriteLine("- {0} (type: {1})", blob.Uri, blob.GetType());

            //}
            return blobs;
        }

        public async Task<string> UploadPhotoPerson(IFormFile imageToUpload, string account)
        {
            string imageFullPath = null;
            try
            {
                if (imageToUpload == null || imageToUpload.Length == 0)
                {
                    return null;
                }

                CloudStorageAccount storageAccount = Common.CreateStorageAccountFromConnectionString(account);

                // Create a blob client for interacting with the blob service.

                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                CloudBlobContainer container = blobClient.GetContainerReference("personphotoupload");

                if (await container.CreateIfNotExistsAsync())
                {
                    await container.SetPermissionsAsync(
                    new BlobContainerPermissions
                    {
                        PublicAccess = BlobContainerPublicAccessType.Blob
                    }
                    );
                }

                string imageName = Guid.NewGuid().ToString() + "-" + Path.GetExtension(imageToUpload.FileName);

                CloudBlockBlob cloudBlockBlob = container.GetBlockBlobReference(imageName);
                cloudBlockBlob.Properties.ContentType = imageToUpload.ContentType;


                // Create or overwrite the blob with the contents of a local file 
                using (var fileStream = imageToUpload.OpenReadStream())
                {
                    await cloudBlockBlob.UploadFromStreamAsync(fileStream);
                }

                imageFullPath = cloudBlockBlob.Uri.ToString();
            }
            catch (Exception ex)
            {
            }
            return imageFullPath;

        }

    }
}
