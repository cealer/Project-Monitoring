﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Models
{
    public class TokenEmbed
    {
        [JsonProperty(PropertyName = "@odata.context")]
        public string odata_context { get; set; }
        public string token { get; set; }
        public string tokenId { get; set; }
        public string expiration { get; set; }

    }
}
