﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Models
{
    public class Report
    {
        [JsonProperty(PropertyName = "@odata.context")]
        public string odata_context { get; set; }
        public List<Value> value { get; set; }
        [JsonIgnore]
        public string token { get; set; }
    }
}