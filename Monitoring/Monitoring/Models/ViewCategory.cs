﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc;

namespace Monitoring.Models
{
    public class ViewCategory
    {
        public Categories Category { get; set; }

        [Required]
        [Display(Name = "Descripción")]
        [Remote("ValidateDescription", "Categories", AdditionalFields = "CatId")]
        public string Cat { get; set; }

        public string CatId { get; set; }

    }
}
