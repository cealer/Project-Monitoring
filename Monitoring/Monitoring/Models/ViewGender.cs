﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Models
{
    public class ViewGender
    {
        public string Description { get; set; }
        public string Value { get; set; }
    }
}
