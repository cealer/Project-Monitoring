﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;

namespace Monitoring.Models
{
    public class viewDetailsPerson
    {
        public Persons Person { get; set; }
        public List<DetailsRecognitions> DetailsRecognitions { get; set; }
        public List<Recognitions> Recognition { get; set; }

        public int CountDetailRecognitions { get; set; }
    }
}
