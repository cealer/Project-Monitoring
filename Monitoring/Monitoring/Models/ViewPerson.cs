﻿using Entities;
using Entities.Validation;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Models
{
    public class ViewPerson
    {
        public Persons Person { get; set; }

        //Campos para validar por medio de validacion Remote

        [Required(ErrorMessage = "Campo obligatorio")]
        [Display(Name = "Email")]
        [MaxLength(50, ErrorMessage = "No puede exceder los {1} caractéres")]
        [MinLength(2, ErrorMessage = "El campo {0} debe contener al menos {1} caractéres")]
        [Remote("ValidateEmailAddress", "People", AdditionalFields = "PerId")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Remote("ValidateDNI", "People", AdditionalFields = "PerId")]
        [Required(ErrorMessage = "Campo obligatorio")]
        [Display(Name = "DNI")]
        [MinLength(8, ErrorMessage = "No puede exceder los {1} caractéres")]
        [MaxLength(8, ErrorMessage = "El campo {0} debe contener al menos {1} caractéres")]
        public string DNI { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime? DateBirth { get; set; }

        //Solo para la validacion remote

        public string PerId { get; set; }
    }
}
