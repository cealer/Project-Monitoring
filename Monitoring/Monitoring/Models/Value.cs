﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Models
{
    public class Value
    {
        public string id { get; set; }
        public int modelId { get; set; }
        public string name { get; set; }
        public string webUrl { get; set; }
        public string embedUrl { get; set; }
        public bool isOwnedByMe { get; set; }
        public bool isOriginalPbixReport { get; set; }
        public string datasetId { get; set; }

    }
}