﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Models.PowerBi
{
    public class Report
    {
        public string id { get; set; }
        public string embedUrl { get; set; }
        public EmbedToken embedToken { get; set; }
        public int minutesToExpiration { get; set; }
        public object errorMessage { get; set; }
    }
}