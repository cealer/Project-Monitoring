﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Models.PowerBi
{
    public class EmbedToken
    {
        public string token { get; set; }
        public string tokenId { get; set; }
        public string expiration { get; set; }

    }
}
