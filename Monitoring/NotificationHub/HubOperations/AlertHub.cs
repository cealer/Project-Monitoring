﻿using Microsoft.Azure.NotificationHubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotificationHub.HubOperations
{
    public class AlertHub
    {
        public static NotificationHubClient hub;

        public static async void SendNotificationAsync(string message, string connectionString, string hubName)
        {
            hub = NotificationHubClient.CreateClientFromConnectionString(connectionString, hubName);
            await hub.SendGcmNativeNotificationAsync("{ \"data\" : {\"message\":\"" + message + "\"}}");
        }
    }
}
