﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using NotificationHub.HubOperations;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.Rest;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using Microsoft.PowerBI.Api.V2;
using NotificationHub.Power_BI;
using Microsoft.PowerBI.Api.V2.Models;
using RestSharp;
using NotificationHub.Power_BI.ADD;

namespace NotificationHub.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        IConfiguration _iconfiguration;

        public ValuesController(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        [HttpGet]
        [Route("TestRec/{message}")]
        public async Task<IActionResult> TestRec(string message)
        {
            string connectionString = _iconfiguration.GetSection("HubNotification").GetSection("ConnectionStrings").Value;
            string hubName = _iconfiguration.GetSection("HubNotification").GetSection("HubName").Value;

            AlertHub.SendNotificationAsync(message, connectionString, hubName);

            return Ok();
        }

        [HttpGet]
        [Route("Reports")]
        public async Task<IActionResult> EmbedReports()
        {
            string ApiUrl = _iconfiguration.GetSection("PowerBi").GetSection("apiUrl").Value;
            string clientId = _iconfiguration.GetSection("PowerBi").GetSection("ClientID").Value;
            string PowerBiAPI = _iconfiguration.GetSection("PowerBi").GetSection("PowerBiAPI").Value;
            string username = _iconfiguration.GetSection("PowerBi").GetSection("Username").Value;
            string password = _iconfiguration.GetSection("PowerBi").GetSection("Password").Value;
            string ClientSecret = _iconfiguration.GetSection("PowerBi").GetSection("ClientSecret").Value;
            string grant_type = _iconfiguration.GetSection("PowerBi").GetSection("grant_type").Value;
            string GroupId = _iconfiguration.GetSection("PowerBi").GetSection("groupId").Value;
            string resourceUrl = _iconfiguration.GetSection("PowerBi").GetSection("resourceUrl").Value;
            
            var Restclient = new RestClient("https://login.microsoftonline.com/common/oauth2/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/x-www-form-urlencoded");
            request.AddParameter("application/x-www-form-urlencoded", $"grant_type=password&username={username}&password={password}&resource={resourceUrl}&client_id={clientId}&client_secret=M0gIsaHQFhBJSa529Eo%2F2A%2FqpOjjrg4gcT%2BNu6l6Kg4%3D", ParameterType.RequestBody);
            IRestResponse response = Restclient.Execute(request);

            var result = response.Content;
            var AADs = JsonConvert.DeserializeObject<AAD>(result);

            var tokenCredentials = new TokenCredentials(AADs.access_token, "Bearer");

            // Create a Power BI Client object. It will be used to call Power BI APIs.
            using (var client = new PowerBIClient(new Uri(ApiUrl), tokenCredentials))
            {
                // Get a list of reports.
                var reports = await client.Reports.GetReportsInGroupAsync(GroupId);

                // Get the first report in the group.
                var report = reports.Value;

                if (report == null)
                {
                    return View(new EmbedConfig()
                    {
                        ErrorMessage = "Group has no reports."
                    });
                }

                // Generate Embed Token.
                var generateTokenRequestParameters = new GenerateTokenRequest(accessLevel: "view");
                var tokenResponse = await client.Reports.GenerateTokenInGroupAsync(GroupId, report[2].Id, generateTokenRequestParameters);

                if (tokenResponse == null)
                {
                    return View(new EmbedConfig()
                    {
                        ErrorMessage = "Failed to generate embed token."
                    });
                }

                // Generate Embed Configuration.
                var embedConfig = new EmbedConfig()
                {
                    EmbedToken = tokenResponse,
                    EmbedUrl = report[2].EmbedUrl,
                    Id = report[2].Id
                };

                return Ok(embedConfig);
            }
        }

        public IActionResult Redirect()
        {
            return View();

        }

        [HttpGet]
        [Route("Auth")]
        public async Task<IActionResult> Auth()
        {
            //using (var client = new HttpClient())
            //{
            //    //string tenant = _iconfiguration.GetSection("Authentication").GetSection("AzureAdB2C").GetSection("Tenant").Value;
            //    string tenant = "cesargonzalesaguijehotmail";
            //    string uri = $"https://login.windows.net/common/oauth2/authorize/";
            //    client.BaseAddress = new Uri(uri);

            //    MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded");
            //    MediaTypeWithQualityHeaderValue format = new MediaTypeWithQualityHeaderValue("application/json");

            //    client.DefaultRequestHeaders.Accept.Add(format);
            //    client.DefaultRequestHeaders.Accept.Add(contentType);

            //    HttpClientHandler handler = new HttpClientHandler();
            //    HttpClient httpClient = new HttpClient(handler);

            //    //var requestUri = QueryHelpers.AddQueryString(uri, "grant_type", "password");
            //    //requestUri = QueryHelpers.AddQueryString(uri, "resource", $"{PowerBiAPI}");
            //    //requestUri = QueryHelpers.AddQueryString(uri, "username", $"{username}");
            //    //requestUri = QueryHelpers.AddQueryString(uri, "password", $"{password}");
            //    //requestUri = QueryHelpers.AddQueryString(uri, "client_id", $"{clientId}");
            //    //requestUri = QueryHelpers.AddQueryString(uri, "client_secret", $"{ClientSecret}");

            //    var obj = new Informatio
            //    {
            //        grant_type = "password",
            //        client_id = "b5254252-1876-40a5-862c-f31b1e28b28e",
            //        client_secret = "bJDWhml8hiPEGuY0uWQXwqyHjL9PY57ey6YVQLp6vTk="
            //    ,
            //        password = "@@AskS81928SADJ",
            //        resource = uri
            //    };

            //    HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, uri);
            //    string jsonBody = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            //    request.Content = new StringContent(jsonBody);
            //    HttpResponseMessage response = await httpClient.SendAsync(request);

            //    var result = response.Content.ReadAsStringAsync().Result;
            //    var AADs = JsonConvert.DeserializeObject<AAD>(result);



            //    //HttpResponseMessage res = client.GetAsync($"api/values/TestRec/{findAlarm.AlmDescription}").Result;
            //}

            // Create a user password cradentials.
            var credential = new UserPasswordCredential("powerbiembeded@cesargonzalesaguijehotmail.onmicrosoft.com", "@@AskS81928SADJ");

            // Authenticate using created credentials
            var authenticationContext = new AuthenticationContext("https://login.windows.net/common/oauth2/authorize/");
            var authenticationResult = await authenticationContext.AcquireTokenAsync("https://analysis.windows.net/powerbi/api", "b5254252-1876-40a5-862c-f31b1e28b28e", credential);

            //if (authenticationResult == null)
            //{
            //    return View(new EmbedConfig()
            //    {
            //        ErrorMessage = "Authentication Failed."
            //    });
            //}

            var tokenCredentials = new TokenCredentials(authenticationResult.AccessToken, "Bearer");

            return Ok();
        }

        // GET api/values
        [HttpGet]
        public async Task<IEnumerable<string>> Get()
        {
            

            // Create a user password cradentials.
            var credential = new UserPasswordCredential("powerbiembeded@cesargonzalesaguijehotmail.onmicrosoft.com", "@@AskS81928SADJ");

            // Authenticate using created credentials
            var authenticationContext = new AuthenticationContext("https://login.windows.net/common/oauth2/authorize");
            var authenticationResult = await authenticationContext.AcquireTokenAsync("https://analysis.windows.net/powerbi/api", "b5254252-1876-40a5-862c-f31b1e28b28e", credential);

            //if (authenticationResult == null)
            //{
            //    return View(new EmbedConfig()
            //    {
            //        ErrorMessage = "Authentication Failed."
            //    });
            //}

            var tokenCredentials = new TokenCredentials(authenticationResult.AccessToken, "Bearer");

            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {

        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
