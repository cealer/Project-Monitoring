﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RealTimeRecognition.Startup))]
namespace RealTimeRecognition
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
