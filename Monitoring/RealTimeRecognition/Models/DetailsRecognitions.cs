﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace RealTimeRecognition.Models
{
    public partial class DetailsRecognitions
    {
        public string Rec_Path { get; set; }
        public string Alm_Description { get; set; }
        public string Cam_Description { get; set; }
        public string Per_LastName { get; set; }
        public string Per_Name { get; set; }
        public decimal Rec_Percentage { get; set; }
        public string Obj_Description { get; set; }
        public decimal Dre_Percentage { get; set; }
        public string Rec_Date { get; set; }
        public string Rec_Time { get; set; }
    }
}