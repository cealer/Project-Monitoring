﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Speech.Synthesis;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace RealTimeRecognition.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public async Task<ActionResult> Voice(string text)
        {
            Task<FileContentResult> task = Task.Run(() =>
            {
                using (var synth = new SpeechSynthesizer())
                {
                    synth.SelectVoice("Microsoft Sabina Desktop");

                    using (var stream = new MemoryStream())
                    {
                        synth.SetOutputToWaveStream(stream);
                        synth.Speak(text);
                        byte[] bytes = stream.GetBuffer();
                        return File(bytes, "audio/x-wav", "callrecording.mp3");
                    }
                }
            });

            return await task;
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}