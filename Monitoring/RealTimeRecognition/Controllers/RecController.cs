﻿using RealTimeRecognition.Hubs;
using RealTimeRecognition.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RealTimeRecognition.Controllers
{
    public class RecController : ApiController
    {
        RecRepository objRepo = new RecRepository();

        // GET api/values
        public IEnumerable<DetailsRecognitions> Get()
        {
            return objRepo.GetData();
        }
    }
}