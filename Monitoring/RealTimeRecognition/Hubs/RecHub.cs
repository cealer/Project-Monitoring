﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RealTimeRecognition.Hubs
{
    [HubName("RecHub")]
    public class RecHub : Hub
    {

        public static void Show()
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<RecHub>();
            context.Clients.All.displayStatus();
        }
    }
}