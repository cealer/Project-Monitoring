﻿using RealTimeRecognition.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace RealTimeRecognition.Hubs
{
    public class RecRepository
    {
        public IEnumerable<DetailsRecognitions> GetData()
        {

            using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(@"SELECT 
	   [Recognitions].[Rec_Path],
       [Alarms].[Alm_Description],
       [Camaras].Cam_Description,
       [Persons].Per_LastName,
	   [Persons].Per_Name,
	   [Recognitions].Rec_Percentage,	
	   [XObjects].[Obj_Description],
	   [DetailsRecognitions].[Dre_Percentage]
       [Dre_Percentage],
       [Rec_Date]
  FROM [Monitoring].[Detection].[DetailsRecognitions]
  INNER JOIN Detection.Recognitions on Detection.DetailsRecognitions.Rec_Id=Detection.Recognitions.Rec_Id
  INNER JOIN Register.Persons on Register.Persons.Per_Id=Recognitions.Per_Id
  INNER JOIN Register.XObjects on Register.XObjects.Obj_Id=DetailsRecognitions.Obj_Id
  INNER JOIN Register.Camaras on Register.Camaras.Cam_Id=Recognitions.Cam_Id
  INNER JOIN Register.Alarms on Register.Alarms.Alm_Id=DetailsRecognitions.Alm_Id
  where convert(Date,Recognitions.Rec_Date)= CONVERT(Date,GETDATE())", connection))
                {
                    // Make sure the command object does not already have
                    // a notification object associated with it.
                    command.Notification = null;

                    SqlDependency dependency = new SqlDependency(command);
                    dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

                    if (connection.State == ConnectionState.Closed)
                        connection.Open();

                    using (var reader = command.ExecuteReader())
                        return reader.Cast<IDataRecord>()
                        .Select(x => new DetailsRecognitions()
                        {
                            Rec_Path = x.GetString(0),
                            Alm_Description = x.GetString(1),
                            Cam_Description = x.GetString(2),
                            Per_LastName = x.GetString(3),
                            Per_Name = x.GetString(4),
                            Rec_Percentage = x.GetDecimal(5) * 100,
                            Obj_Description = x.GetString(6),
                            Dre_Percentage = x.GetDecimal(7) * 100,
                            Rec_Date = x.GetDateTime(8).ToShortDateString(),
                            Rec_Time = x.GetDateTime(8).ToShortTimeString()
                        }).ToList();
                }
            }
        }

        private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            RecHub.Show();
        }

    }
}