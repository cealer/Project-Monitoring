﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Agregandoalertasareconocimientostable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Alm_Id",
                schema: "Detection",
                table: "Recognitions",
                type: "char(18)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Recognitions_Alm_Id",
                schema: "Detection",
                table: "Recognitions",
                column: "Alm_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Recognitions_Alarms_Alm_Id",
                schema: "Detection",
                table: "Recognitions",
                column: "Alm_Id",
                principalSchema: "Register",
                principalTable: "Alarms",
                principalColumn: "Alm_Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Recognitions_Alarms_Alm_Id",
                schema: "Detection",
                table: "Recognitions");

            migrationBuilder.DropIndex(
                name: "IX_Recognitions_Alm_Id",
                schema: "Detection",
                table: "Recognitions");

            migrationBuilder.DropColumn(
                name: "Alm_Id",
                schema: "Detection",
                table: "Recognitions");
        }
    }
}
