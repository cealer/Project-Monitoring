﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Normalizaciontablausuario : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Users_Subsidiaries_Sub_Id",
            //    schema: "Register",
            //    table: "Users");

            //migrationBuilder.DropIndex(
            //    name: "IX_Users_Sub_Id",
            //    schema: "Register",
            //    table: "Users");

            migrationBuilder.DropColumn(
                name: "Sub_Id",
                schema: "Register",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Usr_Date",
                schema: "Register",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Usr_DateBirth",
                schema: "Register",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Usr_Dni",
                schema: "Register",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Usr_Email",
                schema: "Register",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Usr_FirstName",
                schema: "Register",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Usr_Gender",
                schema: "Register",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Usr_LastName",
                schema: "Register",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Usr_Phone",
                schema: "Register",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Usr_Photo",
                schema: "Register",
                table: "Users");

            //migrationBuilder.CreateIndex(
            //    name: "Usr_Username",
            //    schema: "Register",
            //    table: "Users",
            //    column: "Usr_Username",
            //    unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "Usr_Username",
                schema: "Register",
                table: "Users");

            migrationBuilder.AddColumn<string>(
                name: "Sub_Id",
                schema: "Register",
                table: "Users",
                type: "char(18)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Usr_Date",
                schema: "Register",
                table: "Users",
                type: "datetime",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Usr_DateBirth",
                schema: "Register",
                table: "Users",
                type: "datetime",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Usr_Dni",
                schema: "Register",
                table: "Users",
                type: "char(8)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Usr_Email",
                schema: "Register",
                table: "Users",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Usr_FirstName",
                schema: "Register",
                table: "Users",
                maxLength: 50,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Usr_Gender",
                schema: "Register",
                table: "Users",
                type: "char(1)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Usr_LastName",
                schema: "Register",
                table: "Users",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Usr_Phone",
                schema: "Register",
                table: "Users",
                maxLength: 20,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Usr_Photo",
                schema: "Register",
                table: "Users",
                type: "char(11)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_Sub_Id",
                schema: "Register",
                table: "Users",
                column: "Sub_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Subsidiaries_Sub_Id",
                schema: "Register",
                table: "Users",
                column: "Sub_Id",
                principalSchema: "Register",
                principalTable: "Subsidiaries",
                principalColumn: "Sub_Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
