﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class CambiandonombredeEstado : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Enabled",
                schema: "Register",
                table: "XObjects",
                newName: "Obj_Enabled");

            migrationBuilder.RenameColumn(
                name: "Enabled",
                schema: "Register",
                table: "Users",
                newName: "Usr_Enabled");

            migrationBuilder.RenameColumn(
                name: "Enabled",
                schema: "Detection",
                table: "Training",
                newName: "Tra_Enabled");

            migrationBuilder.RenameColumn(
                name: "Enabled",
                schema: "Register",
                table: "Subsidiaries",
                newName: "Sub_Enabled");

            migrationBuilder.RenameColumn(
                name: "Enabled",
                schema: "Detection",
                table: "Recognitions",
                newName: "Rec_Enabled");

            migrationBuilder.RenameColumn(
                name: "Enabled",
                schema: "Register",
                table: "Persons",
                newName: "Per_Enabled");

            migrationBuilder.RenameColumn(
                name: "Enabled",
                schema: "Register",
                table: "NeuralNetworks",
                newName: "Neu_Enabled");

            migrationBuilder.RenameColumn(
                name: "Enabled",
                schema: "Register",
                table: "Companies",
                newName: "Com_Enabled");

            migrationBuilder.RenameColumn(
                name: "Enabled",
                schema: "Register",
                table: "Categories",
                newName: "Cat_Enabled");

            migrationBuilder.RenameColumn(
                name: "Enabled",
                schema: "Register",
                table: "Camaras",
                newName: "Cam_Enabled");

            migrationBuilder.RenameColumn(
                name: "Enabled",
                schema: "Register",
                table: "Alarms",
                newName: "Alm_Enabled");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Obj_Enabled",
                schema: "Register",
                table: "XObjects",
                newName: "Enabled");

            migrationBuilder.RenameColumn(
                name: "Usr_Enabled",
                schema: "Register",
                table: "Users",
                newName: "Enabled");

            migrationBuilder.RenameColumn(
                name: "Tra_Enabled",
                schema: "Detection",
                table: "Training",
                newName: "Enabled");

            migrationBuilder.RenameColumn(
                name: "Sub_Enabled",
                schema: "Register",
                table: "Subsidiaries",
                newName: "Enabled");

            migrationBuilder.RenameColumn(
                name: "Rec_Enabled",
                schema: "Detection",
                table: "Recognitions",
                newName: "Enabled");

            migrationBuilder.RenameColumn(
                name: "Per_Enabled",
                schema: "Register",
                table: "Persons",
                newName: "Enabled");

            migrationBuilder.RenameColumn(
                name: "Neu_Enabled",
                schema: "Register",
                table: "NeuralNetworks",
                newName: "Enabled");

            migrationBuilder.RenameColumn(
                name: "Com_Enabled",
                schema: "Register",
                table: "Companies",
                newName: "Enabled");

            migrationBuilder.RenameColumn(
                name: "Cat_Enabled",
                schema: "Register",
                table: "Categories",
                newName: "Enabled");

            migrationBuilder.RenameColumn(
                name: "Cam_Enabled",
                schema: "Register",
                table: "Camaras",
                newName: "Enabled");

            migrationBuilder.RenameColumn(
                name: "Alm_Enabled",
                schema: "Register",
                table: "Alarms",
                newName: "Enabled");
        }
    }
}
