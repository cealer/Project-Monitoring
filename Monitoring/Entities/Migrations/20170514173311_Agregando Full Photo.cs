﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class AgregandoFullPhoto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Rec_PathFull",
                schema: "Detection",
                table: "Recognitions",
                nullable: true);

            //migrationBuilder.CreateIndex(
            //    name: "IX_Recognitions_Per_Id",
            //    schema: "Detection",
            //    table: "Recognitions",
            //    column: "Per_Id");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Recognitions_Persons_Per_Id",
            //    schema: "Detection",
            //    table: "Recognitions",
            //    column: "Per_Id",
            //    principalSchema: "Register",
            //    principalTable: "Persons",
            //    principalColumn: "Per_Id",
            //    onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Recognitions_Persons_Per_Id",
                schema: "Detection",
                table: "Recognitions");

            migrationBuilder.DropIndex(
                name: "IX_Recognitions_Per_Id",
                schema: "Detection",
                table: "Recognitions");

            migrationBuilder.DropColumn(
                name: "Rec_PathFull",
                schema: "Detection",
                table: "Recognitions");
        }
    }
}
