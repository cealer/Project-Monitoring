﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Cambiandomodeloobjeto4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Obj_Path",
                schema: "Register",
                table: "XObjects",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 50);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Obj_Path",
                schema: "Register",
                table: "XObjects",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
