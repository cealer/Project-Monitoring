﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Agregandofkpersonaenusuario : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Per_Id",
                schema: "Register",
                table: "Users",
                type: "char(18)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Per_Id",
                schema: "Register",
                table: "Users",
                column: "Per_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Users_Persons_Per_Id",
                schema: "Register",
                table: "Users",
                column: "Per_Id",
                principalSchema: "Register",
                principalTable: "Persons",
                principalColumn: "Per_Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Persons_Per_Id",
                schema: "Register",
                table: "Users");

            migrationBuilder.DropIndex(
                name: "IX_Users_Per_Id",
                schema: "Register",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Per_Id",
                schema: "Register",
                table: "Users");
        }
    }
}
