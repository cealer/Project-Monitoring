﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Agregandomodelodescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Rec_Percentage",
                schema: "Detection",
                table: "Recognitions");

            migrationBuilder.CreateTable(
                name: "DescriptionRec",
                schema: "Detection",
                columns: table => new
                {
                    Des_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Rec_Description = table.Column<string>(type: "nvarchar(250)", nullable: false),
                    Rec_Id = table.Column<string>(type: "char(18)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKDescription", x => x.Des_Id);
                    table.ForeignKey(
                        name: "FK_DescriptionRec_Recognitions_Rec_Id",
                        column: x => x.Rec_Id,
                        principalSchema: "Detection",
                        principalTable: "Recognitions",
                        principalColumn: "Rec_Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DescriptionRec_Rec_Id",
                schema: "Detection",
                table: "DescriptionRec",
                column: "Rec_Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DescriptionRec",
                schema: "Detection");

            migrationBuilder.AddColumn<decimal>(
                name: "Rec_Percentage",
                schema: "Detection",
                table: "Recognitions",
                type: "decimal",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
