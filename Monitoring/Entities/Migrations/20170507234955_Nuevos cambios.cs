﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Nuevoscambios : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Alm_Id",
                schema: "Detection",
                table: "DetailsRecognitions",
                type: "char(18)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DetailsRecognitions_Alm_Id",
                schema: "Detection",
                table: "DetailsRecognitions",
                column: "Alm_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_DetailsRecognitions_Alarms_Alm_Id",
                schema: "Detection",
                table: "DetailsRecognitions",
                column: "Alm_Id",
                principalSchema: "Register",
                principalTable: "Alarms",
                principalColumn: "Alm_Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DetailsRecognitions_Alarms_Alm_Id",
                schema: "Detection",
                table: "DetailsRecognitions");

            migrationBuilder.DropIndex(
                name: "IX_DetailsRecognitions_Alm_Id",
                schema: "Detection",
                table: "DetailsRecognitions");

            migrationBuilder.DropColumn(
                name: "Alm_Id",
                schema: "Detection",
                table: "DetailsRecognitions");
        }
    }
}
