﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Agregandoestadosenlosmodelos : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                schema: "Register",
                table: "XObjects",
                nullable: true,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                schema: "Register",
                table: "Users",
                nullable: true,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                schema: "Detection",
                table: "Training",
                nullable: true,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                schema: "Register",
                table: "Subsidiaries",
                nullable: true,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                schema: "Detection",
                table: "Recognitions",
                nullable: true,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                schema: "Register",
                table: "Persons",
                nullable: true,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                schema: "Register",
                table: "NeuralNetworks",
                nullable: true,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                schema: "Register",
                table: "Companies",
                nullable: true,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                schema: "Register",
                table: "Categories",
                nullable: true,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                schema: "Register",
                table: "Camaras",
                nullable: true,
                defaultValue: true);

            migrationBuilder.AddColumn<bool>(
                name: "Enabled",
                schema: "Register",
                table: "Alarms",
                nullable: true,
                defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Enabled",
                schema: "Register",
                table: "XObjects");

            migrationBuilder.DropColumn(
                name: "Enabled",
                schema: "Register",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "Enabled",
                schema: "Detection",
                table: "Training");

            migrationBuilder.DropColumn(
                name: "Enabled",
                schema: "Register",
                table: "Subsidiaries");

            migrationBuilder.DropColumn(
                name: "Enabled",
                schema: "Detection",
                table: "Recognitions");

            migrationBuilder.DropColumn(
                name: "Enabled",
                schema: "Register",
                table: "Persons");

            migrationBuilder.DropColumn(
                name: "Enabled",
                schema: "Register",
                table: "NeuralNetworks");

            migrationBuilder.DropColumn(
                name: "Enabled",
                schema: "Register",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Enabled",
                schema: "Register",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "Enabled",
                schema: "Register",
                table: "Camaras");

            migrationBuilder.DropColumn(
                name: "Enabled",
                schema: "Register",
                table: "Alarms");
        }
    }
}
