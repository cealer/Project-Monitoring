﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Quitandoidpersonaentodaslastablas : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "XObjects");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Detection",
                table: "Training");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "Subsidiaries");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "NeuralNetworks");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Detection",
                table: "DetailsAlarms");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "Categories");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "Camaras");

            migrationBuilder.DropColumn(
                name: "Usr_Id",
                schema: "Register",
                table: "Alarms");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "XObjects",
                type: "char(18)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Detection",
                table: "Training",
                type: "char(18)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "Subsidiaries",
                type: "char(18)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "NeuralNetworks",
                type: "char(18)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Detection",
                table: "DetailsAlarms",
                type: "char(18)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "Companies",
                type: "char(18)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "Categories",
                type: "char(18)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "Camaras",
                type: "char(18)",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Usr_Id",
                schema: "Register",
                table: "Alarms",
                type: "char(18)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_XObjects_Usr_Id",
                schema: "Register",
                table: "XObjects",
                column: "Usr_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Training_Usr_Id",
                schema: "Detection",
                table: "Training",
                column: "Usr_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Subsidiaries_Usr_Id",
                schema: "Register",
                table: "Subsidiaries",
                column: "Usr_Id");

            migrationBuilder.CreateIndex(
                name: "IX_NeuralNetworks_Usr_Id",
                schema: "Register",
                table: "NeuralNetworks",
                column: "Usr_Id");

            migrationBuilder.CreateIndex(
                name: "IX_DetailsAlarms_Usr_Id",
                schema: "Detection",
                table: "DetailsAlarms",
                column: "Usr_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Companies_Usr_Id",
                schema: "Register",
                table: "Companies",
                column: "Usr_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Categories_Usr_Id",
                schema: "Register",
                table: "Categories",
                column: "Usr_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Camaras_Usr_Id",
                schema: "Register",
                table: "Camaras",
                column: "Usr_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Alarms_Usr_Id",
                schema: "Register",
                table: "Alarms",
                column: "Usr_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Alarms_Users_Usr_Id",
                schema: "Register",
                table: "Alarms",
                column: "Usr_Id",
                principalSchema: "Register",
                principalTable: "Users",
                principalColumn: "Usr_Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Camaras_Users_Usr_Id",
                schema: "Register",
                table: "Camaras",
                column: "Usr_Id",
                principalSchema: "Register",
                principalTable: "Users",
                principalColumn: "Usr_Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Categories_Users_Usr_Id",
                schema: "Register",
                table: "Categories",
                column: "Usr_Id",
                principalSchema: "Register",
                principalTable: "Users",
                principalColumn: "Usr_Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Companies_Users_Usr_Id",
                schema: "Register",
                table: "Companies",
                column: "Usr_Id",
                principalSchema: "Register",
                principalTable: "Users",
                principalColumn: "Usr_Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DetailsAlarms_Users_Usr_Id",
                schema: "Detection",
                table: "DetailsAlarms",
                column: "Usr_Id",
                principalSchema: "Register",
                principalTable: "Users",
                principalColumn: "Usr_Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NeuralNetworks_Users_Usr_Id",
                schema: "Register",
                table: "NeuralNetworks",
                column: "Usr_Id",
                principalSchema: "Register",
                principalTable: "Users",
                principalColumn: "Usr_Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Subsidiaries_Users_Usr_Id",
                schema: "Register",
                table: "Subsidiaries",
                column: "Usr_Id",
                principalSchema: "Register",
                principalTable: "Users",
                principalColumn: "Usr_Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Training_Users_Usr_Id",
                schema: "Detection",
                table: "Training",
                column: "Usr_Id",
                principalSchema: "Register",
                principalTable: "Users",
                principalColumn: "Usr_Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_XObjects_Users_Usr_Id",
                schema: "Register",
                table: "XObjects",
                column: "Usr_Id",
                principalSchema: "Register",
                principalTable: "Users",
                principalColumn: "Usr_Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
