﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class ModificandoModeloCamara : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Cam_Location",
                schema: "Register",
                table: "Camaras",
                newName: "Cam_Longitude");

            migrationBuilder.AddColumn<double>(
                name: "Cam_Latitude",
                schema: "Register",
                table: "Camaras",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Cam_Latitude",
                schema: "Register",
                table: "Camaras");

            migrationBuilder.RenameColumn(
                name: "Cam_Longitude",
                schema: "Register",
                table: "Camaras",
                newName: "Cam_Location");
        }
    }
}
