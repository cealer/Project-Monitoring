﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class ModificandoModeloSucursal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sub_Location",
                schema: "Register",
                table: "Subsidiaries");

            migrationBuilder.AlterColumn<string>(
                name: "Per_Photo",
                schema: "Register",
                table: "Persons",
                nullable: true,
                oldClrType: typeof(string));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Sub_Location",
                schema: "Register",
                table: "Subsidiaries",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Per_Photo",
                schema: "Register",
                table: "Persons",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
