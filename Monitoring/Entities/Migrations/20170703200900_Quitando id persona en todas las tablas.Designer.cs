﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Entities;

namespace Entities.Migrations
{
    [DbContext(typeof(MonitoringContext))]
    [Migration("20170703200900_Quitando id persona en todas las tablas")]
    partial class Quitandoidpersonaentodaslastablas
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Entities.Alarms", b =>
                {
                    b.Property<string>("AlmId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Alm_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("AlmDate")
                        .HasColumnName("Alm_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("AlmDescription")
                        .IsRequired()
                        .HasColumnName("Alm_Description")
                        .HasMaxLength(50);

                    b.Property<bool>("AlmEnabled")
                        .HasColumnName("Alm_Enabled");

                    b.Property<string>("ObjId")
                        .HasColumnName("Obj_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("PerId")
                        .HasColumnName("Per_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("AlmId")
                        .HasName("XPKAlarm");

                    b.HasIndex("AlmDescription")
                        .IsUnique()
                        .HasName("Alm_Description_uq");

                    b.HasIndex("ObjId");

                    b.HasIndex("PerId");

                    b.HasIndex("SubId");

                    b.ToTable("Alarms","Register");
                });

            modelBuilder.Entity("Entities.Camaras", b =>
                {
                    b.Property<string>("CamId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Cam_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("CamDate")
                        .HasColumnName("Cam_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("CamDescription")
                        .IsRequired()
                        .HasColumnName("Cam_Description")
                        .HasColumnType("varchar(20)");

                    b.Property<bool>("CamEnabled")
                        .HasColumnName("Cam_Enabled");

                    b.Property<string>("CamSource")
                        .IsRequired()
                        .HasColumnName("Cam_Source")
                        .HasMaxLength(200);

                    b.Property<double?>("Latitude")
                        .HasColumnName("Cam_Latitude");

                    b.Property<double?>("Longitude")
                        .HasColumnName("Cam_Longitude");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("CamId")
                        .HasName("XPKCamara");

                    b.HasIndex("CamDescription")
                        .IsUnique()
                        .HasName("Cam_Description_uq");

                    b.HasIndex("CamSource")
                        .IsUnique()
                        .HasName("Cam_Source_uq");

                    b.HasIndex("SubId");

                    b.ToTable("Camaras","Register");
                });

            modelBuilder.Entity("Entities.Categories", b =>
                {
                    b.Property<string>("CatId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Cat_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime?>("CatDate")
                        .HasColumnName("Cat_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("CatDescription")
                        .IsRequired()
                        .HasColumnName("Cat_Description")
                        .HasColumnType("varchar(50)");

                    b.Property<bool>("CatEnabled")
                        .HasColumnName("Cat_Enabled");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("CatId")
                        .HasName("XPKKind");

                    b.HasIndex("CatDescription")
                        .IsUnique()
                        .HasName("Kin_Description_uq");

                    b.HasIndex("SubId");

                    b.ToTable("Categories","Register");
                });

            modelBuilder.Entity("Entities.Companies", b =>
                {
                    b.Property<string>("ComId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Com_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("ComDate")
                        .HasColumnName("Com_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("ComDescription")
                        .IsRequired()
                        .HasColumnName("Com_Description")
                        .HasMaxLength(50);

                    b.Property<bool>("ComEnabled")
                        .HasColumnName("Com_Enabled");

                    b.HasKey("ComId")
                        .HasName("XPKCompany");

                    b.HasIndex("ComDescription")
                        .IsUnique()
                        .HasName("Com_Description_uq");

                    b.ToTable("Companies","Register");
                });

            modelBuilder.Entity("Entities.DetailsAlarms", b =>
                {
                    b.Property<string>("DalId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("DAl_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("AlmId")
                        .IsRequired()
                        .HasColumnName("Alm_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("DalDate")
                        .HasColumnName("DAl_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("DalId")
                        .HasName("XPKDetailsAlarms");

                    b.HasIndex("AlmId");

                    b.HasIndex("SubId");

                    b.ToTable("DetailsAlarms","Detection");
                });

            modelBuilder.Entity("Entities.DetailsRecognitions", b =>
                {
                    b.Property<string>("DreId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Dre_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("AlmId")
                        .HasColumnName("Alm_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("DreDate")
                        .HasColumnName("Dre_Date")
                        .HasColumnType("datetime");

                    b.Property<bool>("DreEnabled")
                        .HasColumnName("Dre_Enabled");

                    b.Property<decimal>("DrePercentage")
                        .HasColumnName("Dre_Percentage")
                        .HasColumnType("decimal");

                    b.Property<string>("ObjId")
                        .IsRequired()
                        .HasColumnName("Obj_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("RecId")
                        .IsRequired()
                        .HasColumnName("Rec_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("DreId")
                        .HasName("XPKDetailsRecognitions");

                    b.HasIndex("AlmId");

                    b.HasIndex("ObjId");

                    b.HasIndex("RecId");

                    b.ToTable("DetailsRecognitions","Detection");
                });

            modelBuilder.Entity("Entities.HistoryTables", b =>
                {
                    b.Property<string>("HisId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("His_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("HisDate")
                        .HasColumnName("His_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("HisOriginal")
                        .IsRequired()
                        .HasColumnName("His_Original")
                        .HasColumnType("varchar(max)");

                    b.Property<string>("HisTable")
                        .IsRequired()
                        .HasColumnName("His_Table")
                        .HasColumnType("varchar(max)");

                    b.Property<string>("HisUpdate")
                        .IsRequired()
                        .HasColumnName("His_Update");

                    b.Property<string>("UsrId")
                        .IsRequired()
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("HisId")
                        .HasName("XPKHistory");

                    b.HasIndex("UsrId");

                    b.ToTable("History_Tables","History");
                });

            modelBuilder.Entity("Entities.NeuralNetworks", b =>
                {
                    b.Property<string>("NeuId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Neu_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("NeuDate")
                        .HasColumnName("Neu_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("NeuDescription")
                        .IsRequired()
                        .HasColumnName("Neu_Description")
                        .HasMaxLength(50);

                    b.Property<string>("NeuPath")
                        .IsRequired()
                        .HasColumnName("Neu_Path")
                        .HasColumnType("nvarchar(50)");

                    b.Property<bool>("SubEnabled")
                        .HasColumnName("Sub_Enabled");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("NeuId")
                        .HasName("XPKNeuralNetwork");

                    b.HasIndex("NeuDescription")
                        .IsUnique()
                        .HasName("Neu_Description_uq");

                    b.HasIndex("NeuPath")
                        .IsUnique()
                        .HasName("Neu_Path_uq");

                    b.HasIndex("SubId");

                    b.ToTable("NeuralNetworks","Register");
                });

            modelBuilder.Entity("Entities.Persons", b =>
                {
                    b.Property<string>("PerId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Per_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("CatId")
                        .HasColumnName("Cat_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("PerDate")
                        .HasColumnName("Per_Date")
                        .HasColumnType("datetime");

                    b.Property<DateTime?>("PerDateBirth")
                        .HasColumnName("Per_DateBirth")
                        .HasColumnType("datetime");

                    b.Property<string>("PerDni")
                        .IsRequired()
                        .HasColumnName("Per_Dni")
                        .HasColumnType("char(8)")
                        .HasMaxLength(8);

                    b.Property<string>("PerEmail")
                        .IsRequired()
                        .HasColumnName("Per_Email")
                        .HasMaxLength(50);

                    b.Property<bool>("PerEnabled")
                        .HasColumnName("Per_Enabled");

                    b.Property<string>("PerGender")
                        .IsRequired()
                        .HasColumnName("Per_Gender")
                        .HasColumnType("char(1)");

                    b.Property<string>("PerLastName")
                        .IsRequired()
                        .HasColumnName("Per_LastName")
                        .HasMaxLength(50);

                    b.Property<string>("PerName")
                        .IsRequired()
                        .HasColumnName("Per_Name")
                        .HasMaxLength(50);

                    b.Property<string>("PerPath")
                        .IsRequired()
                        .HasColumnName("Per_Path")
                        .HasMaxLength(50);

                    b.Property<string>("PerPhone")
                        .HasColumnName("Per_Phone")
                        .HasMaxLength(16);

                    b.Property<string>("PerPhoto")
                        .HasColumnName("Per_Photo");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("PerId")
                        .HasName("XPKPerson");

                    b.HasIndex("CatId");

                    b.HasIndex("PerDni")
                        .IsUnique()
                        .HasName("Per_Dni_uq");

                    b.HasIndex("PerEmail")
                        .IsUnique()
                        .HasName("Per_Email_uq");

                    b.HasIndex("PerPath")
                        .IsUnique()
                        .HasName("Per_Path");

                    b.HasIndex("SubId");

                    b.ToTable("Persons","Register");
                });

            modelBuilder.Entity("Entities.Recognitions", b =>
                {
                    b.Property<string>("RecId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Rec_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("CamId")
                        .IsRequired()
                        .HasColumnName("Cam_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("PerId")
                        .HasColumnName("Per_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("RecDate")
                        .HasColumnName("Rec_Date")
                        .HasColumnType("datetime");

                    b.Property<bool>("RecEnabled")
                        .HasColumnName("Rec_Enabled");

                    b.Property<string>("RecFullPath")
                        .HasColumnName("Rec_PathFull");

                    b.Property<string>("RecPath")
                        .IsRequired()
                        .HasColumnName("Rec_Path");

                    b.Property<decimal>("RecPercentage")
                        .HasColumnName("Rec_Percentage")
                        .HasColumnType("decimal");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("RecId")
                        .HasName("XPKRecognition");

                    b.HasIndex("CamId");

                    b.HasIndex("PerId");

                    b.HasIndex("SubId");

                    b.ToTable("Recognitions","Detection");
                });

            modelBuilder.Entity("Entities.Subsidiaries", b =>
                {
                    b.Property<string>("SubId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("ComId")
                        .IsRequired()
                        .HasColumnName("Com_Id")
                        .HasColumnType("char(18)");

                    b.Property<double?>("Latitude")
                        .HasColumnName("Sub_Latitude");

                    b.Property<double?>("Longitude")
                        .HasColumnName("Sub_Longitude");

                    b.Property<DateTime>("SubDate")
                        .HasColumnName("Sub_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("SubDescription")
                        .IsRequired()
                        .HasColumnName("Sub_Description")
                        .HasColumnType("varchar(50)");

                    b.Property<bool>("SubEnabled")
                        .HasColumnName("Sub_Enabled");

                    b.Property<string>("SubNameLocation")
                        .HasColumnName("Sub_NameLocation")
                        .HasMaxLength(250);

                    b.HasKey("SubId")
                        .HasName("XPKSubsidiary");

                    b.HasIndex("ComId");

                    b.HasIndex("SubDescription")
                        .IsUnique()
                        .HasName("Sub_Description_uq");

                    b.ToTable("Subsidiaries","Register");
                });

            modelBuilder.Entity("Entities.Training", b =>
                {
                    b.Property<string>("TraId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Tra_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime?>("HighlightedAt")
                        .HasColumnName("Tra_HighlightedAt")
                        .HasColumnType("datetime");

                    b.Property<string>("NeuId")
                        .IsRequired()
                        .HasColumnName("Neu_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("TraDate")
                        .HasColumnName("Tra_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("TraDescription")
                        .IsRequired()
                        .HasColumnName("Tra_Description")
                        .HasColumnType("varchar(20)");

                    b.Property<bool>("TraEnabled")
                        .HasColumnName("Tra_Enabled");

                    b.Property<string>("TraStatus")
                        .HasColumnName("Tra_Status")
                        .HasColumnType("nvarchar(10)");

                    b.HasKey("TraId")
                        .HasName("XPKTraining");

                    b.HasIndex("NeuId");

                    b.HasIndex("TraDescription")
                        .IsUnique()
                        .HasName("Tra_Description_uq");

                    b.ToTable("Training","Detection");
                });

            modelBuilder.Entity("Entities.Users", b =>
                {
                    b.Property<string>("UsrId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("PerId")
                        .IsRequired()
                        .HasColumnName("Per_Id")
                        .HasColumnType("char(18)");

                    b.Property<bool>("UsrEnabled")
                        .HasColumnName("Usr_Enabled");

                    b.Property<string>("UsrUsername")
                        .IsRequired()
                        .HasColumnName("Usr_Username")
                        .HasMaxLength(50);

                    b.HasKey("UsrId")
                        .HasName("XPKUser");

                    b.HasIndex("PerId");

                    b.HasIndex("UsrUsername")
                        .IsUnique()
                        .HasName("Usr_Username");

                    b.ToTable("Users","Register");
                });

            modelBuilder.Entity("Entities.Xobjects", b =>
                {
                    b.Property<string>("ObjId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Obj_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("ObjDate")
                        .HasColumnName("Obj_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("ObjDescription")
                        .IsRequired()
                        .HasColumnName("Obj_Description")
                        .HasColumnType("varchar(20)");

                    b.Property<bool>("ObjEnabled")
                        .HasColumnName("Obj_Enabled");

                    b.Property<string>("ObjPath")
                        .IsRequired()
                        .HasColumnName("Obj_Path")
                        .HasMaxLength(50);

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("ObjId")
                        .HasName("XPKObject");

                    b.HasIndex("ObjDescription")
                        .IsUnique()
                        .HasName("Obj_Description_uq");

                    b.HasIndex("SubId");

                    b.ToTable("XObjects","Register");
                });

            modelBuilder.Entity("Entities.Alarms", b =>
                {
                    b.HasOne("Entities.Xobjects", "Obj")
                        .WithMany("Alarms")
                        .HasForeignKey("ObjId");

                    b.HasOne("Entities.Persons", "Per")
                        .WithMany("Alarms")
                        .HasForeignKey("PerId");

                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Alarms")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Camaras", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Camaras")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Categories", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Categories")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.DetailsAlarms", b =>
                {
                    b.HasOne("Entities.Alarms", "Alm")
                        .WithMany("DetailsAlarms")
                        .HasForeignKey("AlmId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("DetailsAlarms")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.DetailsRecognitions", b =>
                {
                    b.HasOne("Entities.Alarms", "Alm")
                        .WithMany("DetailsRecognitions")
                        .HasForeignKey("AlmId");

                    b.HasOne("Entities.Xobjects", "Obj")
                        .WithMany("DetailsRecognitions")
                        .HasForeignKey("ObjId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Recognitions", "Rec")
                        .WithMany("DetailsRecognitions")
                        .HasForeignKey("RecId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.HistoryTables", b =>
                {
                    b.HasOne("Entities.Users", "Usr")
                        .WithMany("HistoryTables")
                        .HasForeignKey("UsrId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.NeuralNetworks", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("NeuralNetworks")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Persons", b =>
                {
                    b.HasOne("Entities.Categories", "Cat")
                        .WithMany("Persons")
                        .HasForeignKey("CatId");

                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Persons")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Recognitions", b =>
                {
                    b.HasOne("Entities.Camaras", "Cam")
                        .WithMany("Recognitions")
                        .HasForeignKey("CamId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Persons", "Per")
                        .WithMany("Recognitions")
                        .HasForeignKey("PerId");

                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Recognitions")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Subsidiaries", b =>
                {
                    b.HasOne("Entities.Companies", "Com")
                        .WithMany("Subsidiaries")
                        .HasForeignKey("ComId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Training", b =>
                {
                    b.HasOne("Entities.NeuralNetworks", "Neu")
                        .WithMany("Training")
                        .HasForeignKey("NeuId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Users", b =>
                {
                    b.HasOne("Entities.Persons", "Per")
                        .WithMany("Users")
                        .HasForeignKey("PerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Xobjects", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Xobjects")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
