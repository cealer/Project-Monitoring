﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class ModificandoModeloEmpresa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Com_Location",
                schema: "Register",
                table: "Companies");

            migrationBuilder.DropColumn(
                name: "Com_NameLocation",
                schema: "Register",
                table: "Companies");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Com_Location",
                schema: "Register",
                table: "Companies",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Com_NameLocation",
                schema: "Register",
                table: "Companies",
                maxLength: 250,
                nullable: true);
        }
    }
}
