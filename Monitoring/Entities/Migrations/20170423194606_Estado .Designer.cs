﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Entities;

namespace Entities.Migrations
{
    [DbContext(typeof(MonitoringContext))]
    [Migration("20170423194606_Estado ")]
    partial class Estado
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Entities.Alarms", b =>
                {
                    b.Property<string>("AlmId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Alm_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("AlmDate")
                        .HasColumnName("Alm_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("AlmDescription")
                        .IsRequired()
                        .HasColumnName("Alm_Description")
                        .HasMaxLength(50);

                    b.Property<bool>("Enabled");

                    b.Property<string>("ObjId")
                        .HasColumnName("Obj_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("PerId")
                        .HasColumnName("Per_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("RecId")
                        .HasColumnName("Rec_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("AlmId")
                        .HasName("XPKAlarm");

                    b.HasIndex("AlmDescription")
                        .IsUnique()
                        .HasName("Alm_Description_uq");

                    b.HasIndex("ObjId")
                        .HasName("IX_Alarms_Obj_Id");

                    b.HasIndex("PerId")
                        .HasName("IX_Alarms_Per_Id");

                    b.HasIndex("RecId")
                        .HasName("IX_Alarms_Rec_Id");

                    b.HasIndex("SubId")
                        .HasName("IX_Alarms_Sub_Id");

                    b.ToTable("Alarms","Register");
                });

            modelBuilder.Entity("Entities.Camaras", b =>
                {
                    b.Property<string>("CamId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Cam_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("CamDate")
                        .HasColumnName("Cam_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("CamDescription")
                        .IsRequired()
                        .HasColumnName("Cam_Description")
                        .HasColumnType("varchar(20)");

                    b.Property<double?>("CamLocation")
                        .HasColumnName("Cam_Location");

                    b.Property<string>("CamSource")
                        .IsRequired()
                        .HasColumnName("Cam_Source")
                        .HasMaxLength(50);

                    b.Property<bool>("Enabled");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("CamId")
                        .HasName("XPKCamara");

                    b.HasIndex("CamDescription")
                        .IsUnique()
                        .HasName("Cam_Description_uq");

                    b.HasIndex("CamSource")
                        .IsUnique()
                        .HasName("Cam_Source_uq");

                    b.HasIndex("SubId")
                        .HasName("IX_Camaras_Sub_Id");

                    b.ToTable("Camaras","Register");
                });

            modelBuilder.Entity("Entities.Categories", b =>
                {
                    b.Property<string>("CatId")
                        .ValueGeneratedOnAdd()
                        .HasMaxLength(450);

                    b.Property<DateTime>("CatDate")
                        .HasColumnName("Cat_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("CatDescription")
                        .IsRequired()
                        .HasColumnName("Cat_Description")
                        .HasColumnType("varchar(50)");

                    b.Property<bool>("Enabled");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("CatId")
                        .HasName("XPKKind");

                    b.HasIndex("CatDescription")
                        .IsUnique()
                        .HasName("Kin_Description_uq");

                    b.HasIndex("SubId")
                        .HasName("IX_Categories_Sub_Id");

                    b.ToTable("Categories","Register");
                });

            modelBuilder.Entity("Entities.Companies", b =>
                {
                    b.Property<string>("ComId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Com_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("ComDate")
                        .HasColumnName("Com_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("ComDescription")
                        .IsRequired()
                        .HasColumnName("Com_Description")
                        .HasMaxLength(50);

                    b.Property<double?>("ComLocation")
                        .HasColumnName("Com_Location");

                    b.Property<bool>("Enabled");

                    b.Property<string>("UsrId")
                        .IsRequired()
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("ComId")
                        .HasName("XPKCompany");

                    b.HasIndex("ComDescription")
                        .IsUnique()
                        .HasName("Com_Description_uq");

                    b.HasIndex("UsrId")
                        .HasName("IX_Companies_Usr_Id");

                    b.ToTable("Companies","Register");
                });

            modelBuilder.Entity("Entities.NeuralNetworks", b =>
                {
                    b.Property<string>("NeuId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Neu_Id")
                        .HasColumnType("char(18)");

                    b.Property<bool>("Enabled");

                    b.Property<DateTime>("NeuDate")
                        .HasColumnName("Neu_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("NeuDescription")
                        .IsRequired()
                        .HasColumnName("Neu_Description")
                        .HasMaxLength(50);

                    b.Property<string>("NeuPath")
                        .IsRequired()
                        .HasColumnName("Neu_Path")
                        .HasColumnType("char(50)");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("NeuId")
                        .HasName("XPKNeuralNetwork");

                    b.HasIndex("NeuDescription")
                        .IsUnique()
                        .HasName("Neu_Description_uq");

                    b.HasIndex("NeuPath")
                        .IsUnique()
                        .HasName("Neu_Path_uq");

                    b.HasIndex("SubId")
                        .HasName("IX_NeuralNetworks_Sub_Id");

                    b.ToTable("NeuralNetworks","Register");
                });

            modelBuilder.Entity("Entities.Persons", b =>
                {
                    b.Property<string>("PerId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Per_Id")
                        .HasColumnType("char(18)");

                    b.Property<bool>("Enabled");

                    b.Property<DateTime>("PerDate")
                        .HasColumnName("Per_Date")
                        .HasColumnType("datetime");

                    b.Property<DateTime?>("PerDateBirth")
                        .HasColumnName("Per_DateBirth")
                        .HasColumnType("datetime");

                    b.Property<string>("PerDni")
                        .IsRequired()
                        .HasColumnName("Per_Dni")
                        .HasColumnType("char(8)");

                    b.Property<string>("PerEmail")
                        .IsRequired()
                        .HasColumnName("Per_Email")
                        .HasMaxLength(50);

                    b.Property<string>("PerGender")
                        .IsRequired()
                        .HasColumnName("Per_Gender")
                        .HasColumnType("char(1)");

                    b.Property<string>("PerLastName")
                        .IsRequired()
                        .HasColumnName("Per_LastName")
                        .HasMaxLength(50);

                    b.Property<string>("PerName")
                        .IsRequired()
                        .HasColumnName("Per_Name")
                        .HasMaxLength(50);

                    b.Property<string>("PerPath")
                        .IsRequired()
                        .HasColumnName("Per_Path")
                        .HasMaxLength(50);

                    b.Property<string>("PerPhone")
                        .HasColumnName("Per_Phone")
                        .HasMaxLength(16);

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("PerId")
                        .HasName("XPKPerson");

                    b.HasIndex("PerDni")
                        .IsUnique()
                        .HasName("Per_Dni_uq");

                    b.HasIndex("PerEmail")
                        .IsUnique()
                        .HasName("Per_Email_uq");

                    b.HasIndex("PerPath")
                        .IsUnique()
                        .HasName("Per_Path");

                    b.HasIndex("SubId")
                        .HasName("IX_Persons_Sub_Id");

                    b.ToTable("Persons","Register");
                });

            modelBuilder.Entity("Entities.Recognitions", b =>
                {
                    b.Property<string>("RecId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Rec_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("CamId")
                        .IsRequired()
                        .HasColumnName("Cam_Id")
                        .HasColumnType("char(18)");

                    b.Property<bool>("Enabled");

                    b.Property<string>("ObjId")
                        .HasColumnName("Obj_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("PerId")
                        .HasColumnName("Per_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("RecDate")
                        .HasColumnName("Rec_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("RecPath")
                        .IsRequired()
                        .HasColumnName("Rec_Path")
                        .HasMaxLength(50);

                    b.Property<decimal>("RecPercentage")
                        .HasColumnName("Rec_Percentage")
                        .HasColumnType("decimal");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("RecId")
                        .HasName("XPKRecognition");

                    b.HasIndex("CamId")
                        .HasName("IX_Recognitions_Cam_Id");

                    b.HasIndex("RecPath")
                        .IsUnique()
                        .HasName("Rec_Path");

                    b.HasIndex("SubId")
                        .HasName("IX_Recognitions_Sub_Id");

                    b.ToTable("Recognitions","Detection");
                });

            modelBuilder.Entity("Entities.Subsidiaries", b =>
                {
                    b.Property<string>("SubId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("ComId")
                        .IsRequired()
                        .HasColumnName("Com_Id")
                        .HasColumnType("char(18)");

                    b.Property<bool>("Enabled");

                    b.Property<DateTime>("SubDate")
                        .HasColumnName("Sub_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("SubDescription")
                        .IsRequired()
                        .HasColumnName("Sub_Description")
                        .HasColumnType("varchar(50)");

                    b.Property<double?>("SubLocation")
                        .HasColumnName("Sub_Location");

                    b.HasKey("SubId")
                        .HasName("XPKSubsidiary");

                    b.HasIndex("ComId")
                        .HasName("IX_Subsidiaries_Com_Id");

                    b.HasIndex("SubDescription")
                        .IsUnique()
                        .HasName("Sub_Description_uq");

                    b.ToTable("Subsidiaries","Register");
                });

            modelBuilder.Entity("Entities.Training", b =>
                {
                    b.Property<string>("TraId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Tra_Id")
                        .HasColumnType("char(18)");

                    b.Property<bool>("Enabled");

                    b.Property<string>("NeuId")
                        .IsRequired()
                        .HasColumnName("Neu_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("TraDate")
                        .HasColumnName("Tra_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("TraDescription")
                        .IsRequired()
                        .HasColumnName("Tra_Description")
                        .HasColumnType("varchar(20)");

                    b.HasKey("TraId")
                        .HasName("XPKTraining");

                    b.HasIndex("NeuId")
                        .HasName("IX_Training_Neu_Id");

                    b.HasIndex("TraDescription")
                        .IsUnique()
                        .HasName("Tra_Description_uq");

                    b.ToTable("Training","Detection");
                });

            modelBuilder.Entity("Entities.Users", b =>
                {
                    b.Property<string>("UsrId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.Property<bool>("Enabled");

                    b.Property<string>("SubId")
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime?>("UsrDate")
                        .HasColumnName("Usr_Date")
                        .HasColumnType("datetime");

                    b.Property<DateTime?>("UsrDateBirth")
                        .HasColumnName("Usr_DateBirth")
                        .HasColumnType("datetime");

                    b.Property<string>("UsrDni")
                        .HasColumnName("Usr_Dni")
                        .HasColumnType("char(8)");

                    b.Property<string>("UsrEmail")
                        .HasColumnName("Usr_Email")
                        .HasMaxLength(100);

                    b.Property<string>("UsrFirstName")
                        .IsRequired()
                        .HasColumnName("Usr_FirstName")
                        .HasMaxLength(50);

                    b.Property<string>("UsrGender")
                        .HasColumnName("Usr_Gender")
                        .HasColumnType("char(1)");

                    b.Property<string>("UsrLastName")
                        .HasColumnName("Usr_LastName")
                        .HasMaxLength(50);

                    b.Property<string>("UsrPhone")
                        .HasColumnName("Usr_Phone")
                        .HasMaxLength(20);

                    b.Property<string>("UsrPhoto")
                        .HasColumnName("Usr_Photo")
                        .HasColumnType("char(11)");

                    b.Property<string>("UsrUsername")
                        .IsRequired()
                        .HasColumnName("Usr_Username")
                        .HasMaxLength(50);

                    b.HasKey("UsrId")
                        .HasName("XPKUser");

                    b.HasIndex("SubId")
                        .HasName("IX_Users_Sub_Id");

                    b.HasIndex("UsrUsername")
                        .IsUnique()
                        .HasName("Usr_Username");

                    b.ToTable("Users","Register");
                });

            modelBuilder.Entity("Entities.Xobjects", b =>
                {
                    b.Property<string>("ObjId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Obj_Id")
                        .HasColumnType("char(18)");

                    b.Property<bool>("Enabled");

                    b.Property<DateTime>("ObjDate")
                        .HasColumnName("Obj_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("ObjDescription")
                        .IsRequired()
                        .HasColumnName("Obj_Description")
                        .HasColumnType("varchar(20)");

                    b.Property<string>("ObjPath")
                        .IsRequired()
                        .HasColumnName("Obj_Path")
                        .HasMaxLength(50);

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("ObjId")
                        .HasName("XPKObject");

                    b.HasIndex("ObjDescription")
                        .IsUnique()
                        .HasName("Obj_Description_uq");

                    b.HasIndex("SubId")
                        .HasName("IX_XObjects_Sub_Id");

                    b.ToTable("XObjects","Register");
                });

            modelBuilder.Entity("Entities.Alarms", b =>
                {
                    b.HasOne("Entities.Xobjects", "Obj")
                        .WithMany("Alarms")
                        .HasForeignKey("ObjId");

                    b.HasOne("Entities.Persons", "Per")
                        .WithMany("Alarms")
                        .HasForeignKey("PerId");

                    b.HasOne("Entities.Recognitions", "Rec")
                        .WithMany("Alarms")
                        .HasForeignKey("RecId");

                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Alarms")
                        .HasForeignKey("SubId");
                });

            modelBuilder.Entity("Entities.Camaras", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Camaras")
                        .HasForeignKey("SubId");
                });

            modelBuilder.Entity("Entities.Categories", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Categories")
                        .HasForeignKey("SubId");
                });

            modelBuilder.Entity("Entities.Companies", b =>
                {
                    b.HasOne("Entities.Users", "Usr")
                        .WithMany("Companies")
                        .HasForeignKey("UsrId");
                });

            modelBuilder.Entity("Entities.NeuralNetworks", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("NeuralNetworks")
                        .HasForeignKey("SubId");
                });

            modelBuilder.Entity("Entities.Persons", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Persons")
                        .HasForeignKey("SubId");
                });

            modelBuilder.Entity("Entities.Recognitions", b =>
                {
                    b.HasOne("Entities.Camaras", "Cam")
                        .WithMany("Recognitions")
                        .HasForeignKey("CamId");

                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Recognitions")
                        .HasForeignKey("SubId");
                });

            modelBuilder.Entity("Entities.Subsidiaries", b =>
                {
                    b.HasOne("Entities.Companies", "Com")
                        .WithMany("Subsidiaries")
                        .HasForeignKey("ComId");
                });

            modelBuilder.Entity("Entities.Training", b =>
                {
                    b.HasOne("Entities.NeuralNetworks", "Neu")
                        .WithMany("Training")
                        .HasForeignKey("NeuId");
                });

            modelBuilder.Entity("Entities.Users", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Users")
                        .HasForeignKey("SubId");
                });

            modelBuilder.Entity("Entities.Xobjects", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Xobjects")
                        .HasForeignKey("SubId");
                });
        }
    }
}
