﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Nuevoscambios3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Register");

            migrationBuilder.EnsureSchema(
                name: "Detection");

            migrationBuilder.CreateTable(
                name: "Recognitions",
                schema: "Detection",
                columns: table => new
                {
                    Rec_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Cam_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Obj_Id = table.Column<string>(type: "char(18)", nullable: true),
                    Per_Id = table.Column<string>(type: "char(18)", nullable: true),
                    Rec_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Rec_Path = table.Column<string>(maxLength: 50, nullable: false),
                    Rec_Percentage = table.Column<decimal>(type: "decimal", nullable: false),
                    Sub_Id = table.Column<string>(type: "char(18)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKRecognition", x => x.Rec_Id);
                });

            migrationBuilder.CreateTable(
                name: "Subsidiaries",
                schema: "Register",
                columns: table => new
                {
                    Sub_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Com_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Sub_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Sub_Description = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sub_Location = table.Column<double>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKSubsidiary", x => x.Sub_Id);
                });

            migrationBuilder.CreateTable(
                name: "Camaras",
                schema: "Register",
                columns: table => new
                {
                    Cam_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Cam_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Cam_Description = table.Column<string>(type: "varchar(20)", nullable: false),
                    Cam_Location = table.Column<double>(nullable: true),
                    Cam_Source = table.Column<string>(maxLength: 50, nullable: false),
                    Sub_Id = table.Column<string>(type: "char(18)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKCamara", x => x.Cam_Id);
                    table.ForeignKey(
                        name: "FK_Camaras_Subsidiaries_Sub_Id",
                        column: x => x.Sub_Id,
                        principalSchema: "Register",
                        principalTable: "Subsidiaries",
                        principalColumn: "Sub_Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Categories",
                schema: "Register",
                columns: table => new
                {
                    CatId = table.Column<string>(maxLength: 450, nullable: false),
                    Cat_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Cat_Description = table.Column<string>(type: "varchar(50)", nullable: false),
                    Sub_Id = table.Column<string>(type: "char(18)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKKind", x => x.CatId);
                    table.ForeignKey(
                        name: "FK_Categories_Subsidiaries_Sub_Id",
                        column: x => x.Sub_Id,
                        principalSchema: "Register",
                        principalTable: "Subsidiaries",
                        principalColumn: "Sub_Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "NeuralNetworks",
                schema: "Register",
                columns: table => new
                {
                    Neu_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Neu_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Neu_Description = table.Column<string>(maxLength: 50, nullable: false),
                    Neu_Path = table.Column<string>(type: "char(50)", nullable: false),
                    Sub_Id = table.Column<string>(type: "char(18)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKNeuralNetwork", x => x.Neu_Id);
                    table.ForeignKey(
                        name: "FK_NeuralNetworks_Subsidiaries_Sub_Id",
                        column: x => x.Sub_Id,
                        principalSchema: "Register",
                        principalTable: "Subsidiaries",
                        principalColumn: "Sub_Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Persons",
                schema: "Register",
                columns: table => new
                {
                    Per_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Per_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Per_DateBirth = table.Column<DateTime>(type: "datetime", nullable: true),
                    Per_Dni = table.Column<string>(type: "char(8)", nullable: false),
                    Per_Email = table.Column<string>(maxLength: 50, nullable: false),
                    Per_Gender = table.Column<string>(type: "char(1)", nullable: false),
                    Per_LastName = table.Column<string>(maxLength: 50, nullable: false),
                    Per_Name = table.Column<string>(maxLength: 50, nullable: false),
                    Per_Path = table.Column<string>(maxLength: 50, nullable: false),
                    Per_Phone = table.Column<string>(maxLength: 16, nullable: true),
                    Sub_Id = table.Column<string>(type: "char(18)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKPerson", x => x.Per_Id);
                    table.ForeignKey(
                        name: "FK_Persons_Subsidiaries_Sub_Id",
                        column: x => x.Sub_Id,
                        principalSchema: "Register",
                        principalTable: "Subsidiaries",
                        principalColumn: "Sub_Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                schema: "Register",
                columns: table => new
                {
                    Usr_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Sub_Id = table.Column<string>(type: "char(18)", nullable: true),
                    Usr_Date = table.Column<DateTime>(type: "datetime", nullable: true),
                    Usr_DateBirth = table.Column<DateTime>(type: "datetime", nullable: true),
                    Usr_Dni = table.Column<string>(type: "char(8)", nullable: true),
                    Usr_Email = table.Column<string>(maxLength: 100, nullable: true),
                    Usr_FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    Usr_Gender = table.Column<string>(type: "char(1)", nullable: true),
                    Usr_LastName = table.Column<string>(maxLength: 50, nullable: true),
                    Usr_Phone = table.Column<string>(maxLength: 20, nullable: true),
                    Usr_Photo = table.Column<string>(type: "char(11)", nullable: true),
                    Usr_Username = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKUser", x => x.Usr_Id);
                    table.ForeignKey(
                        name: "FK_Users_Subsidiaries_Sub_Id",
                        column: x => x.Sub_Id,
                        principalSchema: "Register",
                        principalTable: "Subsidiaries",
                        principalColumn: "Sub_Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "XObjects",
                schema: "Register",
                columns: table => new
                {
                    Obj_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Obj_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Obj_Description = table.Column<string>(type: "varchar(20)", nullable: false),
                    Obj_Path = table.Column<string>(maxLength: 50, nullable: false),
                    Sub_Id = table.Column<string>(type: "char(18)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKObject", x => x.Obj_Id);
                    table.ForeignKey(
                        name: "FK_XObjects_Subsidiaries_Sub_Id",
                        column: x => x.Sub_Id,
                        principalSchema: "Register",
                        principalTable: "Subsidiaries",
                        principalColumn: "Sub_Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Training",
                schema: "Detection",
                columns: table => new
                {
                    Tra_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Neu_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Tra_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Tra_Description = table.Column<string>(type: "varchar(20)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKTraining", x => x.Tra_Id);
                    table.ForeignKey(
                        name: "FK_Training_NeuralNetworks_Neu_Id",
                        column: x => x.Neu_Id,
                        principalSchema: "Register",
                        principalTable: "NeuralNetworks",
                        principalColumn: "Neu_Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                schema: "Register",
                columns: table => new
                {
                    Com_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Com_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Com_Description = table.Column<string>(maxLength: 50, nullable: false),
                    Com_Location = table.Column<double>(nullable: true),
                    Usr_Id = table.Column<string>(type: "char(18)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKCompany", x => x.Com_Id);
                    table.ForeignKey(
                        name: "FK_Companies_Users_Usr_Id",
                        column: x => x.Usr_Id,
                        principalSchema: "Register",
                        principalTable: "Users",
                        principalColumn: "Usr_Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Alarms",
                schema: "Register",
                columns: table => new
                {
                    Alm_Id = table.Column<string>(type: "char(18)", nullable: false),
                    Alm_Date = table.Column<DateTime>(type: "datetime", nullable: false),
                    Alm_Description = table.Column<string>(maxLength: 50, nullable: false),
                    Obj_Id = table.Column<string>(type: "char(18)", nullable: true),
                    Per_Id = table.Column<string>(type: "char(18)", nullable: true),
                    Rec_Id = table.Column<string>(type: "char(18)", nullable: true),
                    Sub_Id = table.Column<string>(type: "char(18)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("XPKAlarm", x => x.Alm_Id);
                    table.ForeignKey(
                        name: "FK_Alarms_XObjects_Obj_Id",
                        column: x => x.Obj_Id,
                        principalSchema: "Register",
                        principalTable: "XObjects",
                        principalColumn: "Obj_Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Alarms_Persons_Per_Id",
                        column: x => x.Per_Id,
                        principalSchema: "Register",
                        principalTable: "Persons",
                        principalColumn: "Per_Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Alarms_Recognitions_Rec_Id",
                        column: x => x.Rec_Id,
                        principalSchema: "Detection",
                        principalTable: "Recognitions",
                        principalColumn: "Rec_Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Alarms_Subsidiaries_Sub_Id",
                        column: x => x.Sub_Id,
                        principalSchema: "Register",
                        principalTable: "Subsidiaries",
                        principalColumn: "Sub_Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "Alm_Description_uq",
                schema: "Register",
                table: "Alarms",
                column: "Alm_Description",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Alarms_Obj_Id",
                schema: "Register",
                table: "Alarms",
                column: "Obj_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Alarms_Per_Id",
                schema: "Register",
                table: "Alarms",
                column: "Per_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Alarms_Rec_Id",
                schema: "Register",
                table: "Alarms",
                column: "Rec_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Alarms_Sub_Id",
                schema: "Register",
                table: "Alarms",
                column: "Sub_Id");

            migrationBuilder.CreateIndex(
                name: "Cam_Description_uq",
                schema: "Register",
                table: "Camaras",
                column: "Cam_Description",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Cam_Source_uq",
                schema: "Register",
                table: "Camaras",
                column: "Cam_Source",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Camaras_Sub_Id",
                schema: "Register",
                table: "Camaras",
                column: "Sub_Id");

            migrationBuilder.CreateIndex(
                name: "Kin_Description_uq",
                schema: "Register",
                table: "Categories",
                column: "Cat_Description",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Categories_Sub_Id",
                schema: "Register",
                table: "Categories",
                column: "Sub_Id");

            migrationBuilder.CreateIndex(
                name: "Com_Description_uq",
                schema: "Register",
                table: "Companies",
                column: "Com_Description",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Companies_Usr_Id",
                schema: "Register",
                table: "Companies",
                column: "Usr_Id");

            migrationBuilder.CreateIndex(
                name: "Neu_Description_uq",
                schema: "Register",
                table: "NeuralNetworks",
                column: "Neu_Description",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Neu_Path_uq",
                schema: "Register",
                table: "NeuralNetworks",
                column: "Neu_Path",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_NeuralNetworks_Sub_Id",
                schema: "Register",
                table: "NeuralNetworks",
                column: "Sub_Id");

            migrationBuilder.CreateIndex(
                name: "Per_Dni_uq",
                schema: "Register",
                table: "Persons",
                column: "Per_Dni",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Per_Email_uq",
                schema: "Register",
                table: "Persons",
                column: "Per_Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Per_Path",
                schema: "Register",
                table: "Persons",
                column: "Per_Path",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Persons_Sub_Id",
                schema: "Register",
                table: "Persons",
                column: "Sub_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Recognitions_Cam_Id",
                schema: "Detection",
                table: "Recognitions",
                column: "Cam_Id");

            migrationBuilder.CreateIndex(
                name: "Rec_Path",
                schema: "Detection",
                table: "Recognitions",
                column: "Rec_Path",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Recognitions_Sub_Id",
                schema: "Detection",
                table: "Recognitions",
                column: "Sub_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Subsidiaries_Com_Id",
                schema: "Register",
                table: "Subsidiaries",
                column: "Com_Id");

            migrationBuilder.CreateIndex(
                name: "Sub_Description_uq",
                schema: "Register",
                table: "Subsidiaries",
                column: "Sub_Description",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Training_Neu_Id",
                schema: "Detection",
                table: "Training",
                column: "Neu_Id");

            migrationBuilder.CreateIndex(
                name: "Tra_Description_uq",
                schema: "Detection",
                table: "Training",
                column: "Tra_Description",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_Sub_Id",
                schema: "Register",
                table: "Users",
                column: "Sub_Id");

            migrationBuilder.CreateIndex(
                name: "Usr_Username",
                schema: "Register",
                table: "Users",
                column: "Usr_Username",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "Obj_Description_uq",
                schema: "Register",
                table: "XObjects",
                column: "Obj_Description",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_XObjects_Sub_Id",
                schema: "Register",
                table: "XObjects",
                column: "Sub_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Recognitions_Subsidiaries_Sub_Id",
                schema: "Detection",
                table: "Recognitions",
                column: "Sub_Id",
                principalSchema: "Register",
                principalTable: "Subsidiaries",
                principalColumn: "Sub_Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Recognitions_Camaras_Cam_Id",
                schema: "Detection",
                table: "Recognitions",
                column: "Cam_Id",
                principalSchema: "Register",
                principalTable: "Camaras",
                principalColumn: "Cam_Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Subsidiaries_Companies_Com_Id",
                schema: "Register",
                table: "Subsidiaries",
                column: "Com_Id",
                principalSchema: "Register",
                principalTable: "Companies",
                principalColumn: "Com_Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Users_Subsidiaries_Sub_Id",
                schema: "Register",
                table: "Users");

            migrationBuilder.DropTable(
                name: "Alarms",
                schema: "Register");

            migrationBuilder.DropTable(
                name: "Categories",
                schema: "Register");

            migrationBuilder.DropTable(
                name: "Training",
                schema: "Detection");

            migrationBuilder.DropTable(
                name: "XObjects",
                schema: "Register");

            migrationBuilder.DropTable(
                name: "Persons",
                schema: "Register");

            migrationBuilder.DropTable(
                name: "Recognitions",
                schema: "Detection");

            migrationBuilder.DropTable(
                name: "NeuralNetworks",
                schema: "Register");

            migrationBuilder.DropTable(
                name: "Camaras",
                schema: "Register");

            migrationBuilder.DropTable(
                name: "Subsidiaries",
                schema: "Register");

            migrationBuilder.DropTable(
                name: "Companies",
                schema: "Register");

            migrationBuilder.DropTable(
                name: "Users",
                schema: "Register");
        }
    }
}
