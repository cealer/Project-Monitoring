﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Agregandolatitudylongitud : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<double>(
                name: "Sub_Latitude",
                schema: "Register",
                table: "Subsidiaries",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "Sub_Longitude",
                schema: "Register",
                table: "Subsidiaries",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Sub_Latitude",
                schema: "Register",
                table: "Subsidiaries");

            migrationBuilder.DropColumn(
                name: "Sub_Longitude",
                schema: "Register",
                table: "Subsidiaries");
        }
    }
}
