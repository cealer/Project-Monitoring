﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Cambiandomodeloobjeto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropIndex(
            //    name: "Obj_Description_uq",
            //    schema: "Register",
            //    table: "XObjects");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "Obj_Description_uq",
                schema: "Register",
                table: "XObjects",
                column: "Obj_Description",
                unique: true);
        }
    }
}
