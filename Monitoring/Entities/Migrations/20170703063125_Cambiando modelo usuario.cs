﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Cambiandomodelousuario : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropIndex(
            //    name: "Usr_Username",
            //    schema: "Register",
            //    table: "Users");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "Usr_Username",
                schema: "Register",
                table: "Users",
                column: "Usr_Username",
                unique: true);
        }
    }
}
