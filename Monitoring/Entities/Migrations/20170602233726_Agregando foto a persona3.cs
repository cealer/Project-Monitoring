﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Entities.Migrations
{
    public partial class Agregandofotoapersona3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Per_Photo",
                schema: "Register",
                table: "Persons",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Per_Photo",
                schema: "Register",
                table: "Persons",
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
