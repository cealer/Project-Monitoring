﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Entities;

namespace Entities.Migrations
{
    [DbContext(typeof(MonitoringContext))]
    [Migration("20170517031015_Corrigiendo Persona")]
    partial class CorrigiendoPersona
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Entities.Alarms", b =>
                {
                    b.Property<string>("AlmId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Alm_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("AlmDate")
                        .HasColumnName("Alm_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("AlmDescription")
                        .IsRequired()
                        .HasColumnName("Alm_Description")
                        .HasMaxLength(50);

                    b.Property<bool>("AlmEnabled")
                        .HasColumnName("Alm_Enabled");

                    b.Property<string>("ObjId")
                        .HasColumnName("Obj_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("PerId")
                        .HasColumnName("Per_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("UsrId")
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("AlmId")
                        .HasName("XPKAlarm");

                    b.HasIndex("AlmDescription")
                        .IsUnique()
                        .HasName("Alm_Description_uq");

                    b.HasIndex("ObjId");

                    b.HasIndex("PerId");

                    b.HasIndex("SubId");

                    b.HasIndex("UsrId");

                    b.ToTable("Alarms","Register");
                });

            modelBuilder.Entity("Entities.Camaras", b =>
                {
                    b.Property<string>("CamId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Cam_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("CamDate")
                        .HasColumnName("Cam_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("CamDescription")
                        .IsRequired()
                        .HasColumnName("Cam_Description")
                        .HasColumnType("varchar(20)");

                    b.Property<bool>("CamEnabled")
                        .HasColumnName("Cam_Enabled");

                    b.Property<double?>("CamLocation")
                        .HasColumnName("Cam_Location");

                    b.Property<string>("CamSource")
                        .IsRequired()
                        .HasColumnName("Cam_Source")
                        .HasMaxLength(50);

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("UsrId")
                        .IsRequired()
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("CamId")
                        .HasName("XPKCamara");

                    b.HasIndex("CamDescription")
                        .IsUnique()
                        .HasName("Cam_Description_uq");

                    b.HasIndex("CamSource")
                        .IsUnique()
                        .HasName("Cam_Source_uq");

                    b.HasIndex("SubId");

                    b.HasIndex("UsrId");

                    b.ToTable("Camaras","Register");
                });

            modelBuilder.Entity("Entities.Categories", b =>
                {
                    b.Property<string>("CatId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Cat_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime?>("CatDate")
                        .HasColumnName("Cat_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("CatDescription")
                        .IsRequired()
                        .HasColumnName("Cat_Description")
                        .HasColumnType("varchar(50)");

                    b.Property<bool>("CatEnabled")
                        .HasColumnName("Cat_Enabled");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("UsrId")
                        .IsRequired()
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("CatId")
                        .HasName("XPKKind");

                    b.HasIndex("CatDescription")
                        .IsUnique()
                        .HasName("Kin_Description_uq");

                    b.HasIndex("SubId");

                    b.HasIndex("UsrId");

                    b.ToTable("Categories","Register");
                });

            modelBuilder.Entity("Entities.Companies", b =>
                {
                    b.Property<string>("ComId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Com_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("ComDate")
                        .HasColumnName("Com_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("ComDescription")
                        .IsRequired()
                        .HasColumnName("Com_Description")
                        .HasMaxLength(50);

                    b.Property<bool>("ComEnabled")
                        .HasColumnName("Com_Enabled");

                    b.Property<double?>("ComLocation")
                        .HasColumnName("Com_Location");

                    b.Property<string>("ComNameLocation")
                        .HasColumnName("Com_NameLocation")
                        .HasMaxLength(250);

                    b.Property<string>("UsrId")
                        .IsRequired()
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("ComId")
                        .HasName("XPKCompany");

                    b.HasIndex("ComDescription")
                        .IsUnique()
                        .HasName("Com_Description_uq");

                    b.HasIndex("UsrId");

                    b.ToTable("Companies","Register");
                });

            modelBuilder.Entity("Entities.DetailsAlarms", b =>
                {
                    b.Property<string>("DalId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("DAl_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("AlmId")
                        .IsRequired()
                        .HasColumnName("Alm_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("DalDate")
                        .HasColumnName("DAl_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("UsrId")
                        .IsRequired()
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("DalId")
                        .HasName("XPKDetailsAlarms");

                    b.HasIndex("AlmId");

                    b.HasIndex("SubId");

                    b.HasIndex("UsrId");

                    b.ToTable("DetailsAlarms","Detection");
                });

            modelBuilder.Entity("Entities.DetailsRecognitions", b =>
                {
                    b.Property<string>("DreId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Dre_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("AlmId")
                        .HasColumnName("Alm_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("DreDate")
                        .HasColumnName("Dre_Date")
                        .HasColumnType("datetime");

                    b.Property<bool>("DreEnabled")
                        .HasColumnName("Dre_Enabled");

                    b.Property<decimal>("DrePercentage")
                        .HasColumnName("Dre_Percentage")
                        .HasColumnType("decimal");

                    b.Property<string>("ObjId")
                        .IsRequired()
                        .HasColumnName("Obj_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("RecId")
                        .IsRequired()
                        .HasColumnName("Rec_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("DreId")
                        .HasName("XPKDetailsRecognitions");

                    b.HasIndex("AlmId");

                    b.HasIndex("ObjId");

                    b.HasIndex("RecId");

                    b.ToTable("DetailsRecognitions","Detection");
                });

            modelBuilder.Entity("Entities.HistoryTables", b =>
                {
                    b.Property<string>("HisId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("His_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("HisDate")
                        .HasColumnName("His_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("HisOriginal")
                        .IsRequired()
                        .HasColumnName("His_Original")
                        .HasColumnType("varchar(max)");

                    b.Property<string>("HisTable")
                        .IsRequired()
                        .HasColumnName("His_Table")
                        .HasColumnType("varchar(max)");

                    b.Property<string>("HisUpdate")
                        .IsRequired()
                        .HasColumnName("His_Update");

                    b.Property<string>("UsrId")
                        .IsRequired()
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("HisId")
                        .HasName("XPKHistory");

                    b.HasIndex("UsrId");

                    b.ToTable("History_Tables","History");
                });

            modelBuilder.Entity("Entities.NeuralNetworks", b =>
                {
                    b.Property<string>("NeuId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Neu_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("NeuDate")
                        .HasColumnName("Neu_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("NeuDescription")
                        .IsRequired()
                        .HasColumnName("Neu_Description")
                        .HasMaxLength(50);

                    b.Property<string>("NeuPath")
                        .IsRequired()
                        .HasColumnName("Neu_Path")
                        .HasColumnType("char(50)");

                    b.Property<bool>("SubEnabled")
                        .HasColumnName("Sub_Enabled");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("UsrId")
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("NeuId")
                        .HasName("XPKNeuralNetwork");

                    b.HasIndex("NeuDescription")
                        .IsUnique()
                        .HasName("Neu_Description_uq");

                    b.HasIndex("NeuPath")
                        .IsUnique()
                        .HasName("Neu_Path_uq");

                    b.HasIndex("SubId");

                    b.HasIndex("UsrId");

                    b.ToTable("NeuralNetworks","Register");
                });

            modelBuilder.Entity("Entities.Persons", b =>
                {
                    b.Property<string>("PerId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Per_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("CatId")
                        .HasColumnName("Cat_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("PerDate")
                        .HasColumnName("Per_Date")
                        .HasColumnType("datetime");

                    b.Property<DateTime?>("PerDateBirth")
                        .HasColumnName("Per_DateBirth")
                        .HasColumnType("datetime");

                    b.Property<string>("PerDni")
                        .IsRequired()
                        .HasColumnName("Per_Dni")
                        .HasColumnType("char(8)");

                    b.Property<string>("PerEmail")
                        .IsRequired()
                        .HasColumnName("Per_Email")
                        .HasMaxLength(50);

                    b.Property<bool>("PerEnabled")
                        .HasColumnName("Per_Enabled");

                    b.Property<string>("PerGender")
                        .IsRequired()
                        .HasColumnName("Per_Gender")
                        .HasColumnType("char(1)");

                    b.Property<string>("PerLastName")
                        .IsRequired()
                        .HasColumnName("Per_LastName")
                        .HasMaxLength(50);

                    b.Property<string>("PerName")
                        .IsRequired()
                        .HasColumnName("Per_Name")
                        .HasMaxLength(50);

                    b.Property<string>("PerPath")
                        .IsRequired()
                        .HasColumnName("Per_Path")
                        .HasMaxLength(50);

                    b.Property<string>("PerPhone")
                        .HasColumnName("Per_Phone")
                        .HasMaxLength(16);

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("UsrId")
                        .IsRequired()
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("PerId")
                        .HasName("XPKPerson");

                    b.HasIndex("CatId");

                    b.HasIndex("PerDni")
                        .IsUnique()
                        .HasName("Per_Dni_uq");

                    b.HasIndex("PerEmail")
                        .IsUnique()
                        .HasName("Per_Email_uq");

                    b.HasIndex("PerPath")
                        .IsUnique()
                        .HasName("Per_Path");

                    b.HasIndex("SubId");

                    b.HasIndex("UsrId");

                    b.ToTable("Persons","Register");
                });

            modelBuilder.Entity("Entities.Recognitions", b =>
                {
                    b.Property<string>("RecId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Rec_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("CamId")
                        .IsRequired()
                        .HasColumnName("Cam_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("PerId")
                        .HasColumnName("Per_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("RecDate")
                        .HasColumnName("Rec_Date")
                        .HasColumnType("datetime");

                    b.Property<bool>("RecEnabled")
                        .HasColumnName("Rec_Enabled");

                    b.Property<string>("RecFullPath")
                        .HasColumnName("Rec_PathFull");

                    b.Property<string>("RecPath")
                        .IsRequired()
                        .HasColumnName("Rec_Path");

                    b.Property<decimal>("RecPercentage")
                        .HasColumnName("Rec_Percentage")
                        .HasColumnType("decimal");

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("RecId")
                        .HasName("XPKRecognition");

                    b.HasIndex("CamId");

                    b.HasIndex("PerId");

                    b.HasIndex("SubId");

                    b.ToTable("Recognitions","Detection");
                });

            modelBuilder.Entity("Entities.Subsidiaries", b =>
                {
                    b.Property<string>("SubId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("ComId")
                        .IsRequired()
                        .HasColumnName("Com_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("SubDate")
                        .HasColumnName("Sub_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("SubDescription")
                        .IsRequired()
                        .HasColumnName("Sub_Description")
                        .HasColumnType("varchar(50)");

                    b.Property<bool>("SubEnabled")
                        .HasColumnName("Sub_Enabled");

                    b.Property<double?>("SubLocation")
                        .HasColumnName("Sub_Location");

                    b.Property<string>("SubNameLocation")
                        .HasColumnName("Sub_NameLocation")
                        .HasMaxLength(250);

                    b.Property<string>("UsrId")
                        .IsRequired()
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("SubId")
                        .HasName("XPKSubsidiary");

                    b.HasIndex("ComId");

                    b.HasIndex("SubDescription")
                        .IsUnique()
                        .HasName("Sub_Description_uq");

                    b.HasIndex("UsrId");

                    b.ToTable("Subsidiaries","Register");
                });

            modelBuilder.Entity("Entities.Training", b =>
                {
                    b.Property<string>("TraId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Tra_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("NeuId")
                        .IsRequired()
                        .HasColumnName("Neu_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("TraDate")
                        .HasColumnName("Tra_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("TraDescription")
                        .IsRequired()
                        .HasColumnName("Tra_Description")
                        .HasColumnType("varchar(20)");

                    b.Property<bool>("TraEnabled")
                        .HasColumnName("Tra_Enabled");

                    b.Property<string>("UsrId")
                        .IsRequired()
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("TraId")
                        .HasName("XPKTraining");

                    b.HasIndex("NeuId");

                    b.HasIndex("TraDescription")
                        .IsUnique()
                        .HasName("Tra_Description_uq");

                    b.HasIndex("UsrId");

                    b.ToTable("Training","Detection");
                });

            modelBuilder.Entity("Entities.Users", b =>
                {
                    b.Property<string>("UsrId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("SubId")
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime?>("UsrDate")
                        .HasColumnName("Usr_Date")
                        .HasColumnType("datetime");

                    b.Property<DateTime?>("UsrDateBirth")
                        .HasColumnName("Usr_DateBirth")
                        .HasColumnType("datetime");

                    b.Property<string>("UsrDni")
                        .HasColumnName("Usr_Dni")
                        .HasColumnType("char(8)");

                    b.Property<string>("UsrEmail")
                        .HasColumnName("Usr_Email")
                        .HasMaxLength(100);

                    b.Property<bool>("UsrEnabled")
                        .HasColumnName("Usr_Enabled");

                    b.Property<string>("UsrFirstName")
                        .IsRequired()
                        .HasColumnName("Usr_FirstName")
                        .HasMaxLength(50);

                    b.Property<string>("UsrGender")
                        .HasColumnName("Usr_Gender")
                        .HasColumnType("char(1)");

                    b.Property<string>("UsrLastName")
                        .HasColumnName("Usr_LastName")
                        .HasMaxLength(50);

                    b.Property<string>("UsrPhone")
                        .HasColumnName("Usr_Phone")
                        .HasMaxLength(20);

                    b.Property<string>("UsrPhoto")
                        .HasColumnName("Usr_Photo")
                        .HasColumnType("char(11)");

                    b.Property<string>("UsrUsername")
                        .IsRequired()
                        .HasColumnName("Usr_Username")
                        .HasMaxLength(25);

                    b.HasKey("UsrId")
                        .HasName("XPKUser");

                    b.HasIndex("SubId");

                    b.HasIndex("UsrUsername")
                        .IsUnique()
                        .HasName("Usr_Username");

                    b.ToTable("Users","Register");
                });

            modelBuilder.Entity("Entities.Xobjects", b =>
                {
                    b.Property<string>("ObjId")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("Obj_Id")
                        .HasColumnType("char(18)");

                    b.Property<DateTime>("ObjDate")
                        .HasColumnName("Obj_Date")
                        .HasColumnType("datetime");

                    b.Property<string>("ObjDescription")
                        .IsRequired()
                        .HasColumnName("Obj_Description")
                        .HasColumnType("varchar(20)");

                    b.Property<bool>("ObjEnabled")
                        .HasColumnName("Obj_Enabled");

                    b.Property<string>("ObjPath")
                        .IsRequired()
                        .HasColumnName("Obj_Path")
                        .HasMaxLength(50);

                    b.Property<string>("SubId")
                        .IsRequired()
                        .HasColumnName("Sub_Id")
                        .HasColumnType("char(18)");

                    b.Property<string>("UsrId")
                        .HasColumnName("Usr_Id")
                        .HasColumnType("char(18)");

                    b.HasKey("ObjId")
                        .HasName("XPKObject");

                    b.HasIndex("ObjDescription")
                        .IsUnique()
                        .HasName("Obj_Description_uq");

                    b.HasIndex("SubId");

                    b.HasIndex("UsrId");

                    b.ToTable("XObjects","Register");
                });

            modelBuilder.Entity("Entities.Alarms", b =>
                {
                    b.HasOne("Entities.Xobjects", "Obj")
                        .WithMany("Alarms")
                        .HasForeignKey("ObjId");

                    b.HasOne("Entities.Persons", "Per")
                        .WithMany("Alarms")
                        .HasForeignKey("PerId");

                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Alarms")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Users", "Usr")
                        .WithMany("Alarms")
                        .HasForeignKey("UsrId");
                });

            modelBuilder.Entity("Entities.Camaras", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Camaras")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Users", "Usr")
                        .WithMany("Camaras")
                        .HasForeignKey("UsrId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Categories", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Categories")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Users", "Usr")
                        .WithMany("Categories")
                        .HasForeignKey("UsrId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Companies", b =>
                {
                    b.HasOne("Entities.Users", "Usr")
                        .WithMany("Companies")
                        .HasForeignKey("UsrId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.DetailsAlarms", b =>
                {
                    b.HasOne("Entities.Alarms", "Alm")
                        .WithMany("DetailsAlarms")
                        .HasForeignKey("AlmId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("DetailsAlarms")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Users", "Usr")
                        .WithMany("DetailsAlarms")
                        .HasForeignKey("UsrId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.DetailsRecognitions", b =>
                {
                    b.HasOne("Entities.Alarms", "Alm")
                        .WithMany("DetailsRecognitions")
                        .HasForeignKey("AlmId");

                    b.HasOne("Entities.Xobjects", "Obj")
                        .WithMany("DetailsRecognitions")
                        .HasForeignKey("ObjId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Recognitions", "Rec")
                        .WithMany("DetailsRecognitions")
                        .HasForeignKey("RecId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.HistoryTables", b =>
                {
                    b.HasOne("Entities.Users", "Usr")
                        .WithMany("HistoryTables")
                        .HasForeignKey("UsrId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.NeuralNetworks", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("NeuralNetworks")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Users", "Usr")
                        .WithMany("NeuralNetworks")
                        .HasForeignKey("UsrId");
                });

            modelBuilder.Entity("Entities.Persons", b =>
                {
                    b.HasOne("Entities.Categories", "Cat")
                        .WithMany("Persons")
                        .HasForeignKey("CatId");

                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Persons")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Users", "Usr")
                        .WithMany("Persons")
                        .HasForeignKey("UsrId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Recognitions", b =>
                {
                    b.HasOne("Entities.Camaras", "Cam")
                        .WithMany("Recognitions")
                        .HasForeignKey("CamId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Persons", "Per")
                        .WithMany("Recognitions")
                        .HasForeignKey("PerId");

                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Recognitions")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Subsidiaries", b =>
                {
                    b.HasOne("Entities.Companies", "Com")
                        .WithMany("Subsidiaries")
                        .HasForeignKey("ComId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Users", "Usr")
                        .WithMany("Subsidiaries")
                        .HasForeignKey("UsrId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Training", b =>
                {
                    b.HasOne("Entities.NeuralNetworks", "Neu")
                        .WithMany("Training")
                        .HasForeignKey("NeuId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Users", "Usr")
                        .WithMany("Training")
                        .HasForeignKey("UsrId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Entities.Users", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Users")
                        .HasForeignKey("SubId");
                });

            modelBuilder.Entity("Entities.Xobjects", b =>
                {
                    b.HasOne("Entities.Subsidiaries", "Sub")
                        .WithMany("Xobjects")
                        .HasForeignKey("SubId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Entities.Users", "Usr")
                        .WithMany("Xobjects")
                        .HasForeignKey("UsrId");
                });
        }
    }
}
