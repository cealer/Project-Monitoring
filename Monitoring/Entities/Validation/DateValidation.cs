﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Entities.Validation
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class DateValidation : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value != null)
            {
                DateTime _birthJoin = Convert.ToDateTime(value);
                if (_birthJoin > DateTime.Now)
                {
                    return new ValidationResult("Fecha de nacimiento iválida.");
                }
            }
            return ValidationResult.Success;
        }
        //public DateValidation()
        //{

        //}

        //public override bool IsValid(object value)
        //{
        //    var dt = (DateTime)value;
        //    if (dt <= DateTime.Now)
        //    {
        //        return true;
        //    }
        //    return false;
        //}

    }
}