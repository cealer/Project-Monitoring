﻿using Entities.Models.RECOGNITION;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models.API
{
    public class FaceApiResponse
    {
        public string faceId { get; set; }
        public virtual FaceRectangle faceRectangle { get; set; }
        public virtual FaceAttributes faceAttributes { get; set; }
        public virtual FaceLandmarks faceLandmarks { get; set; }
    }
}