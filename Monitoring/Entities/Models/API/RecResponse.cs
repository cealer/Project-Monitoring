﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models.API
{
    public class RecResponse
    {
        public string DreId { get; set; }
        public string RecId { get; set; }
        public string ObjId { get; set; }
        public decimal DrePercentage { get; set; }
        public bool DreEnabled { get; set; }
        public DateTime DreDate { get; set; }
        public string AlmId { get; set; }

        public virtual Xobjects Obj { get; set; }

        public virtual Alarms Alm { get; set; }

        public virtual Recognitions Rec { get; set; }

        public virtual Persons Per { get; set; }

        public virtual Camaras Cam { get; set; }

        public virtual Subsidiaries Sub { get; set; }

        public virtual Companies Com { get; set; }
    }
}
