﻿namespace Entities.Models.RECOGNITION
{
    public class EyebrowRightInner
    {
        public double x { get; set; }
        public double y { get; set; }
    }

}