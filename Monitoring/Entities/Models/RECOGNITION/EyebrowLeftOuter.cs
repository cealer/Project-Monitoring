﻿namespace Entities.Models.RECOGNITION
{
    public class EyebrowLeftOuter
    {
        public double x { get; set; }
        public double y { get; set; }
    }

}