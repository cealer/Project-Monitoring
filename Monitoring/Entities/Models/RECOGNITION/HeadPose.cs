﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models.RECOGNITION
{
    public class HeadPose
    {
        public double pitch { get; set; }
        public double roll { get; set; }
        public double yaw { get; set; }
    }
}
