﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models.RECOGNITION
{
    public class Hair
    {
        public double bald { get; set; }
        public bool invisible { get; set; }
        public List<HairColor> hairColor { get; set; }

    }
}
