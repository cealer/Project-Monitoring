﻿using System.Collections.Generic;

namespace Entities.Models.RECOGNITION
{
    public class FaceAttributes
    {
        public Hair hair { get; set; }
        public double smile { get; set; }
        public HeadPose headPose { get; set; }
        public string gender { get; set; }
        public double age { get; set; }
        public FacialHair facialHair { get; set; }
        public string glasses { get; set; }
        public Makeup makeup { get; set; }
        public Emotion emotion { get; set; }
        public Occlusion occlusion { get; set; }
        public List<Accessory> accessories { get; set; }
        public Blur blur { get; set; }
        public Exposure exposure { get; set; }
        public Noise noise { get; set; }
    }

}