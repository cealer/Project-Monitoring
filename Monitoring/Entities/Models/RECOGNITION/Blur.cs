﻿namespace Entities.Models.RECOGNITION
{
    public class Blur
    {
        public string blurLevel { get; set; }
        public double value { get; set; }
    }

}