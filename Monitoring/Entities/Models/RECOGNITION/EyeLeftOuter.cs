﻿namespace Entities.Models.RECOGNITION
{
    public class EyeLeftOuter
    {
        public double x { get; set; }
        public double y { get; set; }
    }

}