﻿namespace Entities.Models.RECOGNITION
{
    public class EyeRightTop
    {
        public double x { get; set; }
        public double y { get; set; }
    }

}