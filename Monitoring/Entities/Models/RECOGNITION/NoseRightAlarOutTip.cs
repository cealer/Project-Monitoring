﻿namespace Entities.Models.RECOGNITION
{
    public class NoseRightAlarOutTip
    {
        public double x { get; set; }
        public double y { get; set; }
    }

}