﻿namespace Entities.Models.RECOGNITION
{
    public class EyeLeftTop
    {
        public double x { get; set; }
        public double y { get; set; }
    }

}