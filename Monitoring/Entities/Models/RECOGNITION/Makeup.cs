﻿namespace Entities.Models.RECOGNITION
{
    public class Makeup
    {
        public bool eyeMakeup { get; set; }
        public bool lipMakeup { get; set; }
    }

}