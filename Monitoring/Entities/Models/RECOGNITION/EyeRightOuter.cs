﻿namespace Entities.Models.RECOGNITION
{
    public class EyeRightOuter
    {
        public double x { get; set; }
        public double y { get; set; }
    }

}