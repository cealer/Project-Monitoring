﻿namespace Entities.Models.RECOGNITION
{
    public class EyeRightInner
    {
        public double x { get; set; }
        public double y { get; set; }
    }

}