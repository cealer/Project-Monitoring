﻿namespace Entities.Models.RECOGNITION
{
    public class NoseRootRight
    {
        public double x { get; set; }
        public double y { get; set; }
    }

}