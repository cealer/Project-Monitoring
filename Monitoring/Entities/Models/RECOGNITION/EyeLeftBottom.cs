﻿namespace Entities.Models.RECOGNITION
{
    public class EyeLeftBottom
    {
        public double x { get; set; }
        public double y { get; set; }
    }

}