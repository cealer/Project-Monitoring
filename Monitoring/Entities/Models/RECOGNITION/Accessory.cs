﻿namespace Entities.Models.RECOGNITION
{
    public class Accessory
    {
        public string type { get; set; }
        public double confidence { get; set; }
    }
}