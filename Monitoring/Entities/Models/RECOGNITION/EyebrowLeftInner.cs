﻿namespace Entities.Models.RECOGNITION
{
    public class EyebrowLeftInner
    {
        public double x { get; set; }
        public double y { get; set; }
    }

}