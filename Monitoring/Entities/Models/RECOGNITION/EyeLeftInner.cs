﻿namespace Entities.Models.RECOGNITION
{
    public class EyeLeftInner
    {
        public double x { get; set; }
        public double y { get; set; }
    }

}