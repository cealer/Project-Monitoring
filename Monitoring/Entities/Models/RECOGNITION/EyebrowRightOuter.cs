﻿namespace Entities.Models.RECOGNITION
{
    public class EyebrowRightOuter
    {
        public double x { get; set; }
        public double y { get; set; }
    }

}