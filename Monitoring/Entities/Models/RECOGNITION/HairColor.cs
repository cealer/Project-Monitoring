﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models.RECOGNITION
{
    public class HairColor
    {
        public string color { get; set; }
        public double confidence { get; set; }
    }
}
