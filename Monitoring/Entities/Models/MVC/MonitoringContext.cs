﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Entities.Models.MVC;

namespace Entities
{
    public partial class MonitoringContext : DbContext
    {
        public virtual DbSet<Alarms> Alarms { get; set; }
        public virtual DbSet<Camaras> Camaras { get; set; }
        public virtual DbSet<Categories> Categories { get; set; }
        public virtual DbSet<Companies> Companies { get; set; }
        public virtual DbSet<DetailsAlarms> DetailsAlarms { get; set; }
        public virtual DbSet<DetailsRecognitions> DetailsRecognitions { get; set; }
        public virtual DbSet<HistoryTables> HistoryTables { get; set; }
        public virtual DbSet<NeuralNetworks> NeuralNetworks { get; set; }
        public virtual DbSet<Persons> Persons { get; set; }
        public virtual DbSet<Recognitions> Recognitions { get; set; }
        public virtual DbSet<Subsidiaries> Subsidiaries { get; set; }
        public virtual DbSet<Training> Training { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        public virtual DbSet<Xobjects> Xobjects { get; set; }
        public virtual DbSet<DescriptionRec> DescriptionRec { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseSqlServer(@"Data Source=DESKTOP-UF9MHHN;Initial Catalog=Monitoring;User ID=sa;Password=123;MultipleActiveResultSets=true");
            optionsBuilder.UseSqlServer(@"Data Source =tcp:monitoringbd.database.windows.net,1433;Initial Catalog=Monitoring;Persist Security Info=False;User ID=monitoringAdmin;Password=@@Q1w2e3r4t5y;MultipleActiveResultSets=True;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Alarms>(entity =>
            {
                entity.HasKey(e => e.AlmId)
                    .HasName("XPKAlarm");

                entity.HasIndex(e => e.AlmDescription)
                    .HasName("Alm_Description_uq")
                    .IsUnique();
            });

            modelBuilder.Entity<DescriptionRec>(entity =>
            {
                entity.HasKey(e => e.DesId)
                    .HasName("XPKDescription");

            });

            modelBuilder.Entity<Camaras>(entity =>
            {
                entity.HasKey(e => e.CamId)
                    .HasName("XPKCamara");

                entity.HasIndex(e => e.CamDescription)
                    .HasName("Cam_Description_uq")
                    .IsUnique();

                entity.HasIndex(e => e.CamSource)
                    .HasName("Cam_Source_uq")
                    .IsUnique();
            });

            modelBuilder.Entity<Categories>(entity =>
            {
                entity.HasKey(e => e.CatId)
                    .HasName("XPKKind");

                entity.HasIndex(e => e.CatDescription)
                    .HasName("Kin_Description_uq")
                    .IsUnique();
            });

            modelBuilder.Entity<Companies>(entity =>
            {
                entity.HasKey(e => e.ComId)
                    .HasName("XPKCompany");

                entity.HasIndex(e => e.ComDescription)
                    .HasName("Com_Description_uq")
                    .IsUnique();
            });

            modelBuilder.Entity<DetailsAlarms>(entity =>
            {
                entity.HasKey(e => e.DalId)
                    .HasName("XPKDetailsAlarms");
            });

            modelBuilder.Entity<DetailsRecognitions>(entity =>
            {
                entity.HasKey(e => e.DreId)
                    .HasName("XPKDetailsRecognitions");
            });

            modelBuilder.Entity<HistoryTables>(entity =>
            {
                entity.HasKey(e => e.HisId)
                    .HasName("XPKHistory");
            });

            modelBuilder.Entity<NeuralNetworks>(entity =>
            {
                entity.HasKey(e => e.NeuId)
                    .HasName("XPKNeuralNetwork");

                entity.HasIndex(e => e.NeuDescription)
                    .HasName("Neu_Description_uq")
                    .IsUnique();

                entity.HasIndex(e => e.NeuPath)
                    .HasName("Neu_Path_uq")
                    .IsUnique();
            });

            modelBuilder.Entity<Persons>(entity =>
            {
                entity.HasKey(e => e.PerId)
                    .HasName("XPKPerson");

                entity.HasIndex(e => e.PerDni)
                    .HasName("Per_Dni_uq")
                    .IsUnique();

                entity.HasIndex(e => e.PerEmail)
                    .HasName("Per_Email_uq")
                    .IsUnique();

                entity.HasIndex(e => e.PerPath)
                    .HasName("Per_Path")
                    .IsUnique();
            });

            modelBuilder.Entity<Recognitions>(entity =>
            {
                entity.HasKey(e => e.RecId)
                    .HasName("XPKRecognition");
            });

            modelBuilder.Entity<Subsidiaries>(entity =>
            {
                entity.HasKey(e => e.SubId)
                    .HasName("XPKSubsidiary");

                entity.HasIndex(e => e.SubDescription)
                    .HasName("Sub_Description_uq")
                    .IsUnique();
            });

            modelBuilder.Entity<Training>(entity =>
            {
                entity.HasKey(e => e.TraId)
                    .HasName("XPKTraining");

                entity.HasIndex(e => e.TraDescription)
                    .HasName("Tra_Description_uq")
                    .IsUnique();
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UsrId)
                    .HasName("XPKUser");

                entity.HasIndex(e => e.UsrUsername)
                    .HasName("Usr_Username")
                    .IsUnique();
            });

            modelBuilder.Entity<Xobjects>(entity =>
            {
                entity.HasKey(e => e.ObjId)
                    .HasName("XPKObject");

                //entity.HasIndex(e => e.ObjDescription)
                //    .HasName("Obj_Description_uq")
                //    .IsUnique();
            });
        }
    }
}