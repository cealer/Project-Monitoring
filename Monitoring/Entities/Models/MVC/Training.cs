﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("Training", Schema = "Detection")]
    public partial class Training
    {
        [Column("Tra_Id", TypeName = "char(18)")]
        public string TraId { get; set; }

        [Display(Name = "Descripción")]
        [Required]
        [Column("Tra_Description", TypeName = "varchar(20)")]
        public string TraDescription { get; set; }

        [Display(Name = "Fecha de registro")]
        [Column("Tra_Date", TypeName = "datetime")]
        public DateTime TraDate { get; set; }

        [Display(Name = "Clasificador")]
        [Required]
        [Column("Neu_Id", TypeName = "char(18)")]
        public string NeuId { get; set; }

        [Display(Name = "Estado del Clasificador ")]
        [Column("Tra_Status", TypeName = "nvarchar(10)")]
        public string TraStatus { get; set; }

        [Column("Tra_HighlightedAt", TypeName = "datetime")]
        public DateTime? HighlightedAt { get; set; }

        public TimeSpan? HighlightedIn => HighlightedAt.HasValue ? HighlightedAt.Value - TraDate : (TimeSpan?)null;

        [Display(Name = "Habilitado")]
        [Column("Tra_Enabled")]
        public bool TraEnabled { get; set; }

        [JsonIgnore]
        [ForeignKey("NeuId")]
        [InverseProperty("Training")]
        public virtual NeuralNetworks Neu { get; set; }

    }
}
