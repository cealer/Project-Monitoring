﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("Subsidiaries", Schema = "Register")]
    public partial class Subsidiaries
    {
        public Subsidiaries()
        {
            Alarms = new HashSet<Alarms>();
            Camaras = new HashSet<Camaras>();
            Categories = new HashSet<Categories>();
            DetailsAlarms = new HashSet<DetailsAlarms>();
            NeuralNetworks = new HashSet<NeuralNetworks>();
            Persons = new HashSet<Persons>();
            Recognitions = new HashSet<Recognitions>();
            Xobjects = new HashSet<Xobjects>();
        }

        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }

        [Display(Name = "Sucursal")]
        [Required]
        [Column("Sub_Description", TypeName = "varchar(50)")]
        public string SubDescription { get; set; }

        [Display(Name = "Fech de registro")]
        [Column("Sub_Date", TypeName = "datetime")]
        public DateTime SubDate { get; set; }

        [Display(Name = "Latitud")]
        [Column("Sub_Latitude")]
        public double? Latitude { get; set; }

        [Display(Name = "Longitud")]
        [Column("Sub_Longitude")]
        public double? Longitude { get; set; }

        [Display(Name = "Empresa")]
        [Required]
        [Column("Com_Id", TypeName = "char(18)")]
        public string ComId { get; set; }

        [Display(Name = "Dirección")]
        [Column("Sub_NameLocation")]
        [MaxLength(250)]
        public string SubNameLocation { get; set; }

        [Display(Name = "Habilitado")]
        [Column("Sub_Enabled")]
        public bool SubEnabled { get; set; }

        [JsonIgnore]
        [InverseProperty("Sub")]
        public virtual ICollection<Alarms> Alarms { get; set; }
        [JsonIgnore]
        [InverseProperty("Sub")]
        public virtual ICollection<Camaras> Camaras { get; set; }
        [JsonIgnore]
        [InverseProperty("Sub")]
        public virtual ICollection<Categories> Categories { get; set; }
        [JsonIgnore]
        [InverseProperty("Sub")]
        public virtual ICollection<DetailsAlarms> DetailsAlarms { get; set; }
        [JsonIgnore]
        [InverseProperty("Sub")]
        public virtual ICollection<NeuralNetworks> NeuralNetworks { get; set; }
        [JsonIgnore]
        [InverseProperty("Sub")]
        public virtual ICollection<Persons> Persons { get; set; }
        [JsonIgnore]
        [InverseProperty("Sub")]
        public virtual ICollection<Recognitions> Recognitions { get; set; }
        [JsonIgnore]
        [InverseProperty("Sub")]
        public virtual ICollection<Xobjects> Xobjects { get; set; }
        [JsonIgnore]
        [ForeignKey("ComId")]
        [InverseProperty("Subsidiaries")]
        public virtual Companies Com { get; set; }
    }
}
