﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("Categories", Schema = "Register")]
    public partial class Categories
    {
        public Categories()
        {
            Persons = new HashSet<Persons>();
        }

        [Column("Cat_Id", TypeName = "char(18)")]
        public string CatId { get; set; }

        [Required]
        [Display(Name = "Descripción")]
        [Column("Cat_Description", TypeName = "varchar(50)")]
        public string CatDescription { get; set; }

        [Display(Name = "Fecha de registro")]
        [Column("Cat_Date", TypeName = "datetime")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime? CatDate { get; set; }

        [Display(Name = "Sucursal")]
        [Required]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }

        [Display(Name = "Disponible")]
        [Column("Cat_Enabled")]
        public bool CatEnabled { get; set; }

        [JsonIgnore]
        [InverseProperty("Cat")]
        public virtual ICollection<Persons> Persons { get; set; }

        [JsonIgnore]
        [ForeignKey("SubId")]
        [InverseProperty("Categories")]
        [Display(Name = "Sucursal")]
        public virtual Subsidiaries Sub { get; set; }
    }
}
