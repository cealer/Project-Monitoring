﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("Companies", Schema = "Register")]
    public partial class Companies
    {
        public Companies()
        {
            Subsidiaries = new HashSet<Subsidiaries>();
        }

        [Column("Com_Id", TypeName = "char(18)")]
        public string ComId { get; set; }

        [Display(Name = "Empresa")]
        [Required]
        [Column("Com_Description")]
        [MaxLength(50)]
        public string ComDescription { get; set; }

        [Display(Name = "Fecha de registro")]
        [Column("Com_Date", TypeName = "datetime")]
        public DateTime ComDate { get; set; }

        [Display(Name = "Disponible")]
        [Column("Com_Enabled")]
        public bool ComEnabled { get; set; }

        [JsonIgnore]
        [InverseProperty("Com")]
        public virtual ICollection<Subsidiaries> Subsidiaries { get; set; }
    }
}
