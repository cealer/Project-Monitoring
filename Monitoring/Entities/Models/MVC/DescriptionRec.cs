﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models.MVC
{
    [Table("DescriptionRec", Schema = "Detection")]
    public class DescriptionRec
    {
        [Column("Des_Id", TypeName = "char(18)")]
        public string DesId { get; set; }

        [Required]
        [Column("Rec_Id", TypeName = "char(18)")]
        public string RecId { get; set; }

        [Required]
        [Column("Rec_Description", TypeName = "nvarchar(250)")]
        public string Description { get; set; }

        [JsonIgnore]
        [ForeignKey("RecId")]
        [InverseProperty("DescriptionRecs")]
        public virtual Recognitions Rec { get; set; }
    }
}
