﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("XObjects", Schema = "Register")]
    public partial class Xobjects
    {
        public Xobjects()
        {
            Alarms = new HashSet<Alarms>();
            DetailsRecognitions = new HashSet<DetailsRecognitions>();
        }

        [Column("Obj_Id", TypeName = "char(18)")]
        public string ObjId { get; set; }

        [Display(Name = "Descripción")]
        [Required]
        [Column("Obj_Description", TypeName = "varchar(60)")]
        public string ObjDescription { get; set; }

        [Display(Name = "Ruta")]
        [Required]
        [Column("Obj_Path")]
        public string ObjPath { get; set; }

        [Display(Name = "Fecha de registro")]
        [Column("Obj_Date", TypeName = "datetime")]
        public DateTime ObjDate { get; set; }

        [Display(Name = "Sucursal")]
        [Required]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }

        [Display(Name = "Habilitado")]
        [Column("Obj_Enabled")]
        public bool ObjEnabled { get; set; }

        [JsonIgnore]
        [InverseProperty("Obj")]
        public virtual ICollection<Alarms> Alarms { get; set; }
        [JsonIgnore]
        [InverseProperty("Obj")]
        public virtual ICollection<DetailsRecognitions> DetailsRecognitions { get; set; }
        [JsonIgnore]
        [ForeignKey("SubId")]
        [InverseProperty("Xobjects")]
        public virtual Subsidiaries Sub { get; set; }

    }
}
