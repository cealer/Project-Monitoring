﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("DetailsAlarms", Schema = "Detection")]
    public partial class DetailsAlarms
    {
        [Column("DAl_Id", TypeName = "char(18)")]
        public string DalId { get; set; }
        [Required]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }
        [Required]
        [Column("Alm_Id", TypeName = "char(18)")]
        public string AlmId { get; set; }
        [Column("DAl_Date", TypeName = "datetime")]
        public DateTime DalDate { get; set; }
  
        [JsonIgnore]
        [ForeignKey("AlmId")]
        [InverseProperty("DetailsAlarms")]
        public virtual Alarms Alm { get; set; }

        [JsonIgnore]
        [ForeignKey("SubId")]
        [InverseProperty("DetailsAlarms")]
        public virtual Subsidiaries Sub { get; set; }

    }
}