﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("NeuralNetworks", Schema = "Register")]
    public partial class NeuralNetworks
    {
        public NeuralNetworks()
        {
            Training = new HashSet<Training>();
        }

        [Column("Neu_Id", TypeName = "char(18)")]
        public string NeuId { get; set; }

        [Required]
        [Column("Neu_Description")]
        [MaxLength(50)]
        [Display(Name = "Descripción")]
        public string NeuDescription { get; set; }

        [Display(Name = "Ruta")]
        [Required]
        [Column("Neu_Path", TypeName = "nvarchar(50)")]
        public string NeuPath { get; set; }
        [Display(Name = "Fecha de registro")]
        [Column("Neu_Date", TypeName = "datetime")]
        public DateTime NeuDate { get; set; }

        [Required]
        [Display(Name = "Sucursal")]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }

        [Display(Name = "Habilitado")]
        [Column("Sub_Enabled")]
        public bool SubEnabled { get; set; }

        [JsonIgnore]
        [InverseProperty("Neu")]
        public virtual ICollection<Training> Training { get; set; }

        [JsonIgnore]
        [ForeignKey("SubId")]
        [InverseProperty("NeuralNetworks")]
        public virtual Subsidiaries Sub { get; set; }
    }
}
