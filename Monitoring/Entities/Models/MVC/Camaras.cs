﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("Camaras", Schema = "Register")]
    public partial class Camaras
    {
        public Camaras()
        {
            Recognitions = new HashSet<Recognitions>();
        }

        [Column("Cam_Id", TypeName = "char(18)")]
        public string CamId { get; set; }

        [Display(Name = "Descripción")]
        [Required]
        [Column("Cam_Description", TypeName = "varchar(20)")]
        public string CamDescription { get; set; }

        [Display(Name = "Latitud")]
        [Column("Cam_Latitude")]
        public double? Latitude { get; set; }

        [Display(Name = "Longitud")]
        [Column("Cam_Longitude")]
        public double? Longitude { get; set; }

        [Display(Name = "Fecha de registro")]
        [Column("Cam_Date", TypeName = "datetime")]
        public DateTime CamDate { get; set; }

        [Display(Name = "Sucursal")]
        [Required]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }

        [Display(Name = "Ruta de conexión")]
        [Required]
        [Column("Cam_Source")]
        [MaxLength(200)]
        public string CamSource { get; set; }

        [Display(Name = "Habilitado")]
        [Column("Cam_Enabled")]
        public bool CamEnabled { get; set; }

        [JsonIgnore]
        [InverseProperty("Cam")]
        public virtual ICollection<Recognitions> Recognitions { get; set; }

        [ForeignKey("SubId")]
        [InverseProperty("Camaras")]
        [JsonIgnore]
        public virtual Subsidiaries Sub { get; set; }
    }
}
