﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("History_Tables", Schema = "History")]
    public partial class HistoryTables
    {
        [Column("His_Id", TypeName = "char(18)")]
        public string HisId { get; set; }
        [Required]
        [Column("His_Original", TypeName = "varchar(max)")]
        public string HisOriginal { get; set; }
        [Required]
        [Column("His_Update")]
        public string HisUpdate { get; set; }
        [Required]
        [Column("Usr_Id", TypeName = "char(18)")]
        public string UsrId { get; set; }
        [Column("His_Date", TypeName = "datetime")]
        public DateTime HisDate { get; set; }
        [Required]
        [Column("His_Table", TypeName = "varchar(max)")]
        public string HisTable { get; set; }

        [JsonIgnore]
        [ForeignKey("UsrId")]
        [InverseProperty("HistoryTables")]
        public virtual Users Usr { get; set; }
    }
}
