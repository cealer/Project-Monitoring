﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("DetailsRecognitions", Schema = "Detection")]
    public partial class DetailsRecognitions
    {
        [Column("Dre_Id", TypeName = "char(18)")]
        public string DreId { get; set; }
        [Required]
        [Column("Rec_Id", TypeName = "char(18)")]
        public string RecId { get; set; }
        [Required]
        [Column("Obj_Id", TypeName = "char(18)")]
        public string ObjId { get; set; }
        [Column("Dre_Percentage", TypeName = "decimal")]
        public decimal DrePercentage { get; set; }
        [Column("Dre_Enabled")]
        public bool DreEnabled { get; set; }
        [Column("Dre_Date", TypeName = "datetime")]
        public DateTime DreDate { get; set; }
        [Column("Alm_Id", TypeName = "char(18)")]
        public string AlmId { get; set; }


        [JsonIgnore]
        [ForeignKey("ObjId")]
        [InverseProperty("DetailsRecognitions")]
        public virtual Xobjects Obj { get; set; }

        [JsonIgnore]
        [ForeignKey("AlmId")]
        [InverseProperty("DetailsRecognitions")]
        public virtual Alarms Alm { get; set; }

        [JsonIgnore]
        [ForeignKey("RecId")]
        [InverseProperty("DetailsRecognitions")]
        public virtual Recognitions Rec { get; set; }
    }
}
