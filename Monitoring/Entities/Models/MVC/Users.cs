﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("Users", Schema = "Register")]
    public partial class Users
    {
        public Users()
        {
            HistoryTables = new HashSet<HistoryTables>();
        }

        [Column("Usr_Id", TypeName = "char(18)")]
        public string UsrId { get; set; }
        [Required]
        [Column("Usr_Username")]
        [MaxLength(50)]
        [Display(Name = "Usuario")]
        public string UsrUsername { get; set; }

        [Display(Name = "Contraseña")]
        [NotMapped]
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [NotMapped]
        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        [Display(Name = "Persona")]
        [Column("Per_Id", TypeName = "char(18)")]
        public string PerId { get; set; }

        [Display(Name = "Habilitado")]
        [Column("Usr_Enabled")]
        public bool UsrEnabled { get; set; }

        [JsonIgnore]
        [InverseProperty("Usr")]
        public virtual ICollection<HistoryTables> HistoryTables { get; set; }

        [JsonIgnore]
        [ForeignKey("PerId")]
        [InverseProperty("Users")]
        public virtual Persons Per { get; set; }

    }
}