﻿using Entities.Models.MVC;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("Recognitions", Schema = "Detection")]
    public partial class Recognitions
    {
        public Recognitions()
        {
            DetailsRecognitions = new HashSet<DetailsRecognitions>();
            DescriptionRecs = new HashSet<DescriptionRec>();
        }

        [Column("Rec_Id", TypeName = "char(18)")]
        public string RecId { get; set; }
        [Column("Per_Id", TypeName = "char(18)")]
        public string PerId { get; set; }
        [Required]
        [Column("Rec_Path")]
        public string RecPath { get; set; }
        [Column("Rec_PathFull")]
        public string RecFullPath { get; set; }
        [Column("Rec_Date", TypeName = "datetime")]
        public DateTime RecDate { get; set; }
        [Required]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }
        [Required]
        [Column("Cam_Id", TypeName = "char(18)")]
        public string CamId { get; set; }
        [Column("Alm_Id", TypeName = "char(18)")]
        public string AlmId { get; set; }
        [Column("Rec_Enabled")]
        public bool RecEnabled { get; set; }


        [JsonIgnore]
        [InverseProperty("Rec")]
        public virtual ICollection<DetailsRecognitions> DetailsRecognitions { get; set; }

        [InverseProperty("Rec")]
        public virtual ICollection<DescriptionRec> DescriptionRecs { get; set; }


        [JsonIgnore]
        [ForeignKey("CamId")]
        [InverseProperty("Recognitions")]
        public virtual Camaras Cam { get; set; }
        [JsonIgnore]
        [ForeignKey("AlmId")]
        [InverseProperty("Recognitions")]
        public virtual Alarms Alm { get; set; }
        [JsonIgnore]
        [ForeignKey("PerId")]
        [InverseProperty("Recognitions")]
        public virtual Persons Per { get; set; }
        [JsonIgnore]
        [ForeignKey("SubId")]
        [InverseProperty("Recognitions")]
        public virtual Subsidiaries Sub { get; set; }
    }
}
