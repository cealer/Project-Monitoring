﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("Alarms", Schema = "Register")]
    public partial class Alarms
    {
        public Alarms()
        {
            DetailsAlarms = new HashSet<DetailsAlarms>();
            DetailsRecognitions = new HashSet<DetailsRecognitions>();
            Recognitions = new HashSet<Recognitions>();
        }

        [Column("Alm_Id", TypeName = "char(18)")]
        public string AlmId { get; set; }

        [Display(Name = "Descripción")]
        [Required]
        [Column("Alm_Description")]
        [MaxLength(50)]
        public string AlmDescription { get; set; }

        [Display(Name = "Fecha de registro")]
        [Column("Alm_Date", TypeName = "datetime")]
        public DateTime AlmDate { get; set; }

        [Display(Name = "Persona asignada")]
        [Column("Per_Id", TypeName = "char(18)")]
        public string PerId { get; set; }

        [Display(Name = "Objeto asignado")]
        [Column("Obj_Id", TypeName = "char(18)")]
        public string ObjId { get; set; }

        [Display(Name = "Sucursal")]
        [Required]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }

        [Display(Name = "Habilitado")]
        [Column("Alm_Enabled")]
        public bool AlmEnabled { get; set; }

        [JsonIgnore]
        [InverseProperty("Alm")]
        public virtual ICollection<DetailsAlarms> DetailsAlarms { get; set; }

        [JsonIgnore]
        [InverseProperty("Alm")]
        public virtual ICollection<DetailsRecognitions> DetailsRecognitions { get; set; }

        [JsonIgnore]
        [ForeignKey("ObjId")]
        [InverseProperty("Alarms")]
        public virtual Xobjects Obj { get; set; }

        [JsonIgnore]
        [ForeignKey("PerId")]
        [InverseProperty("Alarms")]
        public virtual Persons Per { get; set; }

        [JsonIgnore]
        [InverseProperty("Alm")]
        public virtual ICollection<Recognitions> Recognitions { get; set; }

        [JsonIgnore]
        [ForeignKey("SubId")]
        [InverseProperty("Alarms")]
        public virtual Subsidiaries Sub { get; set; }
    }
}
