﻿using Entities.Validation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities
{
    [Table("Persons", Schema = "Register")]
    public partial class Persons
    {
        public Persons()
        {
            Alarms = new HashSet<Alarms>();
            Recognitions = new HashSet<Recognitions>();
        }

        [Column("Per_Id", TypeName = "char(18)")]
        public string PerId { get; set; }
        [Required(ErrorMessage = "Campo obligatorio")]
        [Display(Name = "Nombres")]
        [Column("Per_Name")]
        [MaxLength(50, ErrorMessage = "No puede exceder los {1} caractéres")]
        [MinLength(2, ErrorMessage = "El campo {0} debe contener al menos {1} caractéres")]
        public string PerName { get; set; }
        [Required(ErrorMessage = "Campo obligatorio")]
        [Display(Name = "Apellidos")]
        [Column("Per_LastName")]
        [MaxLength(50, ErrorMessage = "No puede exceder los {1} caractéres")]
        [MinLength(2, ErrorMessage = "El campo {0} debe contener al menos {1} caractéres")]
        public string PerLastName { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        [Display(Name = "DNI")]
        [MinLength(8, ErrorMessage = "No puede exceder los {1} caractéres")]
        [MaxLength(8, ErrorMessage = "El campo {0} debe contener al menos {1} caractéres")]
        [Column("Per_Dni", TypeName = "char(8)")]
        public string PerDni { get; set; }

        [NotMapped]
        [Display(Name = "Nombres y Apellidos")]
        public string FullName { get { return string.Format("{0} {1}", this.PerName, this.PerLastName); } }

        [Required(ErrorMessage = "Campo obligatorio")]
        [Display(Name = "Email")]
        [Column("Per_Email")]
        [MaxLength(50, ErrorMessage = "No puede exceder los {1} caractéres")]
        [MinLength(2, ErrorMessage = "El campo {0} debe contener al menos {1} caractéres")]
        [DataType(DataType.EmailAddress)]
        public string PerEmail { get; set; }
        [Required(ErrorMessage = "Campo obligatorio")]
        [Display(Name = "Ruta")]
        [Column("Per_Path")]
        [MaxLength(50, ErrorMessage = "No puede exceder los {1} caractéres")]
        public string PerPath { get; set; }
        [Column("Per_Phone")]
        [MaxLength(16, ErrorMessage = "El campo {0} debe contener al menos {1} caractéres")]
        [Display(Name = "Celular")]
        public string PerPhone { get; set; }
        [Column("Per_Date", TypeName = "datetime")]
        [Display(Name = "Fecha de registro")]
        public DateTime PerDate { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        [Display(Name = "Sexo")]
        [Column("Per_Gender", TypeName = "char(1)")]
        public string PerGender { get; set; }

        [DataType(DataType.Date)]
        [DateValidation(ErrorMessage = "Fecha de nacimiento inválida")]
        [Display(Name = "Fecha De Nacimiento"),]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Column("Per_DateBirth", TypeName = "datetime")]
        public DateTime? PerDateBirth { get; set; }

        [Required(ErrorMessage = "Campo obligatorio")]
        [Display(Name = "Sucursal")]
        [Column("Sub_Id", TypeName = "char(18)")]
        public string SubId { get; set; }
        [Display(Name = "Cargo")]
        [Column("Cat_Id", TypeName = "char(18)")]
        public string CatId { get; set; }
        [Display(Name = "Estado")]
        [Column("Per_Enabled")]
        public bool PerEnabled { get; set; }

        [Column("Per_Photo")]
        [Display(Name = "Foto")]
        public string PerPhoto { get; set; }


        [InverseProperty("Per")]
        public virtual ICollection<Users> Users { get; set; }
        [JsonIgnore]
        [InverseProperty("Per")]
        public virtual ICollection<Alarms> Alarms { get; set; }
        [JsonIgnore]
        [InverseProperty("Per")]
        public virtual ICollection<Recognitions> Recognitions { get; set; }
        [JsonIgnore]
        [ForeignKey("CatId")]
        [InverseProperty("Persons")]
        public virtual Categories Cat { get; set; }
        [JsonIgnore]
        [ForeignKey("SubId")]
        [InverseProperty("Persons")]
        public virtual Subsidiaries Sub { get; set; }
    }
}
