﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;

namespace Entities.Utilities
{
    public class Generate
    {
        private readonly MonitoringContext _context;

        public Generate(MonitoringContext context)
        {
            _context = context;

        }

        public Generate()
        {

        }

        //Generar ID
        public string GenerateId(string code, string Table)
        {
            string cod = "";
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = $"SELECT '{code}'+RIGHT('000000000000000' + RTRIM(LTRIM(STR(COUNT(*)+1))),15) FROM {Table}";
                _context.Database.OpenConnection();
                var r = command.ExecuteScalar();
                cod = r.ToString();
            }

            return cod;
        }

        //Generar ID
        public async Task<string> GenerateIdAsync(string code, string Table)
        {
            string cod;
            using (var command = _context.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = $"SELECT '{code}'+RIGHT('000000000000000' + RTRIM(LTRIM(STR(COUNT(*)+1))),15) FROM {Table}";
                _context.Database.OpenConnection();
                var r = await command.ExecuteScalarAsync();
                cod = r.ToString();
            }

            return cod;
        }

        //
        public static string TimeAgo(DateTime dt)
        {
            TimeSpan span = DateTime.Now - dt;
            if (span.Days > 365)
            {
                int years = (span.Days / 365);
                if (span.Days % 365 != 0)
                    years += 1;
                return String.Format("Hace {0} {1}",
                years, years == 1 ? "año" : "años");
            }
            if (span.Days > 30)
            {
                int months = (span.Days / 30);
                if (span.Days % 31 != 0)
                    months += 1;
                return String.Format("Hace {0} {1}",
                months, months == 1 ? "mes" : "meses");
            }
            if (span.Days > 0)
                return String.Format("Hace {0} {1}",
                span.Days, span.Days == 1 ? "día" : "días");
            if (span.Hours > 0)
                return String.Format("Hace {0} {1}",
                span.Hours, span.Hours == 1 ? "hora" : "horas");
            if (span.Minutes > 0)
                return String.Format("Hace {0} {1}",
                span.Minutes, span.Minutes == 1 ? "minuto" : "minutos");
            if (span.Seconds > 5)
                return String.Format("Hace {0} segundos", span.Seconds);
            if (span.Seconds <= 5)
                return "Justo ahora";
            return string.Empty;
        }
    }
}
