﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Utilities
{
    public class Tools /*:*/ /*IDisposable*/
    {
        private MonitoringContext _context;
        private Generate _Utilities;
        private string PathDatasetPerson = @"C:\Users\CealeR\Tesis\Monitoring\Monitoring\wwwroot\Dataset_Person";
        private string PathNNPerson = @"C:\Users\CealeR\Tesis\NN\NEU000000000000001";
        private string PathDatasetObject = @"C:\Users\CealeR\Tesis\Monitoring\Monitoring\wwwroot\Dataset_Object";
        private string pathPython = @"C:/Users/CealeR/AppData/Local/Programs/Python/Python35";
        private string PathRetrain = @"C:/Users/CealeR/Tesis/algorithm/retrain.py";
        private string PathBottlenecks = @"C:/Users/CealeR/Tesis/NN/NEU000000000000001/bottlenecks";
        private string TrainingSteps = "500";
        private string ModelDir = @"C:/Users/CealeR/Tesis/NN/NEU000000000000001/inception";
        private string OutputGraph = @"C:/Users/CealeR/Tesis/NN/NEU000000000000001/retrained_graph.pb";
        private string OutputLabels = @"C:/Users/CealeR/Tesis/NN/NEU000000000000001/retrained_labels.txt";
        private string ImageDir = @"C:/Users/CealeR/Tesis/Monitoring/Monitoring/wwwroot/Dataset_Person";

        private string pathCamara = @"C:\Users\CealeR\Tesis\algorith\video_facial_landmarks.py";
        private string shapePredictor = @"C:\Users\CealeR\Tesis\algorith\shape_predictor_68_face_landmarks.dat";


        public string PathAlgorithm = @"C:\Users\CealeR\Tesis\algorithm\ImageProcessing.py";

        public Tools()
        {
            _context = new MonitoringContext();
            _Utilities = new Generate(_context);
        }

        public void RunImageProcessing(string perId, string PhotoPath)
        {
            var p = new System.Diagnostics.Process();
            p.StartInfo.FileName = $"{pathPython}/python.exe";
            p.StartInfo.Arguments = $"{PathAlgorithm} {PathDatasetPerson}/{PhotoPath}";
            p.StartInfo.WorkingDirectory = $"{PathDatasetPerson}/{perId}";
            //p.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
        }


        private void RunCamara()
        {
            var p = new System.Diagnostics.Process();
            p.StartInfo.FileName = $"{pathPython}/python.exe";
            p.StartInfo.Arguments = $"{PathRetrain} --bottleneck_dir={PathBottlenecks} " +
    $"--how_many_training_steps {TrainingSteps} " +
    $"--model_dir={ModelDir} " +
    $"--output_graph={OutputGraph} " +
    $"--output_labels={OutputLabels} " +
    $"--image_dir {ImageDir}";
            p.StartInfo.WorkingDirectory = $"{PathNNPerson}";
            //p.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();

        }

        public void RunClassifier()
        {
            var p = new System.Diagnostics.Process();
            p.StartInfo.FileName = $"{pathPython}/python.exe";
            p.StartInfo.Arguments = $"{PathRetrain} --bottleneck_dir={PathBottlenecks} " +
                $"--how_many_training_steps {TrainingSteps} " +
                $"--model_dir={ModelDir} " +
                $"--output_graph={OutputGraph} " +
                $"--output_labels={OutputLabels} " +
                $"--image_dir {ImageDir}";
            p.StartInfo.WorkingDirectory = $"{PathNNPerson}";
            //p.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
        }

        //public void Dispose()
        //{
        //    _context.Dispose();
        //}

        public string MoveToDataSetPerson(string camId, string DNI, string Photo, string Percentage)
        {
            string r = string.Empty;

            FileInfo FilePath = new FileInfo(Photo);

            //var PhotoPath = $"{ DNI }/{ FilePath.Name}.jpg";

            //var PhotoCopy = $"{PathDatasetPerson}/{ PhotoPath}";

            //if (!File.Exists(PhotoCopy))
            //{
            //    File.Copy(Photo, $"{ PathDatasetPerson}/{ PhotoPath}");
            //}
            //r = SavePhotoPerson(camId, DNI, $"FullDetection/{PhotoPath}", $"Dataset_Person/{PhotoPath}", Convert.ToDecimal(Percentage));
            r = SavePhotoPerson(camId, DNI, $"FullDetection/{FilePath.Name}", $"Detections_Person/{FilePath.Name}", Convert.ToDecimal(Percentage));
            return r;
        }

        public string MoveToDataSetPersonAzure(string camId, string DNI, string Photo, string Percentage)
        {
            string r = string.Empty;

            FileInfo FilePath = new FileInfo(Photo);

            //var PhotoPath = $"{ DNI }/{ FilePath.Name}.jpg";

            //var PhotoCopy = $"{PathDatasetPerson}/{ PhotoPath}";

            //if (!File.Exists(PhotoCopy))
            //{
            //    File.Copy(Photo, $"{ PathDatasetPerson}/{ PhotoPath}");
            //}
            //r = SavePhotoPerson(camId, DNI, $"FullDetection/{PhotoPath}", $"Dataset_Person/{PhotoPath}", Convert.ToDecimal(Percentage));
            r = SavePhotoPerson(camId, DNI, $"FullDetection/{FilePath.Name}", $"Detections_Person/{FilePath.Name}", Convert.ToDecimal(Percentage));
            return r;
        }

        public async Task<string> MoveToDataSetPersonAsync(string camId, string DNI, string Photo, string Percentage)
        {
            string r = string.Empty;

            FileInfo FilePath = new FileInfo(Photo);

            var PhotoPath = $"{ DNI }/{ FilePath.Name}.jpg";

            var PhotoCopy = $"{PathDatasetPerson}/{ PhotoPath}";

            if (!File.Exists(PhotoCopy))
            {
                File.Copy(Photo, $"{ PathDatasetPerson}/{ PhotoPath}");
                r = await SavePhotoPersonAsync(camId, DNI, $"Dataset_Person/{PhotoPath}", Convert.ToDecimal(Percentage));
            }
            return r;
        }

        //public async Task MoveToDataSetObject(string camId, string xobjectId, string Photo, string Percentage)
        //{
        //    FileInfo FilePath = new FileInfo(Photo);

        //    var PhotoPath = $"{ xobjectId }/{ FilePath.Name}.jpg";

        //    var PhotoCopy = $"{PathDatasetObject}/{ PhotoPath}";

        //    //if (!File.Exists(PhotoCopy))
        //    //{
        //    //  File.Copy($@"{Photo}", $"{PathDatasetObject}/{PhotoPath}");
        //    await SavePhotoObject(camId, xobjectId, $"Dataset/{PhotoPath}", Convert.ToDecimal(Percentage));
        //    //}
        //}

        public void SavePhotoObject(string RecId, string xObjectId, decimal Percentage)
        {
            var xobject = _context.Xobjects.Find(xObjectId);
            var Rec = _context.Recognitions.Find(RecId);
            var detrec = new DetailsRecognitions
            {
                DreDate = DateTime.Now,
                DreEnabled = true,
                DreId = _Utilities.GenerateId("REC", "Detection.DetailsRecognitions"),
                DrePercentage = Percentage,
                ObjId = xObjectId,
                RecId = RecId
            };

            foreach (var item in _context.Alarms)
            {
                if (item.PerId == Rec.PerId)
                {
                    detrec.AlmId = item.AlmId;
                }
            }

            _context.DetailsRecognitions.Add(detrec);
            _context.SaveChanges();
        }

        public string SavePhotoPerson(string camId, string personId, string PhotoFull, string Photo, decimal Percentage)
        {
            try
            {
                var camara = _context.Camaras.Find(camId);

                var person = _context.Persons.Find(personId);

                var rec = _context.Recognitions.Add(new Recognitions
                {
                    CamId = camara.CamId,
                    PerId = person.PerId,
                    RecPath = Photo,
                    //RecPercentage = Percentage,
                    RecDate = DateTime.Now,
                    RecEnabled = true,
                    SubId = person.SubId,
                    RecFullPath = PhotoFull,
                    RecId = _Utilities.GenerateId("REC", "Detection.Recognitions")
                });

                _context.SaveChanges();

                return rec.Entity.RecId;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

        }

        public async Task<string> SavePhotoPersonAsync(string camId, string personId, string Photo, decimal Percentage)
        {
            var camara = _context.Camaras.Find(camId);

            var person = _context.Persons.Find(personId);

            var rec = _context.Recognitions.Add(new Recognitions
            {
                CamId = camara.CamId,
                PerId = person.PerId,
                RecPath = Photo,
                //RecPercentage = Percentage,
                RecDate = DateTime.Now,
                RecEnabled = true,
                SubId = person.SubId,
                RecId = await _Utilities.GenerateIdAsync("REC", "Detection.Recognitions")
            });

            await _context.SaveChangesAsync();
            return rec.Entity.RecId;
        }

    }
}