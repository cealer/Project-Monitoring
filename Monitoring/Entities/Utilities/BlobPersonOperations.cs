﻿using Microsoft.ProjectOxford.Face;
using Microsoft.ProjectOxford.Face.Contract;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Utilities
{
    public class BlobPersonOperations
    {
        const string subscriptionKey = "667ff4c7d73442698763ccec18a7f13b";

        public async System.Threading.Tasks.Task CreatePersonAsync(string imageUrl, string personFullName, string DNI, string personGroupId)
        {
            var faceServiceClient = new FaceServiceClient(subscriptionKey);

            // Define Cesar
            CreatePersonResult person = await faceServiceClient.CreatePersonAsync(
                // Id of the person group that the person belonged to
                personGroupId.ToLower(),
                // Name of the person
                $"{personFullName}-{DNI}"
            );

            // Detect faces in the image and add to Anna
            await faceServiceClient.AddPersonFaceAsync(
                personGroupId.ToLower(), person.PersonId, imageUrl);

            await faceServiceClient.TrainPersonGroupAsync(personGroupId.ToLower());

        }

    }
}
