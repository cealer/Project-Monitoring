﻿using Microsoft.ProjectOxford.Face;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.ProjectOxford.Vision.Contract;
using Microsoft.ProjectOxford.Vision;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using System;

namespace Entities.Utilities
{
    public class RecOperations
    {
        private MonitoringContext _context;
        private Generate _generate;
        IConfiguration _iconfiguration;


        //Init when use notificationHub(Get connectionString and HubName from appsettings.json)
        public RecOperations(MonitoringContext context, IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
            _context = context;
            _generate = new Generate(context);
        }


        public RecOperations(MonitoringContext context)
        {
            _context = context;
            _generate = new Generate(context);
        }

        public async Task<string> IdentifyPerson(string personPhoto, string personGroupId)
        {

            const string subscriptionKey = "667ff4c7d73442698763ccec18a7f13b";
            var faceServiceClient = new FaceServiceClient(subscriptionKey);

            var faces = await faceServiceClient.DetectAsync(personPhoto);
            var faceIds = faces.Select(face => face.FaceId).ToArray();

            var results = await faceServiceClient.IdentifyAsync(personGroupId.ToLower(), faceIds);

            foreach (var identifyResult in results)
            {
                if (identifyResult.Candidates.Length == 0)
                {
                    //Alertar
                    return $"{_context.Persons.Where(x => x.PerName == "DESCONOCIDO").FirstOrDefault().PerId}";
                }
                else
                {
                    // Get top 1 among all candidates returned
                    var candidateId = identifyResult.Candidates[0].PersonId;
                    var person = await faceServiceClient.GetPersonAsync(personGroupId.ToLower(), candidateId);
                    var dni = person.Name.Split('-')[1];

                    return $"{_context.Persons.Where(x => x.PerDni == dni).FirstOrDefault().PerId}";
                }
            }

            return "";
        }

        public async Task DetectAlarm(string personId)
        {
            var findAlarm = _context.Alarms.SingleOrDefault(x => x.PerId == personId);

            if (findAlarm != null)
            {
                //Send Notification
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://notificationhub20170827083407.azurewebsites.net");

                    MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");

                    client.DefaultRequestHeaders.Accept.Add(contentType);
                    HttpResponseMessage res = client.GetAsync($"api/values/TestRec/{findAlarm.AlmDescription}").Result;
                }
            }

            await Task.Yield();
        }

        public async Task DescribeImage(string imageUrl, string recId)
        {
            var VisionServiceClient = new VisionServiceClient("413d0aea55e84fcaa45b2448651c2184");
            var analysisResult = await VisionServiceClient.DescribeAsync(imageUrl, 3);
            await LogAnalysisResultAsync(analysisResult, recId);
        }

        private async Task LogAnalysisResultAsync(AnalysisResult result, string recId)
        {
            if (result == null)
            {
                return;
            }

            if (result.Description != null)
            {
                foreach (var caption in result.Description.Captions)
                {
                    _context.DescriptionRec.Add(new Models.MVC.DescriptionRec
                    {
                        DesId = _generate.GenerateIdAsync("DES", "Detection.DescriptionRec").Result,
                        RecId = recId,
                        Description = caption.Text
                    });
                    await _context.SaveChangesAsync();
                    //r = "  Caption : " + caption.Text + "; Confidence : " + caption.Confidence;
                }
            }
        }
    }
}