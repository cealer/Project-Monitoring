﻿using Microsoft.Azure.NotificationHubs;

namespace HubNotification
{
    public class AlertHub
    {
        public static NotificationHubClient hub;

        public static async void SendNotificationAsync(string message, string connectionString, string hubName)
        {
            hub = NotificationHubClient.CreateClientFromConnectionString(connectionString, hubName);
            await hub.SendGcmNativeNotificationAsync("{ \"data\" : {\"message\":\"" + message + "\"}}");
        }
    }
}