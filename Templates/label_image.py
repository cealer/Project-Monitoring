import tensorflow as tf, sys

import os

# change this as you see fit
image_path = sys.argv[1]

# Read in the image_data
image_data = tf.gfile.FastGFile(image_path, 'rb').read()

# Loads label file, strips off carriage return
#label_lines = [line.rstrip() for line in tf.gfile.GFile("/tf_files/Tensorflow/retrained_labels.txt")]
label_lines = [line.rstrip() for line in tf.gfile.GFile("C:/Users/CealeR/House/Tensorflow/retrained_labels.txt")]

# Unpersists graph from file
#with tf.gfile.FastGFile("/tf_files/Tensorflow/retrained_graph.pb", 'rb') as f:
with tf.gfile.FastGFile("C:/Users/CealeR/House/Tensorflow/retrained_graph.pb", 'rb') as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
    _ = tf.import_graph_def(graph_def, name='')

with tf.Session() as sess:
    # Feed the image_data as input to the graph and get first prediction
    softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
    
    predictions = sess.run(softmax_tensor, \
             {'DecodeJpeg/contents:0': image_data})
    
    # Sort to show labels of first prediction in order of confidence
    top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
    
    #for node_id in top_k:
    #    human_string = label_lines[node_id]
    #    score = (predictions[0][node_id])*100
    #    print('%s (score = %.5f)' % (human_string, score))
    #
    #print("Predicción más alta: %s Score: %.5f " % (human_string,score))
    print(label_lines[top_k[0]]+ ","+ str(predictions[0][top_k[0]]) )
    #print(sys.argv[1])
    #print("Ejecutando dotnet")
    #Copycmd='dotnet "C:/Users/Cesar/Documents/Visual Studio 2015/Projects/ToolFaceDetection/src/ToolFaceDetection/bin/Debug/netcoreapp1.0/ToolFaceDetection.dll" 1 '+label_lines[top_k[0]]+" " +sys.argv[1]+" "+ str(predictions[0][top_k[0]])       
    #print(Copycmd)
    #os.system(Copycmd)
    #print("Terminado proceso dotnet")
    #print("C:/Users/Cesar/Documents/Visual Studio 2015/Projects/House/Tensorflow/%s" % (label_lines[top_k[0]]) )
