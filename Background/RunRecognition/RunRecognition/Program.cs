﻿using Entities.Utilities;
using System.Net.Http.Headers;
using System;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ProjectOxford.Face;

namespace RunRecognition
{
    class Program
    {
        public static string pathLabelImagePerson = @"C:\Users\CealeR\Tesis\NN\NEU000000000000001/label_image.py";
        public static string pathLabelImageObject = @"C:\Users\CealeR\Tesis\NN\NEU000000000000002/label_image.py";
        public static string pathDetectionsPerson = @"C:\Users\CealeR\Tesis\Monitoring\Monitoring\wwwroot\Detections_Person";
        public static string pathFullPerson = @"C:\Users\CealeR\Tesis\Monitoring\Monitoring\wwwroot\FullDetection";
        //  public static string pathDetectionsObject = @"C:\Users\CealeR\Tesis\Monitoring\Monitoring\wwwroot\Detections_Object";
        public static string pathPython = @"C:/Users/CealeR/AppData/Local/Programs/Python/Python35";
        public static string photo;
        public static string IdRecognition;

        const string subscriptionKey = "4271e94dd8e74862a4bb07e7ed0bbf7a";

        const string subscriptionKeyA = "a1c35e21cd6844758883dffa17957f06";

        const string uriBase = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0/detect";
        const string uriBaseA = "https://westcentralus.api.cognitive.microsoft.com/vision/v1.0/analyze";
        static void Main(string[] args)
        {

            Console.ForegroundColor = ConsoleColor.White;

            try

            {

                Console.WriteLine("Enter your Cognitive Services Face API Key:");

                var apiKey = subscriptionKey;

                Console.WriteLine();
                
                Console.WriteLine("Enter the url of the image you want analyze:");

                var imageUrl = Console.ReadLine();

                Console.WriteLine();

                DetectFaces(apiKey, imageUrl).Wait();

            }

            catch (Exception ex)

            {

                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine(ex.Message);

                Console.WriteLine();

            }



            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine();

            Console.WriteLine("Press any key to exit...");

            Console.ReadLine();



            //Console.WriteLine("Detect faces:");
            //Console.Write("Enter the path to an image with faces that you wish to analzye: ");
            //string imageFilePath = Console.ReadLine();

            //// Execute the REST API call.
            //MakeAnalysisRequest(imageFilePath);

            //Console.WriteLine("\nPlease wait a moment for the results to appear. Then, press Enter to exit...\n");
            //Console.ReadLine();

            //// Get the path and filename to process from the user.
            //Console.WriteLine("Analyze an image:");
            //Console.Write("Enter the path to an image you wish to analzye: ");

            //// Execute the REST API call.
            //MakeAnalysisRequestA(imageFilePath);

            //Console.WriteLine("\nPlease wait a moment for the results to appear. Then, press Enter to exit...\n");
            //Console.ReadLine();

            //Run();
        }


        private static async Task DetectFaces(string apiKey, string imageUrl)

        {

            var faceServiceClient = new FaceServiceClient(apiKey);

            var faces = await faceServiceClient.DetectAsync(imageUrl, true, true,

                new FaceAttributeType[] { FaceAttributeType.Gender, FaceAttributeType.Age, FaceAttributeType.Smile, FaceAttributeType.Glasses });


            Console.ForegroundColor = ConsoleColor.Cyan;

            var faceCount = 0;



            foreach (var face in faces)

            {

                faceCount += 1;



                Console.WriteLine("Face #: {0}", faceCount);

                Console.WriteLine(">Face Id: {0}", face.FaceId.ToString());

                Console.WriteLine(">Rectangle:");

                Console.WriteLine(">>Left: {0}", face.FaceRectangle.Left);

                Console.WriteLine(">>Top: {0}", face.FaceRectangle.Top);

                Console.WriteLine(">>Width: {0}", face.FaceRectangle.Width);

                Console.WriteLine(">>Height: {0}", face.FaceRectangle.Height);

                Console.WriteLine(">Gender: {0}", face.FaceAttributes.Gender);

                Console.WriteLine(">Age: {0:#}", face.FaceAttributes.Age);

                Console.WriteLine(">Smiling: {0}", face.FaceAttributes.Smile.ToString());

                Console.WriteLine(">Glasses: {0}", face.FaceAttributes.Glasses.ToString());

                Console.WriteLine("");

            }



            if (faceCount == 0)

            {

                Console.ForegroundColor = ConsoleColor.Yellow;

                Console.WriteLine("No faces were found in the images.");

            }

        }



        //AZURE

        /// <summary>
        /// Gets the analysis of the specified image file by using the Computer Vision REST API.
        /// </summary>
        /// <param name="imageFilePath">The image file.</param>
        static async void MakeAnalysisRequestA(string imageFilePath)
        {
            HttpClient client = new HttpClient();

            // Request headers.
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKeyA);

            // Request parameters. A third optional parameter is "details".
            string requestParameters = "visualFeatures=Categories,Description,Color&language=en";

            // Assemble the URI for the REST API Call.
            string uri = uriBaseA + "?" + requestParameters;

            HttpResponseMessage response;

            // Request body. Posts a locally stored JPEG image.
            byte[] byteData = GetImageAsByteArray(imageFilePath);

            using (ByteArrayContent content = new ByteArrayContent(byteData))
            {
                // This example uses content type "application/octet-stream".
                // The other content types you can use are "application/json" and "multipart/form-data".
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                // Execute the REST API call.
                response = await client.PostAsync(uri, content);

                // Get the JSON response.
                string contentString = await response.Content.ReadAsStringAsync();

                // Display the JSON response.
                Console.WriteLine("\nResponse:\n");
                Console.WriteLine(JsonPrettyPrint(contentString));
            }
        }


        /// <summary>
        /// Gets the analysis of the specified image file by using the Computer Vision REST API.
        /// </summary>
        /// <param name="imageFilePath">The image file.</param>
        static async void MakeAnalysisRequest(string imageFilePath)
        {
            HttpClient client = new HttpClient();

            // Request headers.
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);

            // Request parameters. A third optional parameter is "details".
            string requestParameters = "returnFaceId=true&returnFaceLandmarks=false&returnFaceAttributes=age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise";
            //string requestParameters = "returnFaceId=true";

            // Assemble the URI for the REST API Call.
            string uri = uriBase + "?" + requestParameters;

            HttpResponseMessage response;

            // Request body. Posts a locally stored JPEG image.
            byte[] byteData = GetImageAsByteArray(imageFilePath);

            using (ByteArrayContent content = new ByteArrayContent(byteData))
            {
                // This example uses content type "application/octet-stream".
                // The other content types you can use are "application/json" and "multipart/form-data".
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                // Execute the REST API call.
                response = await client.PostAsync(uri, content);

                // Get the JSON response.
                string contentString = await response.Content.ReadAsStringAsync();

                // Display the JSON response.
                Console.WriteLine("\nResponse:\n");
                Console.WriteLine(JsonPrettyPrint(contentString));
            }
        }


        /// <summary>
        /// Returns the contents of the specified file as a byte array.
        /// </summary>
        /// <param name="imageFilePath">The image file to read.</param>
        /// <returns>The byte array of the image data.</returns>
        static byte[] GetImageAsByteArray(string imageFilePath)
        {
            FileStream fileStream = new FileStream(imageFilePath, FileMode.Open, FileAccess.Read);
            BinaryReader binaryReader = new BinaryReader(fileStream);
            return binaryReader.ReadBytes((int)fileStream.Length);
        }


        /// <summary>
        /// Formats the given JSON string by adding line breaks and indents.
        /// </summary>
        /// <param name="json">The raw JSON string to format.</param>
        /// <returns>The formatted JSON string.</returns>
        static string JsonPrettyPrint(string json)
        {
            if (string.IsNullOrEmpty(json))
                return string.Empty;

            json = json.Replace(Environment.NewLine, "").Replace("\t", "");

            StringBuilder sb = new StringBuilder();
            bool quote = false;
            bool ignore = false;
            int offset = 0;
            int indentLength = 3;

            foreach (char ch in json)
            {
                switch (ch)
                {
                    case '"':
                        if (!ignore) quote = !quote;
                        break;
                    case '\'':
                        if (quote) ignore = !ignore;
                        break;
                }

                if (quote)
                    sb.Append(ch);
                else
                {
                    switch (ch)
                    {
                        case '{':
                        case '[':
                            sb.Append(ch);
                            sb.Append(Environment.NewLine);
                            sb.Append(new string(' ', ++offset * indentLength));
                            break;
                        case '}':
                        case ']':
                            sb.Append(Environment.NewLine);
                            sb.Append(new string(' ', --offset * indentLength));
                            sb.Append(ch);
                            break;
                        case ',':
                            sb.Append(ch);
                            sb.Append(Environment.NewLine);
                            sb.Append(new string(' ', offset * indentLength));
                            break;
                        case ':':
                            sb.Append(ch);
                            sb.Append(' ');
                            break;
                        default:
                            if (ch != ' ') sb.Append(ch);
                            break;
                    }
                }
            }

            return sb.ToString().Trim();
        }







        //Offline
        public static void Run()
        {
            InitWhatcher(pathDetectionsPerson, "Person");
            //while (Console.Read() != 'q') ;
            while (!Console.KeyAvailable) System.Threading.Thread.Sleep(50);
        }

        private static void InitWhatcher(string path, string des)
        {
            // Create a new FileSystemWatcher and set its properties.
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = path;

            /* Watch for changes in LastAccess and LastWrite times, and
               the renaming of files or directories. */
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite
               | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            // Only watch text files.
            watcher.Filter = "*.jpg";

            // Add event handlers.
            //watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            //watcher.Deleted += new FileSystemEventHandler(OnChanged);
            watcher.Renamed += new RenamedEventHandler(OnRenamed);

            // Begin watching.
            watcher.EnableRaisingEvents = true;
            Console.WriteLine($"Iniciando {des}");
            //// Wait for the user to quit the program.
            Console.WriteLine("Presiona \'q\' para salir.");
        }

        static void PersonClassifier(FileSystemEventArgs e)
        {
            try
            {
                // Specify what is done when a file is changed, created, or deleted.
                Console.WriteLine("File: " + e.Name + " " + e.ChangeType);
                //if (e.ChangeType == WatcherChangeTypes.Created)
                //{
                FileInfo PersonLabel = new FileInfo($"{pathLabelImagePerson}");
                FileInfo DetectionPathPerson = new FileInfo($"{pathDetectionsPerson}/{e.Name}");
                //var cmd = $@"py -3.5 {pathLabelImagePerson} {pathDetectionsPerson}/{e.Name}";

                var p = new Process();

                p.StartInfo.FileName = $"{pathPython}/python.exe";
                p.StartInfo.Arguments = $"{PersonLabel.FullName} {DetectionPathPerson.FullName}";
                p.StartInfo.WorkingDirectory = $"{pathPython}";
                //p.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                Stopwatch sw = Stopwatch.StartNew();
                p.Start();
                string output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
                sw.Stop();
                Console.WriteLine(" Tiempo clasificador: " + sw.ElapsedMilliseconds + "ms.");
                Console.WriteLine(output);

                var result = output.Split(',');

                Tools aux = new Tools();

                //using (var tools = new Tools())
                //{
                IdRecognition = aux.MoveToDataSetPersonAzure(result[0], result[1], DetectionPathPerson.FullName, result[2]);

                photo = e.Name;

                //ObjectClassifier(photo, IdRecognition);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error en Person detect {ex.Message}");
            }
            //}

            // await Task.Yield();
        }

        static void ObjectClassifier(string e, string IdRecognition)
        {
            try
            {
                FileInfo objectLabel = new FileInfo($"{pathLabelImageObject}");
                FileInfo DetectionPathPerson = new FileInfo($"{pathDetectionsPerson}/{e}");
                //  var cmd = $@"py -3.5 {pathLabelImagePerson} {pathDetectionsPerson}/{e}";

                var p = new Process();

                p.StartInfo.FileName = $"{pathPython}/python.exe";
                p.StartInfo.Arguments = $"{objectLabel.FullName} {DetectionPathPerson.FullName}";
                p.StartInfo.WorkingDirectory = $"{pathPython}";
                //p.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                Stopwatch sw = Stopwatch.StartNew();
                p.Start();
                string output = p.StandardOutput.ReadToEnd();
                p.WaitForExit();
                sw.Stop();
                Console.WriteLine(" Tiempo clasificador: " + sw.ElapsedMilliseconds + "ms.");
                Console.WriteLine(output);

                var result = output.Split(',');

                Tools tools = new Tools();
                //await tools.SavePhotoObject(IdRecognition, result[0], Convert.ToDecimal(result[1]));
                tools.SavePhotoObject(IdRecognition, result[0], Convert.ToDecimal(result[1]));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error object detect {ex.Message}");
            }
            // Specify what is done when a file is changed, created, or deleted.
            //Console.WriteLine("File: " + e.Name + " " + e.ChangeType);
            //if (e.ChangeType == WatcherChangeTypes.Created)
            //{

            //using (var tools = new Tools())
            //{
            //}
            //await Task.Yield();
        }

        // Define the event handlers.
        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            Stopwatch sw = Stopwatch.StartNew();
            Task task = new Task(() => PersonClassifier(e));
            task.RunSynchronously();
            //PersonClassifier(e);
            sw.Stop();
            Console.WriteLine(" Tiempo Total: " + sw.ElapsedMilliseconds + "ms.");

            //var xobject = ObjectClassifier(photo, IdRecognition);
            // await Task.WhenAny(person);
            //await Task.WhenAll(person);

            //try
            //{
            //    // Specify what is done when a file is changed, created, or deleted.
            //    Console.WriteLine("File: " + e.Name + " " + e.ChangeType);
            //    //if (e.ChangeType == WatcherChangeTypes.Created)
            //    //{
            //    FileInfo PersonLabel = new FileInfo($"{pathLabelImagePerson}");
            //    FileInfo DetectionPathPerson = new FileInfo($"{pathDetectionsPerson}/{e.Name}");
            //    var cmd = $@"py -3.5 {pathLabelImagePerson} {pathDetectionsPerson}/{e.Name}";

            //    var p = new Process();

            //    p.StartInfo.FileName = $"{pathPython}/python.exe";
            //    p.StartInfo.Arguments = $"{PersonLabel.FullName} {DetectionPathPerson.FullName}";
            //    p.StartInfo.WorkingDirectory = $"{pathPython}";
            //    //p.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            //    p.StartInfo.UseShellExecute = false;
            //    p.StartInfo.RedirectStandardOutput = true;
            //    p.Start();
            //    string output = p.StandardOutput.ReadToEnd();
            //    p.WaitForExit();
            //    Console.WriteLine(p.TotalProcessorTime);
            //    Console.WriteLine(output);

            //    var result = output.Split(',');

            //    using (var tools = new Tools())
            //    {
            //        // await tools.MoveToDataSetPerson(result[0], result[1], DetectionPath.FullName, result[2]);
            //    }
            //}

            //catch (Exception)
            //{
            //    Console.WriteLine("Fallo");
            //}
        }

        private static void OnRenamed(object source, RenamedEventArgs e)
        {
            // Specify what is done when a file is renamed.
            Console.WriteLine("File: {0} renamed to {1}", e.OldFullPath, e.FullPath);
        }
    }
}