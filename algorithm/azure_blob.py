import os
import config
from random_data import RandomData
from azure.storage import CloudStorageAccount
from azure.storage.blob import BlockBlobService, PageBlobService, AppendBlobService

    def basic_blockblob_operations(self, account):
        blob_name1 = "blob1"
        blob_name2 = "blob2"
        file_to_upload = "HelloWorld.png"
        
        # Create a Block Blob Service object
        blockblob_service = account.create_block_blob_service()
        #blockblob_service = BlockBlobService(account_name=config.STORAGE_ACCOUNT_NAME, account_key=config.STORAGE_ACCOUNT_KEY)
        container_name = 'blockblobbasicscontainer' + self.random_data.get_random_name(6)

        try:
            # Create a new container
            print('1. Create a container with name - ' + container_name)
            blockblob_service.create_container(container_name)
                    
            # Create blobs
            print('2. Create blobs')
            # Get full path on drive to file_to_upload by joining the fully qualified directory name and file name on the local drive
            full_path_to_file = os.path.join(os.path.dirname(__file__), file_to_upload)
            blockblob_service.create_blob_from_path(container_name, blob_name1, full_path_to_file)
            # Create blob from text
            blockblob_service.create_blob_from_text(container_name, blob_name2, self.random_data.get_random_name(256))
            
            # List all the blobs in the container 
            print('3. List Blobs in Container')
            generator = blockblob_service.list_blobs(container_name)
            for blob in generator:
                print('\tBlob Name: ' + blob.name)
            
            # Download the blob
            print('4. Download the blob')
            blockblob_service.get_blob_to_path(container_name, blob_name1, os.path.join(os.path.dirname(__file__), file_to_upload + '.copy.png'))
            blockblob_service.get_blob_to_path(container_name, blob_name2, os.path.join(os.path.dirname(__file__), 'blob2.copy.txt'))
            
            # Delete the blobs, this can be ommited because the container is deleted
            print('5. Delete blobs')
            blockblob_service.delete_blob(container_name, blob_name1)
            blockblob_service.delete_blob(container_name, blob_name2)
        finally:            
            # Delete the container
            print("6. Delete Container")
            if blockblob_service.exists(container_name):
                blockblob_service.delete_container(container_name)
        
