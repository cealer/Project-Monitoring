import cv2
import numpy as np
import os
import uuid

pwd = os.getcwd()

pathDetect = pwd+"/Detections/"

faceDetect = cv2.CascadeClassifier('%s/haarcascade_frontalface_default.xml' % (pwd))
eyeDetect = cv2.CascadeClassifier('%s/haarcascade_eye.xml'%(pwd))

cam = cv2.VideoCapture("rtsp://192.168.1.10:554/user=admin&password=&channel=1&stream=0.sdp?")
#cam = cv2.VideoCapture(0)
foto = ""
comando = ""

while(True):
	ret, img = cam.read()
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	faces = faceDetect.detectMultiScale(gray, 1.3, 5)
	rdn = str(uuid.uuid1())

	for(x,y,w,h) in faces:
		foto = pathDetect+ rdn+".jpg"
		cv2.rectangle (img,(x,y),(x+w,y+h),(0,0,255), 2)
		roi_gray=gray[y:y+h,x:x+w]
		roi_color=img[y:y+h,x:x+w]
		eyes=eyeDetect.detectMultiScale(roi_gray)
		for (ex,ey,ew,eh) in eyes:
			cv2.rectangle(roi_color, (ex,ey), (ex+ew,ey+eh), (0,255,0), 2)
			cv2.imwrite(foto,gray[y:y+h,x:x+w])		
		cv2.waitKey(100)
	cv2.imshow("Face", img)

	cv2.waitKey(10)
cam.release()
cv2.destroyAllWindows()