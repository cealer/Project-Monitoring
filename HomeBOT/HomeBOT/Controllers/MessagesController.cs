﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.Bot.Connector;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using HomeBOT.Models;
using System.Collections.Generic;
using System.Text;
using com.valgut.libs.bots.Wit;
using com.valgut.libs.bots.Wit.Models;
using Microsoft.Bot.Builder.Dialogs;

namespace HomeBOT
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {
        public string uri = "https://monitoring20170825095705.azurewebsites.net/";

        private async Task GetCurrentState(string person, Activity activity, ConnectorClient connector)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    //Desarrollo
                    //client.BaseAddress = new Uri("https://localhost:44317");

                    client.BaseAddress = new Uri(uri);

                    MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");

                    client.DefaultRequestHeaders.Accept.Add(contentType);

                    //Desarrollo
                    HttpResponseMessage res = client.GetAsync($"api/Recognitions/GetRecognitionsByName/{person}").Result;

                    //HttpResponseMessage res = client.GetAsync($"/ApiFace/api/values/{person}").Result;

                    string stringData = res.Content.ReadAsStringAsync().Result;

                    RecResponse data = JsonConvert.DeserializeObject<RecResponse>(stringData);

                    if (data != null)
                    {
                        StringBuilder str = new StringBuilder();

                        if (data.recDate.ToShortDateString() == DateTime.Now.ToShortDateString())
                        {
                            str.AppendLine($"{person} ha sido captado hoy {data.recDate} por la cámara {data.camDescription}, {data.comDescription}-{data.subDescription}.");
                        }
                        else
                        {
                            str.AppendLine($"{person} no fue captado por la cámara el día de hoy. \n");
                            str.AppendLine($"La última captura de {person} fue el {data.recDate} por la cámara {data.camDescription}, {data.comDescription}-{data.subDescription}.");
                        }

                        Activity reply2 = activity.CreateReply();
                        //reply2.Speak = "This is the text that will be spoken.";

                        reply2.Recipient = activity.From;
                        reply2.Attachments = new List<Attachment>();

                        //Activity replyToConversation = reply2.CreateReply();
                        reply2.Recipient = reply2.From;
                        reply2.Type = "message";
                        reply2.Attachments = new List<Attachment>();
                        // AttachmentLayout options are list or carousel
                        reply2.AttachmentLayout = "carousel";

                        #region Card One
                        // Full URL to the image

                        // Create a CardImage and add our image
                        List<CardImage> cardImages1 = new List<CardImage>();
                        cardImages1.Add(new CardImage(url: $"{data.recPath}"));
                        // Create a CardAction to make the HeroCard clickable
                        // Note this does not work in some Skype clients
                        CardAction btn = new CardAction()
                        {
                            Type = "openUrl",
                            Title = "Reconocimiento",
                            Value = $"{data.recPath}"
                        };

                        // Finally create the Hero Card
                        // adding the image and the CardAction
                        HeroCard plCard1 = new HeroCard()
                        {
                            Title = "Resultado de la consulta",
                            Subtitle = str.ToString(),
                            Images = cardImages1,
                            Tap = btn
                        };

                        // Create an Attachment by calling the
                        // ToAttachment() method of the Hero Card
                        Attachment plAttachment1 = plCard1.ToAttachment();

                        // Add the Attachment to the reply message
                        reply2.Attachments.Add(plAttachment1);

                        #endregion

                        #region Card Two

                        List<CardImage> cardImages2 = new List<CardImage>();
                        cardImages2.Add(new CardImage(url: $"{data.recFullPath}"));

                        // CardAction to make the HeroCard clickable
                        CardAction btn2 = new CardAction()
                        {
                            Type = "openUrl",
                            Title = "Tamaño completo",
                            Value = $"{data.recFullPath}"
                        };

                        HeroCard plCard2 = new HeroCard()
                        {
                            Title = "Captura completa",
                            Subtitle = $"{data.descriptionRecs[0].Description}, {data.descriptionRecs[1].Description}, {data.descriptionRecs[2].Description}",
                            Images = cardImages2,
                            Tap = btn2
                        };

                        Attachment plAttachment2 = plCard2.ToAttachment();
                        reply2.Attachments.Add(plAttachment2);
                        #endregion

                        await connector.Conversations.ReplyToActivityAsync(reply2);

                    }

                    else
                    {
                        Activity reply2 = activity.CreateReply($"{person} no está registrado");
                        await connector.Conversations.ReplyToActivityAsync(reply2);
                    }

                }
            }
            catch (Exception ex)
            {
                var r = ex.Message;
            }

        }

        private async Task GetLastUnknow(string person, Activity activity, ConnectorClient connector)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    //Desarrollo
                    //client.BaseAddress = new Uri("https://localhost:44317");

                    client.BaseAddress = new Uri(uri);

                    MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");

                    client.DefaultRequestHeaders.Accept.Add(contentType);

                    //Desarrollo
                    HttpResponseMessage res = client.GetAsync($"api/Recognitions/GetLastUnknow").Result;

                    //HttpResponseMessage res = client.GetAsync($"/ApiFace/api/values/{person}").Result;

                    string stringData = res.Content.ReadAsStringAsync().Result;

                    RecResponse data = JsonConvert.DeserializeObject<RecResponse>(stringData);

                    if (data != null)
                    {
                        StringBuilder str = new StringBuilder();

                        if (data.recDate.ToShortDateString() == DateTime.Now.ToShortDateString())
                        {
                            str.AppendLine($"{person} ha sido captado hoy {data.recDate} por la cámara {data.camDescription}, {data.comDescription}-{data.subDescription}.");
                        }
                        else
                        {
                            str.AppendLine($"{person} no fue captado por la cámara el día de hoy. \n");
                            str.AppendLine($"La última captura de {person} fue el {data.recDate} por la cámara {data.camDescription}, {data.comDescription}-{data.subDescription}.");
                        }

                        Activity reply2 = activity.CreateReply();
                        //reply2.Speak = "This is the text that will be spoken.";

                        reply2.Recipient = activity.From;
                        reply2.Attachments = new List<Attachment>();

                        //Activity replyToConversation = reply2.CreateReply();
                        reply2.Recipient = reply2.From;
                        reply2.Type = "message";
                        reply2.Attachments = new List<Attachment>();
                        // AttachmentLayout options are list or carousel
                        reply2.AttachmentLayout = "carousel";

                        #region Card One
                        // Full URL to the image

                        // Create a CardImage and add our image
                        List<CardImage> cardImages1 = new List<CardImage>();
                        cardImages1.Add(new CardImage(url: $"{data.recPath}"));
                        // Create a CardAction to make the HeroCard clickable
                        // Note this does not work in some Skype clients
                        CardAction btn = new CardAction()
                        {
                            Type = "openUrl",
                            Title = "Reconocimiento",
                            Value = $"{data.recPath}"
                        };

                        // Finally create the Hero Card
                        // adding the image and the CardAction
                        HeroCard plCard1 = new HeroCard()
                        {
                            Title = "Resultado de la consulta",
                            Subtitle = str.ToString(),
                            Images = cardImages1,
                            Tap = btn
                        };

                        // Create an Attachment by calling the
                        // ToAttachment() method of the Hero Card
                        Attachment plAttachment1 = plCard1.ToAttachment();

                        // Add the Attachment to the reply message
                        reply2.Attachments.Add(plAttachment1);

                        #endregion

                        #region Card Two

                        List<CardImage> cardImages2 = new List<CardImage>();
                        cardImages2.Add(new CardImage(url: $"{data.recFullPath}"));

                        // CardAction to make the HeroCard clickable
                        CardAction btn2 = new CardAction()
                        {
                            Type = "openUrl",
                            Title = "Tamaño completo",
                            Value = $"{data.recFullPath}"
                        };

                        HeroCard plCard2 = new HeroCard()
                        {
                            Title = "Captura completa",
                            Subtitle = $"{data.descriptionRecs[0].Description}, {data.descriptionRecs[1].Description}, {data.descriptionRecs[2].Description}",
                            Images = cardImages2,
                            Tap = btn2
                        };

                        Attachment plAttachment2 = plCard2.ToAttachment();
                        reply2.Attachments.Add(plAttachment2);
                        #endregion

                        await connector.Conversations.ReplyToActivityAsync(reply2);

                    }

                    else
                    {
                        Activity reply2 = activity.CreateReply($"{person} no está registrado");
                        await connector.Conversations.ReplyToActivityAsync(reply2);
                    }

                }
            }
            catch (Exception ex)
            {
                var r = ex.Message;
            }

        }


        private async Task GetAssistancePerson(string contact, DateTime date, Activity activity, ConnectorClient connector)
        {
            using (var client = new HttpClient())
            {
                //Desarrollo
                client.BaseAddress = new Uri("https://localhost:44317");

                //client.BaseAddress = new Uri(uri);

                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");

                client.DefaultRequestHeaders.Accept.Add(contentType);

                //Desarrollo
                HttpResponseMessage res = client.GetAsync($"/api/DetailsRecognitions/GetAssistance/{contact}/{date}").Result;

                //HttpResponseMessage res = client.GetAsync($"/ApiFace/api/people/GetAssistance/{contact}/{date}").Result;

                string stringData = res.Content.ReadAsStringAsync().Result;

                List<RecRequest> data = JsonConvert.DeserializeObject<List<RecRequest>>(stringData);

                if (data.Count > 0)
                {

                    //var LastView = data.OrderByDescending(x => x.dreDate).FirstOrDefault();

                    StringBuilder str = new StringBuilder();

                    str.AppendLine($"Asistencia de {contact} \n");

                    foreach (var item in data)
                    {
                        str.AppendLine($"{item.dreDate.ToShortDateString()} {item.dreDate.ToShortTimeString()} \n");
                    }

                    //if (LastView.dreDate.ToShortDateString() == DateTime.Now.ToShortDateString())
                    //{
                    //    str.AppendLine($"{contact} ha sido visto por última vez el {LastView.dreDate}");
                    //}
                    //else
                    //{
                    //    str.AppendLine($"{contact} no fue captado por la cámara el día de hoy. \n");
                    //    str.AppendLine($"La última captura de {contact} fue el {LastView.dreDate}.");
                    //}

                    Activity reply2 = activity.CreateReply($"{str.ToString()}");
                    await connector.Conversations.ReplyToActivityAsync(reply2);

                }
                else
                {
                    Activity reply2 = activity.CreateReply($"{contact} no está registrado");
                    await connector.Conversations.ReplyToActivityAsync(reply2);
                }
            }
        }

        private async Task GetAssistancePerson(string contact, Activity activity, ConnectorClient connector)
        {
            using (var client = new HttpClient())
            {
                //Desarrollo
                client.BaseAddress = new Uri("https://localhost:44317");
                //client.BaseAddress = new Uri(uri);

                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");

                client.DefaultRequestHeaders.Accept.Add(contentType);

                //Desarrollo
                HttpResponseMessage res = client.GetAsync($"/api/DetailsRecognitions/GetAssistance/{contact}").Result;

                //HttpResponseMessage res = client.GetAsync($"/ApiFace/api/people/GetAssistance/{contact}").Result;

                string stringData = res.Content.ReadAsStringAsync().Result;

                List<RecRequest> data = JsonConvert.DeserializeObject<List<RecRequest>>(stringData);

                if (data.Count > 0)
                {

                    //var LastView = data.OrderByDescending(x => x.dreDate).FirstOrDefault();

                    StringBuilder str = new StringBuilder();

                    str.AppendLine($"Asistencia de {contact} \n");

                    foreach (var item in data)
                    {
                        str.AppendLine($"{item.dreDate.ToShortDateString()} {item.dreDate.ToShortTimeString()} \n");
                    }

                    //if (LastView.dreDate.ToShortDateString() == DateTime.Now.ToShortDateString())
                    //{
                    //    str.AppendLine($"{contact} ha sido visto por última vez el {LastView.dreDate}");
                    //}
                    //else
                    //{
                    //    str.AppendLine($"{contact} no fue captado por la cámara el día de hoy. \n");
                    //    str.AppendLine($"La última captura de {contact} fue el {LastView.dreDate}.");
                    //}

                    Activity reply2 = activity.CreateReply($"{str.ToString()}");
                    await connector.Conversations.ReplyToActivityAsync(reply2);

                }
                else
                {
                    Activity reply2 = activity.CreateReply($"{contact} no está registrado");
                    await connector.Conversations.ReplyToActivityAsync(reply2);
                }
            }
        }

        private async Task GetAssistancePerson(Activity activity, ConnectorClient connector)
        {
            using (var client = new HttpClient())
            {
                //Desarrollo
                client.BaseAddress = new Uri("https://localhost:44317");

                //client.BaseAddress = new Uri(uri);

                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");

                client.DefaultRequestHeaders.Accept.Add(contentType);

                //Desarrollo
                HttpResponseMessage res = client.GetAsync($"api/DetailsRecognitions/GetAssistance").Result;

                // HttpResponseMessage res = client.GetAsync($"/ApiFace/api/people/GetAssistance").Result;

                string stringData = res.Content.ReadAsStringAsync().Result;

                List<RecRequest> data = JsonConvert.DeserializeObject<List<RecRequest>>(stringData);

                if (data.Count > 0)
                {
                    StringBuilder str = new StringBuilder();

                    foreach (var item in data)
                    {
                        str.AppendLine($"{item.per.fullName} {item.dreDate.ToShortDateString()} {item.dreDate.ToShortTimeString()} \n");
                    }

                    Activity reply2 = activity.CreateReply($"{str.ToString()}");
                    await connector.Conversations.ReplyToActivityAsync(reply2);

                }
            }
        }

        private async Task SendNotification(ConnectorClient connector, Activity activity, string url)
        {
            var replyMessage = activity.CreateReply("Hola!", "es");

            activity.ChannelId = "facebook";
            activity.Recipient = new ChannelAccount(name: "cesar", id: "@cealerr");
            await connector.Conversations.ReplyToActivityAsync(replyMessage);

            //IMessageActivity newMessage = Activity.CreateMessageActivity();
            //newMessage.Type = ActivityTypes.Message;
            //newMessage.From = activity.From;
            //newMessage.Recipient = new ChannelAccount() { Name="Cesar",Id="@cealerr" };
            //newMessage.Text = $"Hey, something interesting's happening with!";
            //newMessage.Locale = activity.Locale;
            //if (url == "https://localhost:44317/")
            //{
            //    newMessage.ChannelId = "emulator";
            //}
            //else
            //{
            //    newMessage.ChannelId = "facebook";
            //}

            //var conversation = await connector.Conversations.CreateDirectConversationAsync(newMessage.Recipient, newMessage.From);

            //newMessage.Conversation = new ConversationAccount(id: conversation.Id);

            //await connector.Conversations.SendToConversationAsync((Activity)newMessage);
        }

        /// <summary>
        /// POST: api/Messages
        /// Receive a message from a user and reply to it
        /// </summary>
        /// 
        public async Task<HttpResponseMessage> Post([FromBody]Activity activity)
        {
            ConnectorClient connector = new ConnectorClient(new Uri(activity.ServiceUrl));
            // calculate something for us to return
            int length = (activity.Text ?? string.Empty).Length;

            WitClient clienteWit = new WitClient("5XEDASHHCHADD6B4HL5NV7O7OPGZNZ4T");

            if (activity.Type == ActivityTypes.Message)
            {
                //Message message = clienteWit.GetMessage(activity.Text);

                Message message = clienteWit.GetMessage(activity.Text);

                if (message.entities.ToList().FindAll(x => x.Key == "intent").Count > 0)
                {
                    if (message.entities.ToList().FindAll(x => x.Value.FirstOrDefault().value.ToString() == "saludo").Count > 0)
                    {
                        Activity reply2 = activity.CreateReply($"Hola! {activity.From.Name}, estoy a su disposición.");
                        await connector.Conversations.ReplyToActivityAsync(reply2);
                    }

                    else if (message.entities.ToList().FindAll(x => x.Value.FirstOrDefault().value.ToString() == "estado_actual").Count > 0)
                    {

                        var contact = message.entities.ToList().FindAll(x => x.Key == "contact").FirstOrDefault();
                        await GetCurrentState(contact.Value.FirstOrDefault().value.ToString(), activity, connector);
                    }

                    else if (message.entities.ToList().FindAll(x => x.Value.FirstOrDefault().value.ToString() == "asistencia").Count > 0)
                    {
                        //if (message.entities.ToList().FindAll(x => x.Key == "asistencia").Count > 0)
                        //{
                        //GetAssistance/{person}/{date}             
                        if (message.entities.ToList().FindAll(x => x.Key == "contact").Count > 0 && message.entities.ToList().FindAll(x => x.Key == "datetime").Count > 0)
                        {
                            var contact = message.entities.Where(x => x.Key == "contact").FirstOrDefault().Value.FirstOrDefault().value.ToString();
                            var date = DateTime.Parse(message.entities.Where(x => x.Key == "datetime").FirstOrDefault().Value.FirstOrDefault().value.ToString());
                            await GetAssistancePerson(contact, date, activity, connector);
                        }
                        //GetAssistance/{person}   
                        else if (message.entities.ToList().FindAll(x => x.Key == "contact").Count > 0)
                        {
                            var contact = message.entities.ToList().FindAll(x => x.Key == "contact").FirstOrDefault();
                            await GetAssistancePerson(contact.Value[0].value.ToString(), activity, connector);
                        }

                        // GetAssistance
                        else
                        {
                            await GetAssistancePerson(activity, connector);
                        }

                        //}
                    }
                }

            }
            else
            {
                HandleSystemMessage(activity);
            }
            var response = Request.CreateResponse(HttpStatusCode.OK);
            return response;
        }




        private Activity HandleSystemMessage(Activity message)
        {
            if (message.Type == ActivityTypes.DeleteUserData)
            {
                // Implement user deletion here
                // If we handle user deletion, return a real message
            }
            else if (message.Type == ActivityTypes.ConversationUpdate)
            {
                // Handle conversation state changes, like members being added and removed
                // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
                // Not available in all channels
            }
            else if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
                // Handle add/remove from contact lists
                // Activity.From + Activity.Action represent what happened
            }
            else if (message.Type == ActivityTypes.Typing)
            {
                // Handle knowing tha the user is typing
            }
            else if (message.Type == ActivityTypes.Ping)
            {
            }

            return null;
        }
    }
}