﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeBOT.Models
{
    public class Camara
    {
        public string camId { get; set; }
        public string camDescription { get; set; }
        public object camLocation { get; set; }
        public string camDate { get; set; }
        public string subId { get; set; }
        public string camSource { get; set; }
        public bool camEnabled { get; set; }

    }
}