﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeBOT.Models
{
    public class Sub
    {
        public string subId { get; set; }
        public string subDescription { get; set; }
        public DateTime subDate { get; set; }
        public object subLocation { get; set; }
        public string comId { get; set; }
        public string usrId { get; set; }
        public string subNameLocation { get; set; }
        public bool subEnabled { get; set; }

    }
}