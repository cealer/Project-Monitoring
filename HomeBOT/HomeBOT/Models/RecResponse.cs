﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeBOT.Models
{
    public class RecResponse
    {
        public string recId { get; set; }
        public string personFullName { get; set; }
        public string personDNI { get; set; }
        public string recPath { get; set; }
        public string recFullPath { get; set; }
        public DateTime recDate { get; set; }
        public string subDescription { get; set; }
        public string camDescription { get; set; }
        public string comDescription { get; set; }
        public List<DescriptionRec> descriptionRecs { get; set; }

    }
}