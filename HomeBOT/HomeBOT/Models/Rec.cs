﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeBOT.Models
{
    public class Rec
    {
        public string recId { get; set; }
        public string perId { get; set; }
        public string recPath { get; set; }
        public string recFullPath { get; set; }
        public double recPercentage { get; set; }
        public DateTime recDate { get; set; }
        public string subId { get; set; }
        public string camId { get; set; }
        public bool recEnabled { get; set; }

    }
}