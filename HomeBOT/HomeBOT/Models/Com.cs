﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeBOT.Models
{
    public class Com
    {
        public string comId { get; set; }
        public string comDescription { get; set; }
        public DateTime comDate { get; set; }
        public object comLocation { get; set; }
        public object comNameLocation { get; set; }
        public bool comEnabled { get; set; }

    }
}