﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeBOT.Models
{
    public class Per
    {
        public string perId { get; set; }
        public string perName { get; set; }
        public string perLastName { get; set; }
        public string perDni { get; set; }
        public string fullName { get; set; }
        public string perEmail { get; set; }
        public string perPath { get; set; }
        public object perPhone { get; set; }
        public DateTime perDate { get; set; }
        public string perGender { get; set; }
        public DateTime perDateBirth { get; set; }
        public string subId { get; set; }
        public string catId { get; set; }
        public string usrId { get; set; }
        public bool perEnabled { get; set; }
    }
}