﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeBOT.Models
{
    public class RecRequest
    {
        public string dreId { get; set; }
        public string recId { get; set; }
        public string objId { get; set; }
        public double drePercentage { get; set; }
        public bool dreEnabled { get; set; }
        public DateTime dreDate { get; set; }
        public string almId { get; set; }
        public Obj obj { get; set; }
        public Alm alm { get; set; }
        public Rec rec { get; set; }
        public Per per { get; set; }
        public Camara cam { get; set; }
        public Com com { get; set; }
        public Sub sub { get; set; }
    }
}