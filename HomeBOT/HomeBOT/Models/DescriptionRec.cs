﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeBOT.Models
{
    public class DescriptionRec
    {
        public string DesId { get; set; }

        public string RecId { get; set; }

        public string Description { get; set; }

        public virtual Rec Rec { get; set; }
    }
}