﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeBOT.Models
{
    public class Obj
    {
        public string objId { get; set; }
        public string objDescription { get; set; }
        public string objPath { get; set; }
        public DateTime objDate { get; set; }
        public string subId { get; set; }
        public string usrId { get; set; }
        public bool objEnabled { get; set; }
    }
}