﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HomeBOT.Models
{
    public class Alm
    {
        public string almId { get; set; }
        public string almDescription { get; set; }
        public DateTime almDate { get; set; }
        public string perId { get; set; }
        public object objId { get; set; }
        public string subId { get; set; }
        public string usrId { get; set; }
        public bool almEnabled { get; set; }

    }
}