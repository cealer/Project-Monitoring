﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;

namespace HomeBOT
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //var test = DateTime.Now.Minute + 1;
            //if (DateTime.Now.Minute == test)
            //{

            //}
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
